/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
if (typeof(CKEDITOR) !== 'undefined') {
  CKEDITOR.addStylesSet('drupal',
    [
      {
        name: 'Uppercase',
        element: 'span',
        styles: {
          'text-transform': 'uppercase',
        }
      },
      {
        name: 'Blue',
        element: 'span',
        styles: {
          'color': '#179eda',
        }
      },
      {
        name: 'Green',
        element: 'span',
        styles: {
          'color': '#79ba5a',
        }
      },
      {
        name: 'Grey',
        element: 'span',
        styles: {
          'color': '#3c4245',
        }
      },
    ]);
}
