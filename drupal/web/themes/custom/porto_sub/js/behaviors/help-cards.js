(function ($) {
  Drupal.behaviors.helpCards = {
    attach: function (context, settings) {
      $('#carouselHelpCards', context).once('helpCards').each(function () {
        $('.sidebar .list-group-item').on('click', function(e) {
          e.preventDefault();
          var $target = $($(this).attr('href'));

          var scrollTimer = setTimeout(function () {
            $('html,body').animate({scrollTop: $target.offset().top - 160},'slow');
          }, 500);
        });
      });
    }
  };
}(jQuery));
