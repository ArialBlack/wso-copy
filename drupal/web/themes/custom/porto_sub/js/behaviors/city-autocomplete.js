(function ($) {
  Drupal.behaviors.cityAutocomplete = {
    attach: function (context, settings) {
      $('input.locality', context).once('cityAutocomplete').each(function () {
        var $input = $(this),
          api_key = settings.geocodeApiKey,

          buildAutocompleteCity = function($selectors) {
            $selectors.each(function() {
              var $inputCity = $(this);
              var country = $inputCity.closest('details').find('select.country').val();
              var $inputProvince = $inputCity.closest('details').find('select.administrative-area');

              $inputCity.autocomplete({
                source: function(request, response) {
                  $.ajax({
                    url: 'https://app.geocodeapi.io/api/v1/autocomplete?apikey=' + api_key + '&size=10&layers=locality&text=' + request.term + '&boundary.country=' + country,
                  }).done(function(data) {
                    if (data.features) {
                      response($.map(data.features, function(item) {
                        return item.properties
                      }));
                    }
                  });
                },
                delay: 500,
                minLength: 3,
                response: function(event, ui) {
                  if (!ui.content.length) {
                    var noResult = {
                      value: '',
                      label: 'No results found'
                    };
                    ui.content.push(noResult);
                  }
                },
                select: function (event, ui) {
                  console.log(ui.item.region_a);
                  $inputCity.val(ui.item.name);
                  $inputProvince.val(ui.item.region_a).change();
                  return false;
                }
              }).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li class="city-autocomplete-item"></li>')
                  .data('item.autocomplete', item)
                  .append('<a class="p-1 w-100 d-block"><i class="icon icon-location"></i> <b>' + item.name + '<b> <span class="small">' + item.label + '</span></a>')
                  .appendTo(ul);
              };
            });
          };

        buildAutocompleteCity($input);
      });
    }
  };
}(jQuery));
