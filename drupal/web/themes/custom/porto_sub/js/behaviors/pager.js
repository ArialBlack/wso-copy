(function ($) {
  Drupal.behaviors.wsoPager = {
    attach: function (context, settings) {

      $('.pager-container form', context).once('wsoPager').each(function () {
        var $form = $(this),
          $input = $form.find('input[name="page"]'),
          pageNumber = parseInt($input.val()),
          $submit = $form.find('button'),
          currentUrl = window.location,
          pageURL = decodeURIComponent(currentUrl.search.substring(1)),
          uRLVariables = pageURL.split('&'),

          removeParam = function(key, sourceURL) {
            var rtn = sourceURL.split("?")[0],
              param,
              params_arr = [],
              queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";

            if (queryString !== "") {
              params_arr = queryString.split("&");

              for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];

                if (param === key) {
                  params_arr.splice(i, 1);
                }
              }
              rtn = rtn + "?" + params_arr.join("&");
            }
            return rtn;
          }

          navigateUrl = function (e) {
            if (uRLVariables.length > 0 && uRLVariables[0].length > 0) {
              e.preventDefault();
              pageURL = removeParam('page', currentUrl.href);
              pageURL = pageURL + '&page=' + pageNumber;
              location.href = pageURL;
            }
          };

        $input.on('change blur', function() {
          pageNumber = parseInt($input.val());
          // As pagination starts with 0 - get parameter should be reduced by 1.
          --pageNumber;
          if (pageNumber < 0) {
            pageNumber = 0;
          }
        });

        $submit.on('click', function(e) {
          navigateUrl(e);
        });

        $form.submit(function(e) {
          navigateUrl(e);
        });
      });
    }
  };
}(jQuery));
