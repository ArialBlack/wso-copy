(function ($) {
  Drupal.behaviors.responsiveTables = {
    attach: function (context, settings) {
      $('.page-company .table-responsive', context).once('responsiveTables').each(function () {
        $(this).doubleScroll({
          onlyIfScroll: true, // top scrollbar is not shown if the bottom one is not present
          resetOnWindowResize: true // recompute the top ScrollBar requirements when the window is resized
        });
      });
    }
  };
}(jQuery));
