(function ($) {
  Drupal.behaviors.userEdit = {
    attach: function (context, settings) {
      $('#user-form', context).once('userEdit').each(function () {
        var urlHash = window.location.hash;
        var $tabTriggers = $('a[href="' + urlHash + '"]');

        // First check, if hash from root-level-tabs: [user profile + professional profile].
        $tabTriggers.each(function(){
          var $tabTrigger = $(this);
          var $tabContainer = $tabTrigger.closest('#pills-tab');

          if ($tabContainer.length === 1) {
            var $tab = $tabContainer.find('a[href="' + urlHash + '"]');

            if ($tab.length === 1) {
              var thisTabTriggerEl = $tab[0];
              var tab = new bootstrap.Tab(thisTabTriggerEl);
              tab.show();
              $('.sidebar .list-group').hide();
              $('.sidebar .list-group[data-menu-for="' + urlHash + '"]').css('display', 'flex');
            }

            return false;
          }
        });

        // Second check, if hash from second-level-tabs: [Personal+Resume+Skills+Education].
        $tabTriggers.each(function() {
          var $tabTrigger = $(this);
          var $tabContainer = $tabTrigger.closest('#job-pills-tab');

          if ($tabContainer.length === 1) {
            // First of all open root-level-tab: professional profile.
            var thisTabTriggerEl = document.querySelector('#job-tab');
            var tab = new bootstrap.Tab(thisTabTriggerEl);
            tab.show();
            $('.sidebar .list-group').hide();
            $('.sidebar .list-group[data-menu-for="#job"]').css('display', 'flex');
            // Then open proper second-level-tab.
            thisTabTriggerEl = $tabContainer.find('a[href="' + urlHash + '"]');

            if (thisTabTriggerEl.length === 1) {
              tab = new bootstrap.Tab(thisTabTriggerEl[0]);
              tab.show();
            }

            return false;
          }
        });

        $(this).find('.btn-collapse').on('click', function(e) {
          e.preventDefault();
        });

        $('#pills-tab a').on('click', function(e) {
          var pillTarget = $(this).attr('href');

          $('.sidebar .list-group').hide();
          $('.sidebar .list-group[data-menu-for="' + pillTarget + '"]').css('display', 'flex');
        });

        $('.sidebar a[data-bs-toggle-pane]').on('click', function(e) {
          e.preventDefault();
          var targetPane = $(this).data('bs-toggle-pane'),
            href = $(this).attr('href');

          $('#job-pills-tab a[href="' + targetPane + '"]').click();

          var scrollTimer = setTimeout(function () {
            $('html,body').animate({scrollTop: $(href).offset().top - 160},'slow');
          }, 500);
        });
      });
    }
  };
}(jQuery));
