(function ($) {
  Drupal.behaviors.wsoMedia = {
    attach: function (context, settings) {
      $('.view-wso-media', context).once('wsoNodeMedia').each(function () {
        var $view = $(this),
          $podcast_name = $view.find('#edit-show-name'),
          $select = $view.find('#edit-industry-detail'),
          $targetContainer = $view.find('.wso-media__selected-industry'),
          $textSearchInput = $view.find('#edit-title'),
          $form = $view.find('.views-exposed-form'),

          textSearch = function () {
            if ($textSearchInput.val().length > 2 || $textSearchInput.val().length === 0) {
              $form.submit();
            }
          },

          updateTags = function () {
            var $select2 = $select.next('.select2'),
              $chosenFilters = $select2.find('.select2-selection__rendered .select2-selection__choice');

            $targetContainer.html('');

            if ($chosenFilters.length > 0) {
              var $result = $chosenFilters.closest('ul').clone();
              $result.find('.select2-search').remove();
              $result.find('.select2-selection__clear').remove();
              $result.find('.select2-selection__choice__remove').text('');
              $targetContainer.append($result);
            }
          };

        $(document).on('select2-is-ready', function() {
          $('.select-name-show-name .select2-selection--single .select2-selection__placeholder').text('All shows');
          updateTags();
        });

        $podcast_name.on('select2:select', function (e) {
          $form.submit();
        });

        $select.on('select2:select', function(evt, params) {
          //$('#edit_industry_detail_chosen li.result-selected').append('<a class="fake-choice-close"></a>');
          updateTags();
          console.log($select.val());
        });

        $(document).on('click', '.wso-media__selected-industry .select2-selection__choice', function (e){
          e.preventDefault();

          $(this).remove();
          var selectedValues = [];

          $('.wso-media__selected-industry .select2-selection__choice').each(function() {
            $(this).find('select2-selection__choice__remove').text('');
            selectedValues.push($(this).text());
          });

          $select.val(selectedValues).trigger('change');
          $form.submit();
        });

        //Text search:
        //Let's wait when user end typing
        $textSearchInput.donetyping(function () {
          textSearch();
        });

        //Catch clipboard events
        $textSearchInput.bind({
          copy : function(){
            $textSearchInput.on('input propertychange', function () {
              textSearch();
            });
          },
          paste : function(){
            $textSearchInput.on('input propertychange', function () {
              textSearch();
            });
          },
          cut : function(){
            $textSearchInput.on('input propertychange', function () {
              textSearch();
            });
          }
        });
      });
    }
  };

  $.fn.extend({
    donetyping: function (callback, timeout) {
      timeout = timeout || 1500; // 1.5 second default timeout
      var timeoutReference,
        doneTyping = function (el) {
          if (!timeoutReference) return;
          timeoutReference = null;
          callback.call(el);
        };
      return this.each(function (i, el) {
        var $el = $(el);

        $el.is(':input') && $el.on('keyup keypress paste', function (e) {
          if (e.type == 'keyup' && e.keyCode != 8) return;

          if (timeoutReference) clearTimeout(timeoutReference);
          timeoutReference = setTimeout(function () {
            doneTyping(el);
          }, timeout);
        }).on('blur', function () {
          doneTyping(el);
        });
      });
    }
  });
}(jQuery));
