(function ($) {
  Drupal.behaviors.wsoCounter = {
    attach: function (context, settings) {
      $('article .wso-timer-countdown', context).once('wsoCounter').each(function () {
        let $container = $(this);
        let date = new Date();
        let today = Date.now();
        let endDate = new Date();
        let counterType = $container.data('end');
        let counterStartDate = new Date($container.data('start'));
        let diffSeconds = -1;

        let getNextDayOfWeek = function (date, dayOfWeek) {
          let resultDate = new Date(date.getTime());
          resultDate.setDate(date.getDate() + (7 + dayOfWeek - date.getDay()) % 7);

          return resultDate;
        }

        switch (counterType) {
          case 'month':
            //If counter was added for the current month -> start counter. Else - stop it and show zero.
            if (counterStartDate.getMonth() === date.getMonth()) {
              let firstDayNextMonth = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 1).getTime();
              diffSeconds = Math.floor((firstDayNextMonth - today) / 1000);
            }
            break;

          case 'week':
            Date.prototype.getWeek = function() {
              var weekDate = new Date(this.getTime());
              weekDate.setHours(0, 0, 0, 0);
              // Thursday in current week decides the year.
              weekDate.setDate(weekDate.getDate() + 3 - (weekDate.getDay() + 6) % 7);
              // January 4 is always in week 1.
              var week1 = new Date(weekDate.getFullYear(), 0, 4);
              // Adjust to Thursday in week 1 and count number of weeks from date to week1.
              return 1 + Math.round(((weekDate.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
            }

            let nextFriday = getNextDayOfWeek(date, 5);
            //If counter was added for the current week -> start counter. Else - stop it and show zero.
            if (counterStartDate.getWeek() === nextFriday.getWeek()) {
              // Find closets Friday + 12 hours.
              let nextFridayEndTime = getNextDayOfWeek(date, 5).getTime() + (12 * 60 * 60 * 1000);
              diffSeconds = Math.floor((nextFridayEndTime - today) / 1000);
            }
            break;
        }

        if (diffSeconds > 0) {
          $container.countdown({
            outputPattern: '$day Days $hour:$minute:$second',
            from: diffSeconds
          });
        } else {
          $container.text('0 Days 00:00:00');
        }

      });
    }
  };
}(jQuery));
