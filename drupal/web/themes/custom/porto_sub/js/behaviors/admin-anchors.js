(function ($) {
  Drupal.behaviors.wsoAdminAnchors = {
    attach: function (context, settings) {
      $('#show-achors', context).once('wsoAdminAnchors').click(function (e) {
        e.preventDefault();

        let $anchors = $('.main [id]');

        let copyUrl2Clipboard = function(copyText) {
          document.addEventListener('copy', function(e) {
            e.clipboardData.setData('text/plain', copyText);
            e.preventDefault();
          }, true);

          document.execCommand('copy');
          alert('Anchor link was copied to the clipboard: ' + copyText);
        };

        $anchors.each(function (index) {
          let id = $(this).attr('id');
          $('<a class="copy-anchor-link text-white" style="background:red;">#' + id + '</a>').insertBefore($(this));
        });

        $(document).on('click','.copy-anchor-link', function() {
          copyUrl2Clipboard($(this).text());
        });
      });
    }
  };
}(jQuery));
