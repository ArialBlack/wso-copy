(function ($, Drupal) {
  // Special case: Select not being restored when Back button used.
  Drupal.behaviors.htmlSelect = {
    attach: function (context, settings) {
      $('#views-exposed-form-myfeed-user-comments-block-1').find('#edit-sort-by', context).once('htmlSelect').each(function () {
        var select = $(this);

          repairSelect = function() {
            var selectedValue = select.find('option[selected]').val();

            if (selectedValue) {
              select.val(selectedValue);
            } else {
              select.prop('selectedIndex', 0);
            }
          };

        repairSelect();

        // If a page is loading from a cache.
        window.addEventListener('pageshow', function (event) {
          if (event.persisted) {
            repairSelect();
          }
        });
      });
    }
  };

  Drupal.behaviors.wsoSelect2 = {
    attach: function (context, settings) {
      $('select:not(' +
        '#edit-rules, ' +
        '#edit-sets, ' +
        '[name="field_date_graduation_undergrad_value[]"], ' +
        'div.fivestar-form-item select, ' +
        'body.section-admin select, ' +
        '#container.page-node-add-job #edit-locations-0-country, ' +
        '.form-managed-file .form-select' +
        ')', context).once('wsoSelect2').each(function () {

        var $select = $(this),
          className,
          selectClass = '',
          formClass = '',
          allOption = $select.find('option[value="All"]:selected, option[value="_none"]:selected'),
          selectMinimumResultsForSearch = 10,

          formatState = function(state) {
            if (!state.id) {
              return state.text;
            }

            var $state = $(
              '<span><span class="fake-checkbox"></span><i class="icon-flag ' + state.element.value.toLowerCase() + '"></i> ' + state.text + '</span>'
            );

            return $state;
          },

          formatStateSelection = function(state) {
            if (!state.id) {
              return state.text;
            }

            var $state = $(
              '<span><i class="icon-flag"></i> <span></span></span>'
            );

            $state.find("span").text(state.text);
            $state.find("i").addClass(state.element.value.toLowerCase() );

            return $state;
          };

        if ($select.attr('name') && $select.attr('name').length > 0) {
          selectClass = ' select-name-' + $select.attr('name').replace(/_/g, '-');
        }

        if ($select.closest('form').attr('id') && $select.closest('form').attr('id').length > 0) {
          formClass = ' form-id-' + $select.closest('form').attr('id').replace(/_/g, '-');
        }

        if ($select[0].hasAttribute("multiple")) {
          className = 'bootstrap4 select2-multiple' + selectClass + formClass;
        } else {
          className = 'bootstrap4 select2-single' + selectClass + formClass;
        }

        // Hide search if select is small
        if ($select.find('option').length < 50) {
          selectMinimumResultsForSearch = Infinity;
        }

        if ($select.hasClass('select2--region-filter')) {
          $select.select2({
            theme: className,
            placeholder : 'Select Region',
            dropdownCssClass: "select2-dropdown--region-filter",
            allowClear: true,
            minimumResultsForSearch: selectMinimumResultsForSearch,
            templateResult: formatState,
            templateSelection: formatStateSelection
          });
        } else {
          $select.select2({
            theme: className,
            placeholder :'Select..',
            allowClear: true,
            minimumResultsForSearch: selectMinimumResultsForSearch,
          });
        }

        $('.select2-search__field').css('width', '100%');

        // Deselect first All/none option
        if (allOption.length === 1) {
          var $befForm = $select.closest('form.bef-exposed-form');
          // Do not trigger 'change' for BEF.
          if ($befForm.length === 0) {
            $select.val(null).trigger('change');
          }
        }

        $select.closest('.form-type-select').addClass('select2--processed');

        $(document).trigger('select2-is-ready');

        // Validate select2 after closing dropdown
        $select.on('select2:closing', function (e) {
          $(this).valid();
        });

        // Check scrolling. Close opened select2 if document's body scrolled.
        var timer = null;
        document.addEventListener('scroll', function(event) {
          if (timer !== null) {
            clearTimeout(timer);
          }
          timer = setTimeout(function() {
            if (!$(event.target).hasClass('select2-results__options')) {
              if ($('.select2-container--open').length > 0) {
                $('select').select2('close');
              }
            }
          }, 150);
        }, true);

      });
    }
  };
})(jQuery, Drupal);
