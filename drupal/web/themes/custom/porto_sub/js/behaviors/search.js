(function ($) {
  Drupal.behaviors.solrFilterForums = {
    attach: function (context, settings) {
      $('.solr-filter-forums-dropdown', context).once('solrFilterForums').each(function () {
        var $dropdown = $(this),
          $pageContainer = $('.region-content'),
          resizeTimer,

          redrawDropdown = function ($openedDropdown) {
            if ($openedDropdown.length < 1) return;

            var $dMenu = $openedDropdown.find('> .dropdown-menu').css({'opacity':0});

            if ($dMenu.hasClass('col-num-1')) return;

            var dWidth = $pageContainer.width(),
              dpLeft = $openedDropdown.offset().left,
              wWidth = $(window).width(),
              cWidth = $('.search-filters__container').width(),
              dlOffset = (wWidth - cWidth) / 2,
              dShift = -1 * (dpLeft - dlOffset) + 'px';

            dShift = '0 0 0 ' + dShift;

            setTimeout(function () {
              $dMenu.css({
                'width': dWidth,
                'margin': dShift,
                'opacity': 1
              });
            }, 150);
          };

        $dropdown.on('show.bs.dropdown', function () {
          redrawDropdown($(this));
        });

        $(window).on('resize', function () {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function () {
            redrawDropdown($dropdown.filter('.open'));
          }, 250);
        });
      });

      $('.search-form__block', context).once('solrSearch').each(function () {
        var $this = $(this),
          $checkbox = $this.find('#edit-exact'),
          $input = $this.find('input[type="text"], input[type="search"]');

        $checkbox.on('change', function() {
          var execSearch = $(this).prop('checked'),
            text = $input.val();

          if (text.length > 0) {
            if (execSearch) {
              if (text.slice(0,1) !== '"') {
                text = '"' + text;
              }

              if (text.slice(-1) !== '"') {
                text = text + '"';
              }

              $input.val(text);
            }
            else {
              if (text.slice(0,1) === '"') {
                text = text.substr(1);
              }

              if (text.slice(-1) === '"') {
                text = text.slice(0, -1);
              }

              $input.val(text);
            }
          }
        });
      });
    }
  };
}(jQuery));
