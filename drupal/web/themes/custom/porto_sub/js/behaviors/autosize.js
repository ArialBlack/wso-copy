(function ($) {
  Drupal.behaviors.jqueryAutosize = {
    attach: function (context, settings) {
      $('#block-system-main').find('textarea', context).once('jqueryAutosize').each(function () {
        $(this).autosize();
      });
    }
  };
}(jQuery));
