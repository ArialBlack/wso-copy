(function ($) {
  Drupal.behaviors.vudTracker = {
    attach: function (context, settings) {
      if (!$("#custom-user-blocker-rel").hasClass('vudTrackerProcessed')) {
        $("#custom-user-blocker-rel").addClass('vudTrackerProcessed');
        var element = $("#custom-user-blocker-rel").detach();
        $('body').append(element);
      }

      $("#custom-user-blocker-replaceme .hider").click(function(){$('#custom-user-blocker-replaceme').removeClass('ajax-processed');return false;});

      $("#custom-flagged-content-trash-replaceme .hider").click(function(){$('#custom-flagged-content-trash-replaceme').removeClass('ajax-processed');return false;});


      $('#block-system-main table.table a.custom_user_blocker').click(function() {
        $("#custom-user-blocker-abs").position2($(this), {
          anchor: ['tl', 'br'],
          offset: [-5, 5]
        });
        return false;
      });
    }
  };

  jQuery.fn.getBox = function() {
    return {
      left: $(this).offset().left,
      top: $(this).offset().top,
      width: $(this).outerWidth(),
      height: $(this).outerHeight()
    };
  };

  jQuery.fn.position2 = function(target, options) {
    var anchorOffsets = {t: 0, l: 0, c: 0.5, b: 1, r: 1};
    var defaults = {
      anchor: ['tl', 'tl'],
      animate: false,
      offset: [0, 0]
    };
    options = $.extend(defaults, options);
    var targetBox = $(target).getBox();
    var sourceBox = $(this).getBox();

    var left = targetBox.left;
    var top = targetBox.top;

    top -= anchorOffsets[options.anchor[0].charAt(0)] * sourceBox.height;
    left -= anchorOffsets[options.anchor[0].charAt(1)] * sourceBox.width;

    top += anchorOffsets[options.anchor[1].charAt(0)] * targetBox.height;
    left += anchorOffsets[options.anchor[1].charAt(1)] * targetBox.width;

    left += options.offset[0];
    top += options.offset[1];

    $(this).css({
      left: left + 'px',
      top: top + 'px'
    });

  }
}(jQuery));
