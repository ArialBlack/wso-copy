(function ($) {
  Drupal.behaviors.boostrap4Affix = {
    attach: function (context, settings) {
      $('.affix-top', context).once('boostrap4Affix').each(function () {

        var $block = $(this),
          isMobile,
          resizeTimer,
          scrolledPosition,
          isInit = false,
          offsetTop = $block.data('offset-top'),
          offsetBottom = $('.footer-new').height(),
          $parentBlock = $($block.data('parent-block')),
          parentPosition = $block.data('position'),
          blockWidth = $block.width() + 5,
          $body = $('body'),
          bodyHeight = $body.height(),
          initTopPosition = $parentBlock.offset().top,

          checkScroll = function () {
            scrolledPosition = $(document).scrollTop();
            stickBlock();
          },

          init = function () {
            checkScroll();
          },

          checkInit = function () {
            isMobile = window.matchMedia("(max-width: 992px)").matches;

            if (!isMobile) {
              if (!isInit) {
                init();
                isInit = true;
              }
            } else {
              return;
            }
          },

          checkResize = function () {
            bodyHeight = $body.height();
            checkInit();
            checkScroll();
          },

          checkParentPosition = function ($block, set) {
            if (set) {
              var pos = $parentBlock.offset();
              if (parentPosition === 'left') {
                pos = pos.left - blockWidth;
                $block.css('left', pos + 'px');
                $block.css('top', initTopPosition + 'px');
              }
            } else {
              $block.css('left', '');
              $block.css('top', '');
            }
          };

          stickBlock = function () {
            //offset-top exists
            if (offsetTop > 0) {
              if (scrolledPosition >= offsetTop) {
                checkParentPosition($block, true);
                $block.addClass('position--affix--fixed').removeClass('position--affix--not-fixed');
              } else {
                $block.removeClass('position--affix--fixed').addClass('position--affix--not-fixed');
                checkParentPosition($block, false);
              }
            }

            //offset-bottom exists
            if (offsetBottom > 0) {
              if (scrolledPosition >= bodyHeight - offsetBottom) {
                $block.removeClass('position--affix--fixed');
                checkParentPosition($block, false);
              }
            }
          };

        $(document).scroll(function () {
          checkScroll();
        });

        $(window).on('resize', function () {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function () {
            checkResize();
          }, 250);
        });
      });
    }
  };
}(jQuery));
