(function ($) {
  Drupal.behaviors.wsoNodeMedia = {
    attach: function (context, settings) {
      $('body.node-type-media', context).once('wsoNodeMedia').each(function () {
        var $this = $(this);

        $(window).on("load", function (e) {
          $this.addClass('loaded');
        });
      });
    }
  };
}(jQuery));
