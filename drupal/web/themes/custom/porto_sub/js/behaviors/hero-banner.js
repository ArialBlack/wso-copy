(function ($, Drupal) {
  Drupal.behaviors.heroBanner = {
    attach: function (context, settings) {
      $('#hp-hero-banner', context).once('heroBanner').each(function () {
        var $block = $(context),
          $card = $(this),
          $showTrigger = $block.find('.field-name-field-footer-buttons .field--item:first-child span'),
          $modal = $('#userLeaveModal');

        $showTrigger.click(function () {
          $card.toggleClass('active');

          if ($card.hasClass('active')) {
            $modal.modal('show');
          } else {
            $modal.modal('hide');
            $card.removeClass('active');
          }
        });

        $modal.on('hidden.bs.modal', function (e) {
          $card.removeClass('active');
        });
      });
    }
  };
})(jQuery, Drupal);
