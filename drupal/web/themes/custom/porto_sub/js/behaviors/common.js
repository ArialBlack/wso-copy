(function ($) {
  var wso = wso || {};

  // todo: check if we need this
  // Fix Bootstrap 4 and jQueryUI Conflict
  //var bootstrapButton = $.fn.button.noConflict();
  //$.fn.bootstrapBtn = bootstrapButton;

  wso.isTouchDevice = function () {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),

      mq = function(query) {
        return window.matchMedia(query).matches;
      };

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) return true;

    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
  };

  Drupal.behaviors.wsoCommon = {
    attach: function (context, settings) {

      var isTouch = wso.isTouchDevice(),
        $content = $('#content'),

        intiTooltip = function ($tooltip) {
          var tooltipTrigger;

          if (isTouch) {
            tooltipTrigger = 'click';
          } else {
            tooltipTrigger = 'hover focus';
          }

          $tooltip.tooltip({
            'html': true,
            'trigger': tooltipTrigger
          });
        };

      //Common js for all pages
      $('body', context).once('wsoCommon').each(function () {
        $.event.special.touchstart = {
          setup: function( _, ns, handle ){
            this.addEventListener("touchstart", handle, { passive: true });
          }
        };

        //Detect touch screen
        if (isTouch) $('body').addClass('is-touch');

        $('body').on('wsoModalClose', function(event, arg1) {
          if ($(arg1.selector).length > 0) {
            $(arg1.selector).modal('hide');
          }
          $('body').removeClass('modal-open');
          $('.modal-backdrop').hide();
        });

        // var $commentForumWrapper = $('#comments > .comment-forum-wrapper');
        //
        // if ($commentForumWrapper.isInViewport()) {
        //   $commentForumWrapper.css({
        //     'visibility': 'visible',
        //     'content-visibility': 'auto'
        //   });
        // }

        // $(window).one('scroll',function() {
        //   $('#comments > .comment-forum-wrapper, .region-sidebar-right > section:nth-child(n+4)').css({
        //     'visibility': 'visible',
        //     'content-visibility': 'auto'
        //   });
        // });
      });

      $('table.table-hover tbody tr', context).once('wsoTableHover').click(function (e) {
        var url = $(this).find('.views-field-view-node a').attr('href');

        if ((url || '').length > 0) {
          window.location.href = url;
        }
      })

      //Init not 'alternate identity' tooltips only for big screens
      $content.find('span[data-bs-toggle="tooltip"]:not(".author-details-container")', context).once('wsoTooltip').each(function () {
        if (window.matchMedia("(min-width: 992px)").matches) {
          intiTooltip($(this));
        }
      });

      //Init 'alternate identity' tooltips for all screens
      $content.find('.author-details-container[data-bs-toggle="tooltip"]', context).once('wsoAltIdentityTooltip').each(function () {
        intiTooltip($(this));
      });
    }
  };

  Drupal.behaviors.wsoPopovers = {
    attach: function (context, settings) {
      $(document).once('wsoAjaxViews').ajaxSuccess(function (event, data) {
        if (drupalSettings && drupalSettings.views && drupalSettings.views.ajaxViews) {
          var ajaxViews = drupalSettings.views.ajaxViews;

          Object.keys(ajaxViews || {}).forEach(function (i) {
            if (ajaxViews[i]['view_name'] == 'calendar' &&  ajaxViews[i]['view_display_id'] == 'month') {
              $('.popover').popover('hide');
            }
          });
        }
      });

      $('[data-bs-toggle=popover]:not(.special-popover-trigger)', context).once('wsoPopovers').each(function () {
        $(this).popover({
          html : true,
          content: function() {
            var content = $(this).attr("data-popover-content");
            return $(content).html();
          },
        });
      });
    }
  };

  // D7 script for hall of fame threads
  Drupal.behaviors.oldToggle = {
    attach: function (context, settings) {
      $('#content').find('section.toggle', context).once('oldToggle').each(function () {
        var $section = $(this),
          $sectionLabel = $section.find('> label'),
          previewParClosedHeight = 25;

        $sectionLabel.prepend($("<i />").addClass("icon icon-plus"));
        $sectionLabel.prepend($("<i />").addClass("icon icon-minus"));

        $("section.toggle.active > div").addClass("preview-active");
        $("section.toggle.active > div.toggle-content").slideDown(350, function () {
        });

        $sectionLabel.click(function (e) {

          var parentSection = $(this).parent(),
            parentWrapper = $(this).parents("div.toogle"),
            previewPar = false,
            isAccordion = parentWrapper.hasClass("toogle-accordion");

          if (isAccordion && typeof(e.originalEvent) != "undefined") {
            parentWrapper.find("section.toggle.active > label").trigger("click");
          }

          parentSection.toggleClass("active");

          // Preview Paragraph
          if (parentSection.find("> div").get(0)) {
            previewPar = parentSection.find("> div");
            var previewParCurrentHeight = previewPar.css("height");
            previewPar.css("height", "auto");
            var previewParAnimateHeight = previewPar.css("height");
            previewPar.css("height", previewParCurrentHeight);
          }

          // Content
          var toggleContent = parentSection.find("> div.toggle-content");

          if (parentSection.hasClass("active")) {
            $(previewPar).animate({
              height: previewParAnimateHeight
            }, 350, function () {
              $(this).addClass("preview-active");
            });

            toggleContent.slideDown(350, function () {
            });
          } else {
            $(previewPar).animate({
              height: previewParClosedHeight
            }, 350, function () {
              $(this).removeClass("preview-active");
            });
            toggleContent.slideUp(350, function () {
            });
          }
        });
      });
    }
  };

  Drupal.behaviors.bsDropdowns = {
    attach: function (context, settings) {
      $('.bs-dropdown-as-select', context).once('bsDropdowns').each(function () {
        let $dropdown = $(this),
          $btn = $dropdown.find('.dropdown-toggle'),
          $link = $dropdown.find('.dropdown-item');

        $link.click(function(e){
          e.preventDefault();
          $btn.text($(this).text());

          let $tab = $($(this).attr('href')+'-tab');
          $tab.click();
        });
      });
    }
  };

  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
}(jQuery));
