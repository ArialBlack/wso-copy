//todo: move into form
(function ($) {
  Drupal.behaviors.userRegister = {
    attach: function (context, settings) {
      $('#block-socialauthlogin', context).once('userRegister').each(function () {
        $(this).insertAfter($('.social-login-section h5'));
      });
    }
  };
}(jQuery));
