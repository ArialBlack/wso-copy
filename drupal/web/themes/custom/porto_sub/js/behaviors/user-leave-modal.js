(function ($) {
  Drupal.behaviors.wsoUserLeaveModal = {
    attach: function (context, settings) {
      $('#userLeaveModal', context).once('wsoUserLeaveModal').each(function () {
        // User already closed modal. Exit.
        if (localStorage.getItem('userLeaveModalCookie') !== null) {
          return;
        }

        var $modal = $(this),
          startTime = new Date(),
          endTime,
          $form = $modal.find('form'),
          userLeaveModalCookie = localStorage.getItem('userLeaveModalCookie');

        // Detect mouse position.
        $(document).mousemove(function(e) {
          // User's mouse position at page top & Modal never been closed.
          if (e.pageY - $(window).scrollTop() <= 5 && (userLeaveModalCookie === null)) {
            endTime = new Date();

            // user spend at least 10 seconds at the page.
            // prevent accidentaly mouse movements to page top.
            if ((endTime.getTime() - startTime.getTime()) / 1000 > 10) {
              $modal.modal('show');
            }}
        });

        // It's unable to detect mouse movements at touch devices.
        // Will try to show modal after some inactivity.
        // 'detectedTouch' - event from common.js - fires after script detects touch device.
        $(document).on('detectedTouch', function (e) {
          if ($('body').hasClass('is-touch')) {
            var idleTime_inactive = 0,
              idleInterval = setInterval(timerIncrements, 1000),
              idleTime_inactivetemp = false;

            function timerIncrements(){
              // Show modal after 15s of inactivity
              if (idleTime_inactive >= 15 && localStorage.getItem('userLeaveModalCookie') === null) {
                $modal.modal('show');
              }

              if (idleTime_inactivetemp == false) {
                idleTime_inactive++;
              }
            }

            // Zero the idle timer on mouse movement.
            $(document).bind('mouseover click keydown scroll', function () {
              idleTime_inactive = 0;
            });

            document.addEventListener("visibilitychange", function() {
              if(document.hidden == true) {
                idleTime_inactivetemp = true;
              } else {
                idleTime_inactivetemp = false;
              }
            }, false);
          }
        });

        // Save cookies after closing modal.
        $modal.on('hide.bs.modal', function () {
          localStorage.setItem('userLeaveModalCookie', 1);
          userLeaveModalCookie = localStorage.getItem('userLeaveModalCookie');
        });

        $form.submit(function(e) {
          localStorage.setItem('userLeaveModalCookie', 1);
          userLeaveModalCookie = localStorage.getItem('userLeaveModalCookie');
        });
      });
    }
  };
}(jQuery));
