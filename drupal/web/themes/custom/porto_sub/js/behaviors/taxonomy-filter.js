(function ($) {
  Drupal.behaviors.taxonomyFilter = {
    attach: function (context, settings) {
      $('#edit-taxonomy-vocabulary-15-und', context).once('taxonomyFilter').each(function () {
        var $this = $(this);

        $this.keypress(function(key) {
          // block # sign on taxonmy term field
          //if (key.charCode == 35) {
          //key.preventDefault();
          //}
          var code = key.charCode;
          if (!(code > 47 && code < 58) && // numeric (0-9)
            !(code > 64 && code < 91) && // upper alpha (A-Z)
            !(code > 96 && code < 123) && // lower alpha (a-z)
            code != 32 && code != 38 && code != 44 && code != 45) { //space, ampersand, comma and dash
            key.preventDefault();
          }

        });

        $this.bind("cut copy paste", function(e) {
          e.preventDefault();
        });
      });
    }
  };
}(jQuery));
