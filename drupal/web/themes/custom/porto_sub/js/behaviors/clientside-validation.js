(function ($) {
  Drupal.behaviors.wsoClientsideValidation = {
    attach: function (context, settings) {
      $('.node-forum-form, ' +
        '#wso-comments-blurred-registration-form',
        context).once('wsoClientsideValidation').each(function () {
        $(this).validate();
      });
    }
  };

  Drupal.behaviors.wsoFormItemValidation = {
    attach: function (context, settings) {
      $('textarea.clientside-validation', context).once('wsoFormItem').each(function () {
        var $element = $(this),
          minLength = parseInt($element.data('rule-minlength')),
          elementLength = $element.val().length,
          $label = $element.closest('.form-item').find('label'),
          ckEditor,
          checkLength = function() {
            var userString = ckEditor.getData(),
              tmp = document.createElement("div");

            if (tmp.textContent === "" && typeof tmp.innerText == "undefined") {
              userString = "";
            } else {
              tmp.innerHTML = userString;
              userString = tmp.innerText;
            }

            userString = userString.replace(/[\x00-\x1F\x7F-\x9F]/g, "");
            elementLength = userString.length;

            var $target = $label.find('.counter');

            if (elementLength < minLength) {
              $target.removeClass('status').addClass('warning').html('Content minimum length is set to 100 characters, <strong>' + (minLength - elementLength) + '</strong> to go');
            } else {
              $target.removeClass('warning').addClass('status').html('Content length <strong>' + elementLength + '</strong> is fine');
            }
          };

        $label.addClass('form-item--min-check');
        $label.html('<div class="label-text">' + $label.html() + '</div>');
        $label.find('.label-text').after('<div class="counter messages"></div>');

        CKEDITOR.on( 'instanceReady', function( evt ) {
          ckEditor = CKEDITOR.instances;
          ckEditor = ckEditor[Object.keys(ckEditor)[0]];

          checkLength();

          ckEditor.on('change', function() {
            checkLength();
          });
        });
      });

      $('form.user-form', context).once('userFormValidation').each(function () {
        var $form = $(this);

        $form.validate({
          errorPlacement: function (error, element) {
            // Radio/checkbox.
            if (element.parent('.form-type-checkbox').length) {
              error.insertAfter(element.next('.control-label'));
            }
            else {
              // Default.
              error.insertAfter(element);
            }
          }
        });
      });
    }
  };
}(jQuery));
