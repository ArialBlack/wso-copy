(function ($) {
  Drupal.behaviors.eventsForm = {
    attach: function (context, settings) {
      $('.infusion-form', context).once('eventsForm').each(function () {

        // Special validation for async infusionsoft js form
        var $form = $(this),
          $submit = $form.find('button'),
          $fields = $form.find('.infusion-field input, .infusion-field select');

          validateForm = function() {
            $fields.each(function(){
              var $field = $(this),
                $parent = $field.closest('.infusion-field');

              if (!$field.val() || $field.val().length === 0) {
                if ($parent.find('label.error').length === 0) {
                  $parent.append('<label class="error">Field is required</label>');
                }
              } else {
                $parent.find('label.error').remove();
              }
            });

            if ($('label.error').length > 0) {
              $submit.attr('disabled','disabled');
            }
            else {
              $submit.removeAttr('disabled');
            }
          };

        $(window).on('load', function() {
          $submit.attr('disabled','disabled');

          $form.on('click', function(e) {
            validateForm();
          });

          $fields.on('change', function() {
            validateForm();
          });
        });
      });
    }
  };
}(jQuery));

