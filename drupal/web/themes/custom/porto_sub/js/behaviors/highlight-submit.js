(function ($) {
  Drupal.behaviors.highlightSubmit = {
    attach: function (context, settings) {
      $.fn.exists = function() {
        return this.length > 0;
      };

      $('#block-system-main').find('form.node-form input#edit-submit[type=submit]', context).once('bananas').each(function () {
        var $this = $(this);

        $this.on('click', function (e) {
          if (!$this.attr('disabled')) {
            if (!$('.page-node-edit').exists()) {
              if ($('body.page-node-add-blog').exists() || $('body.page-node-add-forum').exists()) {
                this.value = "+2 Bananas!";
              }
              else if ($('body.page-node-add-company').exists()) {
                this.value = "+3 Bananas!";
              }
              else if ($('body.page-node-add-job').exists()) {
                this.value = "+5 Bananas!";
              }
              else if ($('body.page-node-add-company-interview').exists() || $('body.page-node-add-company-review').exists() || $('body.page-node-add-company-compensation').exists() || $('body.page-node-add-resume').exists()) {
                this.value = "+10 Bananas!";
              }
            }
            $this.css('background-image', 'inherit');
          }
          else {
            e.preventDefault();
          }
        });
      });
    }
  };
}(jQuery));
