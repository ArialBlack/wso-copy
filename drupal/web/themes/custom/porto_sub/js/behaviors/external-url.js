(function ($) {
  Drupal.behaviors.wsoExternalUrls = {
    attach: function (context, settings) {
      $('#content article a', context).once('wsoExternalUrls').click(function (e) {

        var a = new RegExp('/' + window.location.host + '/');

        if(!a.test(this.href)) {
          this.setAttribute('target', '_blank');
        }
      });
    }
  };
}(jQuery));
