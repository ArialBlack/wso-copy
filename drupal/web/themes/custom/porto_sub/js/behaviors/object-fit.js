(function ($) {
  Drupal.behaviors.objectFit = {
    attach: function (context, settings) {
      var ua = window.navigator.userAgent;
      var isIE = /MSIE|Trident/.test(ua);

      if ( isIE ) {
        //IE specific code goes here
        $('#content').find('.jumbotron__header img', context).once('objectFit').each(function () {
          objectFitImages($(this));
        });
      }
    }
  };
}(jQuery));
