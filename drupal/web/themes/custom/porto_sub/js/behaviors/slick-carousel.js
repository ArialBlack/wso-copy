(function ($) {
  Drupal.behaviors.slickSlider = {
    attach: function (context, settings) {
      $('.slick-slider', context).once('slickSlider').each(function () {
        var $this = $(this);
        $this.slick();
      });

      $('.view-trustpilot-reviews.view-display-id-block_1 .view-content', context).once('slickSliderTrustpilot').each(function () {
        var $this = $(this);
        $this.slick({
          centerPadding: "60px",
          arrows: true,
          dots: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 560,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
      });

      $('.view-trustpilot-reviews.view-display-id-block_2 .view-content', context).once('slickSliderTrustpilot').each(function () {
        var $this = $(this);
        $this.slick({
          //centerMode: true,
          autoplay: true,
          autoplaySpeed: 4000,
          //centerPadding: "60px",
          arrows: true,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1
        });
      });

      // Carousel of trustpilot reviews for the checkout popup.
      $('.product-trustpilot-reviews .view-trustpilot-reviews.view-id-trustpilot_reviews .view-content', context).once('slickSliderTrustpilotCheckout').each(function () {
        var $this = $(this);
        $this.slick({
          autoplay: true,
          autoplaySpeed: 4000,
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1
        });
      });

      // Special case for nested sliders.
      $('.paragraph--type--carousels-list .slick-cloned .slick-slider:not(.slick-initialized)', context).once('slickSliderCloned').each(function () {
        var $this = $(this);
        $this.slick();
      });
    }
  };
}(jQuery));
