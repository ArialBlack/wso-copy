/*
 * WSO Primary menu login link destination issue
 *
 */
(function ($) {
  Drupal.behaviors.wsoLoginRedirect = {
    attach: function (context, settings) {
      $('#header').find('a[href^="/user/login"]', context).once('wsoLoginRedirect').each(function () {
        $(this).click(function () {
          var pathname = window.location.pathname;

          if (pathname != '/user/login' && pathname != '/user/register' && pathname != '/user/password') {
            var newUrl = this + '?destination=' + pathname.substring(1);
            $(this).attr('href', newUrl);
          }
        });
      });
    }
  };
}(jQuery));
