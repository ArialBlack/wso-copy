(function ($) {
  Drupal.behaviors.blockEnterSubmit = {
    attach: function (context, settings) {
      $('#edit-taxonomy-vocabulary-15-und', context).once('blockEnterSubmit').each(function () {
        $(this).keypress(function(e){
          if ( e.which == 13 ) {
            e.preventDefault();
          }
        });
      });
    }
  };
}(jQuery));
