(function ($) {
  Drupal.behaviors.nodeFormEdit = {
    attach: function (context, settings) {
      $('.node-form:not(.node-course-edit-form, .node-lesson-edit-form, .node-landing-page-edit-form, .with-paragraphs .node-page-edit-form)', context).once('nodeFormEdit').each(function () {

        var $form = $(this),
          formCkEditor = false,
          emptyParagraph = '<p>&nbsp;</p>',

          findCkEditor = function (selector) {
            for ( var i in CKEDITOR.instances ){
              var ckEditorName = CKEDITOR.instances[i].name;

              if ( $(selector + ' #' + ckEditorName).length === 1 ) {
                return CKEDITOR.instances[i];
              }
            }
          },

          checkEmptyMultiField = function(field, trigger, checkOnChange) {
            if ($form.find(field).length != 0) {
              var f = field, t = trigger;

              if ($form.find(f).val().length === 0) {
                $form.find(t).attr('disabled','disabled');
              } else {
                $form.find(t).removeAttr('disabled');
              }

              if (checkOnChange) {
                $form.find(f).donetyping(function () {
                  checkEmptyMultiField(f, trigger, false);
                });
              }
            }
          };

        if ($('#edit-body-wrapper .form-type-select').length > 0) {
          $('#edit-body-wrapper .form-type-select').appendTo('[data-move-target="body-text-formats"]');
        } else {
          $form.find('.col-title').removeClass('col-md-16').addClass('col-md-24');
        }

        checkEmptyMultiField('.field--name-field-address table tr:last-child select.country', '[data-drupal-selector="edit-field-address-add-more"]', false);
        checkEmptyMultiField('.field--name-field-interviewquestions table tr:last-child textarea', '[data-drupal-selector="edit-field-interviewquestions-add-more"]', true);

        $(document).once('nodeFormEditAjax').ajaxComplete(function (event, xhr, settings) {
          checkEmptyMultiField('.field--name-field-address table tr:last-child select.country', '[data-drupal-selector="edit-field-address-add-more"]', false);
          checkEmptyMultiField('.field--name-field-interviewquestions table tr:last-child textarea', '[data-drupal-selector="edit-field-interviewquestions-add-more"]', true);
        });

        // Insert empty paragraph at the end for better UX at edit mode
        if (typeof CKEDITOR !== 'undefined') {
          CKEDITOR.on('instanceReady', function () {
            formCkEditor = findCkEditor('form.node-form');

            if (formCkEditor && $('body.is-touch').length === 1) {
              formCkEditor.focus();

              formCkEditor.setData(formCkEditor.getData() + emptyParagraph, function () {
                // Move cursor position to the end of the text.
                var range = formCkEditor.createRange();
                range.moveToElementEditablePosition( formCkEditor.editable(), true );
                formCkEditor.getSelection().selectRanges( [ range ] );
              });
            }

            // Darkmode hack.
            if (formCkEditor) {
              if ($('html').hasClass('darkmode')) {
                $("iframe.cke_wysiwyg_frame").contents().find("html").addClass("darkmode");
              }
              else {
                $("iframe.cke_wysiwyg_frame").contents().find("html").removeClass("darkmode");
              }
            }
          });
        }

        $('fieldset[required="required"] input[type="checkbox"]').addClass('require-one');
        $('fieldset[required="required"]').after('<div class="fieldset-error"></div>');

        $.validator.addMethod('require-one', function (value) {
          return $('.require-one:checked').length > 0;
          }, 'Please check at least one.');

        var checkboxes = $('.require-one');
        var checkbox_names = $.map(checkboxes, function(e,i) { return $(e).attr('name')}).join(' ');

        checkboxes.change(function() {
          if (this.checked) {
            $(this).closest('fieldset').next('.fieldset-error').html('');
          }
        });

        // Todo: move into clientside-validation.js.
        // Change clientside validation errors placement.
        $form.validate({
          groups: {
            checks: checkbox_names
          },
          rules: {
            field_interviewdifficulty: 'required',
            field_sex: 'required',
            field_varsity_athlete: 'required',
            field_millitary_program: 'required',
            field_jobstatus: 'required',
            field_paytype:  'required',
          },
          messages: {
            field_interviewdifficulty: 'Overall, how difficult was the interview? is required.',
            field_sex: 'Sex is required.',
            field_varsity_athlete: 'Varsity Athlete is required.',
            field_millitary_program: 'Millitary Program (ie. ROTC) is required.',
            field_jobstatus: 'Job Status is required.',
            field_paytype: 'Pay Type is required.'
          },
          errorPlacement: function (error, element) {
            if (element.hasClass('require-one')) {
              checkboxes.closest('fieldset').next('.fieldset-error').html(error); // checkboxes in fieldset
            }
            else if (element.parent('.input-group').length) {
              error.insertAfter(element.parent()); // radio/checkbox
            }
            else if (element.closest('.fieldset-wrapper').length) {
              error.insertAfter(element.closest('.fieldset-wrapper'));
            }
            else if (element.hasClass('select2-hidden-accessible')) {
              error.insertAfter(element.next('span')); // select2
              element.next('span').addClass('error').removeClass('valid');
            }
            else {
              error.insertAfter(element); // default
            }
          }
        });

        $form.on('submit', function(e) {
          if (formCkEditor) {
            var ckData = formCkEditor.getData(),
              stringEnd = emptyParagraph + '\n';

            // Remove previously inserted empty paragraph
            if (formCkEditor && ckData.endsWith(stringEnd)) {
              ckData = ckData.substring(0, ckData.length - stringEnd.length);
              formCkEditor.setData(ckData);
            }
          }
        });
      });

      // Dropdown with color preview: http://prntscr.com/p55g7v
      $('.node-form .field--name-field-cover-color select', context).once('nodeFormEditColorSelect').each(function () {
        var $select = $(this),
          timer;

        $select.on('select2:open', function (e) {
          clearTimeout(timer);
          timer = setTimeout(function () {
            var $options = $('.select2-container--open').find('li');

            $options.each(function() {
              var $hexValue = $(this).text(),
                $textColor = 'white';

              if ($hexValue === '- None -') {
                $hexValue = '#fff';
                $textColor = 'black';
              }

              $(this).css({
                'background-color': $hexValue,
                'color': $textColor
              });
            });
          }, 150);
        });

        $select.on('select2:select', function (e) {
          var $hexValue = $select.val(),
            $textColor = 'white';

          if ($hexValue === '_none') {
            $hexValue = '#fff';
            $textColor = 'black';
          }

          $select.next('.select2').find('.select2-selection').css({
            'background-color': $hexValue,
            'color': $textColor
          });
        });
      });

      $('.node-form select.select2--region-filter', context).once('nodeFormRegionalFilter').each(function () {
        var $select = $(this);

        $select.on('select2:open', function (e) {
          setTimeout(function () {
            $('.select2-container--open .select2-dropdown--region-filter .select2-results__option[role=group]').click(function(e) {
              e.preventDefault();
              $(this).toggleClass('opened');
            });
          }, 250);
        });

        // Clean empty values
        $select.on('select2:select, select2:clear', function (e) {
          var values = $select.val();
          values = values.filter(function(item){
            return item.length > 0;
          });

          $select.val(values).trigger('change');
        });
      });
    }
  };

  $.fn.extend({
    donetyping: function (callback, timeout) {
      timeout = timeout || 1500; // 1.5 second default timeout
      var timeoutReference,
        doneTyping = function (el) {
          if (!timeoutReference) return;
          timeoutReference = null;
          callback.call(el);
        };
      return this.each(function (i, el) {
        var $el = $(el);

        $el.is(':input') && $el.on('keyup keypress paste', function (e) {
          if (e.type == 'keyup' && e.keyCode != 8) return;

          if (timeoutReference) clearTimeout(timeoutReference);
          timeoutReference = setTimeout(function () {
            doneTyping(el);
          }, timeout);
        }).on('blur', function () {
          doneTyping(el);
        });
      });
    }
  });

  if (!String.prototype.endsWith) {
    Object.defineProperty(String.prototype, 'endsWith', {
      value: function(searchString, position) {
        var subjectString = this.toString();
        if (position === undefined || position > subjectString.length) {
          position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.indexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
      }
    });
  }
}(jQuery));
