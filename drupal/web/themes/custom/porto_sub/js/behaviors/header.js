(function ($, Drupal) {
  Drupal.behaviors.wsoHeader = {
    attach: function (context, settings) {

      $('#header').find('[data-bs-toggle="collapse"]', context).once('wsoHeaderTrigger').each(function () {
        var $trigger = $(this);

        $trigger.click(function (e) {
          var expanded = $trigger.attr('aria-expanded'),
            classNames,
            $body = $('body');

          if ($trigger.hasClass('m-user')) {
            classNames = 'header-menu--opened header-user-menu--opened';
          }
          else {
            classNames = 'header-menu--opened';
          }

          if (expanded === 'true') {
            $body.addClass(classNames);
          }
          else {
            $body.removeClass(classNames);
          }

        });
      });

      $('#megamenu').find('.dropdown', context).once('megamenuOpenTouch').each(function () {
        var $dropdown = $(this),

          processToggle = function ($container, isClick, e) {
            //Allow navigate to url after click at dropdown toggle at not-touch devices
            if (!isTouch && isClick) {
              if ($(e.target).not(':button.trigger').length !== 0) {
                window.location.href = $container.find('.dropdown-toggle').attr('href');
              }
            }
          },

          detectTouch = function () {
            var prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),

              mq = function (query) {
                return window.matchMedia(query).matches;
              };

            if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) return true;

            var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
            return mq(query);
          },

          isTouch = detectTouch();

        $dropdown.click(function (e) {
          processToggle($(this), true, e);
        }).hover(function (e) {
          processToggle($(this), false, e);
        });
      });

      // Dropdown in dropdown, for region filters.
      $('.dropdown .sub-dropdown-item', context).once('collapseDropdown').each(function() {
        var $subDropdown = $(this),
          $subTrigger = $subDropdown.find('a.sub-dropdown-toggle'),
          $subMenu = $subDropdown.find('.sub-dropdown-menu');

        $subTrigger.on('click', function(e) {
          // stop bootstrap.js to hide the parents
          e.stopPropagation();
          // stop scroll to anchor
          e.preventDefault();
          $subDropdown.toggleClass('opened');
        });
      });
    }
  };
})(jQuery, Drupal);
