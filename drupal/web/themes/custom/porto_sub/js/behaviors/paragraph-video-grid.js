(function ($) {
  Drupal.behaviors.paragraphVideoGrid = {
    attach: function (context, settings) {
      $('.paragraphs-item-paragraph-wista-video--in-grid', context).once('paragraphVideoGrid').each(function () {
        var $modal = $(this).find('.modal');

        $modal.on('show.bs.modal', function (e) {
          var iframe = $modal.find('iframe'),
            originalSrc = iframe.attr('src');

          //todo: investigate autoplay
         // iframe.attr('src', originalSrc + '&autoplay=1');
          iframe.attr('data-original-src', originalSrc);
        });

        $modal.on('hide.bs.modal', function (e) {
          var iframe = $modal.find('iframe'),
            originalSrc = iframe.attr('data-original-src');

          iframe.attr('src', originalSrc);
        });
      });
    }
  };
}(jQuery));
