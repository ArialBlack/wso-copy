(function ($) {
  Drupal.behaviors.userProfilePmSend = {
    attach: function (context, settings) {
      $("#privatemsg-new #edit-body-value", context).once('userProfilePmSend', function() {
        $(this).userProfilePmCalculate($(this));
        $(this).keyup(function() {
          $(this).userProfilePmCalculate($(this));
        });
        $(this).change(function() {
          $(this).userProfilePmCalculate($(this));
        });
      });
    }
  };

  $.fn.userProfilePmCalculate = function() {
    //console.log('calculate');
    var textlen = $(this).val().length;
    if(textlen > 0) {
      $(this).closest('form').find('input[type=submit]').removeAttr('disabled');
    }
    else {
      $(this).closest('form').find('input[type=submit]').attr('disabled','disabled');
    }
  };

  Drupal.behaviors.userProfilePmActions = {
    attach: function (context, settings) {
      var theadInput = $('.privatemsg-list th.select-all input.form-checkbox', context);

      $('.privatemsg-list th.select-all *', context).wrapAll('<div class="form-type-checkbox"></div>');
      $(theadInput).prop('id', 'select-all').after('<label for=' + $(theadInput).attr('id') + '>1</label>');
    }
  };
}(jQuery));
