(function ($) {
  Drupal.behaviors.advancedSearchFilters = {
    attach: function (context, settings) {
      $('#advanced-filter', context).once('advancedSearchFilters').each(function () {
        var $container = $(this),
          advancedNotEmpty = false,

          processUrlParams = function() {
            var params = window.location.search.split(/\?|\&/);

            params.forEach( function(it) {
              if (it) {
                var param = it.split("=");

                //Param value is not empty
                if (param[1]) {
                  var fieldName = decodeURI(param[0]);

                  //Param field is in advanced section
                  if ($container.find('[name="' + fieldName + '"]').length == 1) {
                    advancedNotEmpty = true;
                  }
                }
              }
            });

            return advancedNotEmpty;
          };

        processUrlParams();

        //One of advanced filters is not empty => Let's open advanced filters panel to show it for user
        if (advancedNotEmpty) {
          $('#database_search .company-database__advanced-widgets__toggler').click();
        }
      });
    }
  };
}(jQuery));
