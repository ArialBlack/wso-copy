(function ($) {
  Drupal.behaviors.wsoAdminCommon = {
    attach: function (context, settings) {

      //Custom logic for admin menu
      $('body:not(.node-type-landing-page, .node-type-job)', context).once('wsoAdminCommon').each(function () {

        $(window).on("load", function (e) {
          var $adminMenu = $('#admin-menu');

          if ($adminMenu.length == 1) {
              var resizeTimer,

              checkResize = function() {
                var headerHeight = 0;

                if ($('.forum-dropdown-wrapper').is(':visible')) {
                  headerHeight = $('#header').height();
                }

                var adminMenuHeight = $adminMenu.height() + headerHeight,
                  style = 'margin-top: ' + adminMenuHeight + 'px !important;';

                $(' body.admin-menu')[0].setAttribute( 'style', style);
              };

            checkResize();

            $(window).on('resize', function() {
              clearTimeout(resizeTimer);
              resizeTimer = setTimeout(function() {
                checkResize();
              }, 250);
            });
          }
        })
      });
    }
  };
}(jQuery));
