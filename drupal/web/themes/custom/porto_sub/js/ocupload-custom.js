BUE.postprocess.zzzocuploadcustom = function (E, $) {
    var $button;
    for (var i = 0; i < E.tpl.buttons.length; i++) {
        if ($.trim(E.tpl.buttons[i][1]) == 'js: E.showFileSelectionDialog();') {
            $button = $('#bue-' + E.index + '-button-' + i);
            break;
        }
    }
    if (!$button) {
        return;
    }
    $button.remove();
    return;
};
