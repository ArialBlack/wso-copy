(function($) {
  Drupal.behaviors.ibPrepPack = {
    attach: function (context, settings) {
      var $container = $('#block-system-main');

      $container.find('.gallery-modal a', context).once('ibPrepPackGalleryModalLink').each(function () {
        $(this).on('click', function (e) {
          e.preventDefault();
        });
      });

      $container.find('.gallery-modal', context).once('ibPrepPackGalleryModal').each(function () {
        $(this).magnificPopup({
          type: 'iframe',
          iframe: {
            patterns: {
              youtube: {
                index: 'youtube.com/',
                id: 'v=',
                src: '//www.youtube.com/embed/%id%?autoplay=1'
              }
            },
          }
        });
      });

      $container.find('.video-gallery-modal', context).once('ibPrepPackVideoGalleryModal').each(function () {
        var $this = $(this);

        $this.on('click', function (e) {
          e.preventDefault();
        });

        $this.magnificPopup({
          type: 'iframe',
          iframe: {
            patterns: {
              youtube: {
                index: 'youtube.com/',
                id: 'v=',
                src: '//www.youtube.com/embed/%id%?autoplay=1'
              }
            },
          }
        });
      });


      $container.find('.testimonials-wrapper', context).once('ibPrepPackTestimonialsWrapper').each(function () {
        var $this = $(this),
          $testimonials = $this.find('.testimonials');

        $testimonials.hide();

        var $seemore_btn = $this.find(".seemore");

        var size = $testimonials.length;

        var slider_data = {
          "wrapper":	this,
          "x": 		3,
          "size":		size
        };

        $seemore_btn.data("slider_data", slider_data);

        $testimonials.filter(':lt('+slider_data.x+')').show();

        $seemore_btn.click(function () {
          var slider_data = $(this).data("slider_data");
          slider_data.x = (slider_data.x + 3 <= slider_data.size) ? slider_data.x + 3 : slider_data.size;

          $('.testimonials:lt('+slider_data.x+')', slider_data.wrapper).slideDown("slow");

          if(slider_data.x == slider_data.size){
            $(this).hide();
          }
          return false;
        });
      });
    }
  };
})(jQuery);
