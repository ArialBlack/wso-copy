(function ($) {
  Drupal.behaviors.stickyHeader = {
    attach: function (context, settings) {
      var $body = $("body"),
        isIE11 = !(window.ActiveXObject) && "ActiveXObject" in window;

      // Disable script If browser not IE11.
      // We have position: sticky in CSS.
      if (!isIE11) {
        return;
      }

        if ($body.hasClass("boxed")) {
        return false;
      }

      $('#header', context).once('stickyHeader').each(function () {
        function stickymenu() {
          var $this = this,
            $body = $("body"),
            header = $("#header"),
            headerContainer = header.parent(),
            menuAfterHeader = (typeof header.data('after-header') !== 'undefined'),
            headerHeight = header.outerHeight(),
            flatParentItems = $("#header.flat-menu ul.nav-main > li > a"),
            logoWrapper = header.find(".logo"),
            logo = header.find(".logo img"),
            logoWidth = "187px",
            logoHeight = "auto",
            logoPaddingTop = parseInt(logo.attr("data-sticky-padding") ? logo.attr("data-sticky-padding") : "0");

          if (menuAfterHeader) {
            headerContainer.css("min-height", header.height());
          }

          $(window).afterResize(function () {
            headerContainer.css("min-height", header.height());
          });

          $this.checkStickyMenu = function () {
            if ($body.hasClass("boxed") || $(window).width() < 991) {
              $this.stickyMenuDeactivate();
              header.removeClass("fixed");
              return false;
            }

            if (!menuAfterHeader) {
              if ($(window).scrollTop() > (headerHeight - 4)) {
                $this.stickyMenuActivate();
              } else {
                $this.stickyMenuDeactivate();
              }

            } else {
              if ($(window).scrollTop() > header.parent().offset().top) {
                header.addClass("fixed");
              } else {
                header.removeClass("fixed");
              }
            }
          };

          $this.stickyMenuActivate = function () {
            if ($body.hasClass("sticky-menu-active")) {
              return false;
            }

            logo.stop(true, true);

            $body.addClass("sticky-menu-active").css("padding-top", headerHeight);
            flatParentItems.addClass("sticky-menu-active");

            logoWrapper.addClass("logo-sticky-active");

            logo.animate({
              top: logoPaddingTop + "px"
            }, 200);
          };

          $this.stickyMenuDeactivate = function () {
            if ($body.hasClass("sticky-menu-active")) {
              $body.removeClass("sticky-menu-active").css("padding-top", 0);
              flatParentItems.removeClass("sticky-menu-active");

              logoWrapper.removeClass("logo-sticky-active");

              logo.animate({
                width: logoWidth,
                height: logoHeight
              }, 200);
              logo.removeAttr('style');
            }
          };

          $(window).on("scroll", function () {
            $this.checkStickyMenu();
          });

          $this.checkStickyMenu();
        }

        stickymenu();
      });
    }
  };
}(jQuery));
