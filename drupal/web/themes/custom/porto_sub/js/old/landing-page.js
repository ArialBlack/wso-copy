"use strict";

document.querySelectorAll('a[href^="#"]').forEach(function (anchor) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault();
    var selector = document.querySelector(e.target.getAttribute('href'));

    if (selector) {
      selector.scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    }
  });
});

/*
 * WSO Primary menu login link destination issue
 *
 */
(function ($) {
  Drupal.behaviors.landingFAQ = {
    attach: function (context, settings) {
      $('.list-arrow', context).once('landingFAQ').each(function () {
        var $ul = $(this);

        $ul.find('li').click(function () {
          if (!$(this).hasClass('active')) {
            $ul.find('li').removeClass('active');
            $(this).addClass('active');
          }
          else {
            $ul.find('li').removeClass('active');
          }
        });
      });
    }
  };
}(jQuery));
