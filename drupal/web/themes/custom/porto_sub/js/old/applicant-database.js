(function($) {
  Drupal.behaviors.applicantDatabase = {
    attach: function(context, settings) {
      $('#accordion h3').each(function() {
        $(this).addClass('ui-accordion-header ui-helper-reset ui-state-default ui-corner-all').prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>');
        $(this).next().addClass('ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom').hide();
      });

      $('#accordion h3').click(function() {
        if ($(this).hasClass('ui-state-active')) {
          $(this).removeClass('ui-state-active ui-corner-top').addClass('ui-state-default ui-corner-all').children('span.ui-icon').removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
          $(this).next().slideUp('fast').removeClass('ui-accordion-content-active');

        } else {
          $(this).next().slideDown('fast').addClass('ui-accordion-content-active');
          $(this).removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');

        }
        return false;
      });

      $(".hw").hide();
      /*--------------------------------------- Score Slider ---------------------------------------------*/
      /* Written SAT */
      var startValue = [0, 800];
      var textStartValue = 'All';
      if ($("#edit-wsat").val() != 'undefined' && $("#edit-wsat").val() != 'All' && $("#edit-wsat").val() != null) {
        var d = $("#edit-wsat").val();
        var start = d.pop();
        var end = d.shift();
        startValue = [start, end];
        textStartValue = start + ' - ' + end;
        $('#expose-Skills').show();
      }
      $("#slider-WSAT").slider({
        range: true,
        values: startValue,
        min: 0,
        max: 800,
        step: 10,
        start: function(event, ui) {
          $("#edit-wsat").val(startValue);
          $("#WSAT-amount").text(textStartValue);

        },
        slide: function(event, ui) {
          var foo = [];
          if (ui.values[0] > 0) {
            for (var i = ui.values[0]; i <= ui.values[1]; i += 10) {
              foo.push(i);
            }
          }
          $('#edit-wsat').chosen().val(foo).trigger("chosen:updated");
          if (ui.values[0] == 0 && ui.values[1] == 800) {
            $("#WSAT-amount").text('All');
          } else {
            $("#WSAT-amount").text(ui.values[0] + ' - ' + ui.values[1]);

          }
        }
      }); //slider
      $("#WSAT-amount").text(textStartValue);
      /* Verbal SAT */
      var startValue = [0, 800];
      var textStartValue = 'All';
      if ($("#edit-vsat").val() != 'undefined' && $("#edit-vsat").val() != 'All' && $("#edit-vsat").val() != null) {
        var d = $("#edit-vsat").val();
        var start = d.pop();
        var end = d.shift();
        startValue = [start, end];
        textStartValue = start + ' - ' + end;
        $('#accordion #expose-Scores').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');

      }
      $("#slider-VSAT").slider({
        range: true,
        values: startValue,
        min: 0,
        max: 800,
        step: 10,
        start: function(event, ui) {
          $("#edit-vsat").val(startValue);
          $("#VSAT-amount").text(textStartValue);

        },
        slide: function(event, ui) {
          var foo = [];
          if (ui.values[0] > 0) {
            for (var i = ui.values[0]; i <= ui.values[1]; i += 10) {

              foo.push(i);
            }
          }
          $('#edit-vsat').chosen().val(foo).trigger("chosen:updated");
          if (ui.values[0] == 0 && ui.values[1] == 800) {
            $("#VSAT-amount").text('All');
          } else {
            $("#VSAT-amount").text(ui.values[0] + ' - ' + ui.values[1]);

          }
        }
      }); //slider
      $("#VSAT-amount").text(textStartValue);

      /* MATH SAT */
      var startValue = [0, 800];
      var textStartValue = 'All';
      if ($("#edit-msat").val() != 'undefined' && $("#edit-msat").val() != 'All' && $("#edit-msat").val() != null) {
        var d = $("#edit-msat").val();
        var end = d.shift();
        var start = d.pop();

        startValue = [start, end];
        textStartValue = start + ' - ' + end;
        $('#accordion #expose-Scores').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      }
      $("#slider-MSAT").slider({
        range: true,
        values: startValue,
        min: 0,
        max: 800,
        step: 10,
        start: function(event, ui) {
          $("#edit-msat").val(startValue);
          $("#MSAT-amount").text(textStartValue);

        },
        slide: function(event, ui) {
          var foo = [];
          if (ui.values[0] > 0) {
            for (var i = ui.values[0]; i <= ui.values[1]; i += 10) {

              foo.push(i);
            }
          }
          //alert(foo);
          $('#edit-msat').chosen().val(foo).trigger("chosen:updated");
          if (ui.values[0] == 0 && ui.values[1] == 800) {
            $("#MSAT-amount").text('All');
          } else {
            $("#MSAT-amount").text(ui.values[0] + ' - ' + ui.values[1]);

          }
        }
      }); //slider
      $("#MSAT-amount").text(textStartValue);

      /* GMAT */
      var startValue = [0, 800];
      var textStartValue = 'All';
      if ($("#edit-gmat").val() != 'undefined' && $("#edit-gmat").val() != 'All' && $("#edit-gmat").val() != null) {
        var d = $("#edit-gmat").val();
        var end = d.shift();
        var start = d.pop();

        startValue = [start, end];
        textStartValue = start + ' - ' + end;
        $('#accordion #expose-Scores').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      }
      $("#slider-GMAT").slider({
        range: true,
        values: startValue,
        min: 0,
        max: 800,
        step: 10,
        start: function(event, ui) {
          $("#edit-gmat").val(startValue);
          $("#GMAT-amount").text(textStartValue);

        },
        slide: function(event, ui) {
          var foo = [];
          if (ui.values[0] > 0) {
            for (var i = ui.values[0]; i <= ui.values[1]; i += 10) {
              foo.push(i);
            }
          }
          //alert(foo);
          $('#edit-gmat').chosen().val(foo).trigger("chosen:updated");
          if (ui.values[0] == 0 && ui.values[1] == 800) {
            $("#GMAT-amount").text('All');
          } else {
            $("#GMAT-amount").text(ui.values[0] + ' - ' + ui.values[1]);

          }
        }
      }); //slider
      $("#GMAT-amount").text(textStartValue);

      /*grade-GPA*/
      $(".slider-filter-GPA").each(function() {
        var selId = '#' + $(this).find('select').attr('id');
        var divAmountId = '#' + $(this).find('.slider-text').attr('id');
        var sliderId = '#' + $(this).find('.v-slider').attr('id');
        var startValue = [0, 5.0];
        var textStartValue = 'All';
        if ($(selId).val() != 'undefined' && $(selId).val() != 'All' && $(selId).val() != null) {
          var d = $(selId).val();
          $('#accordion #expose-Scores').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');

          //alert($( selId ).val());
          var end = d.pop();
          var start = d.shift();
          // alert('start:'+start+'-- end:'+end);
          startValue = [start, end];
          textStartValue = start + ' - ' + end;

        }
        // alert(sliderId);
        $(divAmountId).text(textStartValue);
        $(sliderId).empty().slider({
          range: true,
          values: startValue,
          min: 0,
          max: 5.0,
          step: 0.1,
          start: function(event, ui) {
            $(selId).val(startValue);
            $(divAmountId).text(textStartValue);

          },
          slide: function(event, ui) {
            //alert(ui.value);
            var foo = [];
            if (ui.values[0] > 0) {
              for (var i = ui.values[0]; i <= ui.values[1];) {
                foo.push(i.toFixed(1));
                i += 0.1;

              }
            }

            $(selId).chosen().val(foo).trigger("chosen:updated");
            if (ui.values[0] == 0 && ui.values[0] == 5.0) {
              $(divAmountId).text('All');
            } else {
              $(divAmountId).text(ui.values[0] + ' - ' + ui.values[1]);

            }
          }

        });
      });

      /* ACT */
      var startValue = 0;
      var textStartValue = 'All';
      var startValue = [0, 36];

      if ($("#edit-act").val() != 'undefined' && $("#edit-act").val() != 'All' && $("#edit-act").val() != null) {
        var d = $("#edit-act").val();
        var end = d.shift();
        var start = d.pop();
        startValue = [start, end];
        textStartValue = start + ' - ' + end;
        $('#accordion #expose-Scores').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      }
      $("#slider-ACT").slider({

        range: true,
        values: startValue,
        min: 0,
        max: 36,
        start: function(event, ui) {
          $("#edit-act").val(startValue);
          $("#ACT-amount").text(textStartValue);

        },
        slide: function(event, ui) {
          var foo = [];
          if (ui.values[0] > 0) {
            for (var i = ui.values[0]; i <= ui.values[1]; i++) {
              foo.push(i);
            }
          }
          $('#edit-act').chosen().val(foo).trigger("chosen:updated");
          if (ui.values[0] == 1 && ui.values[1] == 36) {
            $("#ACT-amount").text('All');
          } else {
            $("#ACT-amount").text(ui.values[0] + ' - ' + ui.values[1]);

          }
        }
      }); //slider
      $("#ACT-amount").text(textStartValue);
      /*--------------------------------------- Score Slider ---------------------------------------------*/
      /*--------------------------------------- Skills Slider ---------------------------------------------*/
      /* Year Experience 1 */
      // setup Year Experience filters
      $(".slider-year-filter").each(function() {

        var selId = '#' + $(this).find('.ddx select').attr('id');
        var divAmountId = '#' + $(this).find('.amountdiv').attr('id');
        var sliderId = '#' + $(this).find('.slider').attr('id');
        var startValue = [0, 10];
        var textStartValue = 'All';
        if ($(selId).val() != 'undefined' && $(selId).val() != 'All' && $(selId).val() != null) {
          $('#accordion #expose-Skills').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
          var d = $(selId).val();
          //alert($( selId ).val());
          var end = d.pop();
          var start = d.shift();


          if (start === '&lt; 1 year') {
            start = 0;
            startTXT = '0';
          } else {
            startTXT = start;
            start = start.split(" ").shift();
          }

          if (end === '10+ years') {
            end = 10;
            endTXT = '10+ years';
          } else {
            endTXT = end;
            end = end.split(" ").shift();

          }
          if (start == 0 && end == 10) {
            textStartValue = 'All';
          } else {
            textStartValue = startTXT + ' - ' + endTXT;
          }
          startValue = [start, end];

        }
        //alert(startValue);
        $(divAmountId).text(textStartValue);
        $(sliderId).empty().slider({
          range: true,
          values: startValue,
          min: 0,
          max: 10,
          step: 1,
          start: function(event, ui) {
            $(selId).val(startValue);
            $(divAmountId).text(textStartValue);

          },
          slide: function(event, ui) {
            var foo = [];
            for (var i = ui.values[0]; i <= ui.values[1]; i++) {
              if (i == 0) {
                foo.push('&lt; 1 year');
              } else if (i == 1) {
                foo.push(i + ' year');
              } else if (i == 10) {
                foo.push('10+ years');
              } else {
                foo.push(i + ' years');
              }
            }

            if (ui.values[0] == 0 && ui.values[1] == 10) {
              var foo = [];
              $(selId).chosen().val(foo).trigger("chosen:updated");
              $(divAmountId).text('All');
            } else if (ui.values[1] == 10) {
              $(selId).chosen().val(foo).trigger("chosen:updated");
              $(divAmountId).text(ui.values[0] + ' - ' + ui.values[1] + '+ years');

            } else {
              $(selId).chosen().val(foo).trigger("chosen:updated");
              $(divAmountId).text(ui.values[0] + ' - ' + ui.values[1] + ' years');
            }
          }
        });
      }); //each
      /*--------------------------------------- Skills Slider ---------------------------------------------*/
      /*--------------------------------------- Language  and level ---------------------------------------------*/
      $("#edit-language-1").chosen().change(function() {
        var val = $(this).val();
        $("#edit-language-2, #edit-language-3, #edit-language-4").chosen().val(val).trigger("chosen:updated");
      });
      $("#edit-field-major1").chosen().change(function() {
        var val = $(this).val();
        $("#edit-major2, #edit-major3").chosen().val(val).trigger("chosen:updated");
      });
      $("#edit-field-degree1").chosen().change(function() {
        var val = $(this).val();
        $("#edit-field-degree2, #edit-field-degree3").chosen().val(val).trigger("chosen:updated");
      });
      /*--------------------------------------- Language  and level---------------------------------------------*/
      /*--------------------------------------- open multi tabs after search---------------------------------------------*/

      $('#expose-current-status select option:selected').each(function() {
        if ($(this).text() != '<Any>') {
          $('#accordion #expose-current-status').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');

        }
      });

      $('#expose-current-status input:checked').each(function() {
        $('#accordion #expose-current-status').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      });
      $("#expose-current-status input[type='text']").each(function() {
        if ($(this).val()) {
          $('#accordion #expose-current-status').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
        }
      });

      $('#expose-education select option:selected').each(function() {
        if ($(this).text() != '<Any>') {
          $('#accordion #expose-education').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
        }
      });
      $("#expose-education input[type='text']").each(function() {
        if ($(this).val()) {
          $('#accordion #expose-education').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
        }
      });


      $('#expose-desired select option:selected').each(function() {
        if ($(this).text() != '<Any>') {
          $('#accordion #expose-desired').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');

        }
      });

      $('#expose-desired input:checked').each(function() {
        $('#accordion #expose-desired').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      });

      $('#expose-Skills input:checked, #expose-Skills select option:selected').each(function() {
        $('#accordion #expose-Skills').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      });

      if ($("#expose-Skills #edit-other-skills").val()) {
        $('#accordion #expose-Skills').show().addClass('ui-accordion-content-active').prev('h3.ui-accordion-header').removeClass('ui-state-default ui-corner-all').addClass('ui-state-active ui-corner-top').children('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
      }
    }
  };
})(jQuery);
