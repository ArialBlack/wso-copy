<?php

/**
 * @file
 */

use Drupal\user\Entity\Role;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\views\Views;
use Drupal\image\Entity\ImageStyle;
use Drupal\file\Entity\File;
use Drupal\wso_products\WsoProductBoughtSubscriptionHandler;
use Drupal\Core\Database\Database;

/**
 * Implements template_preprocess_page().
 */
function porto_sub_preprocess_page(&$variables) {
  $page = &$variables['page'];
  $user = User::load(\Drupal::currentUser()->id());
  $variables['uid'] = $user->get('uid')->value;
  $variables['user_name'] = $user->get('name')->value;

  if(\Drupal::routeMatch()->getRouteName() == 'entity.node.edit_form') {
    $variables['edit_mode'] = TRUE;
  }

  if (!$user->user_picture->isEmpty()) {
    $style = ImageStyle::load('user_picture_small');
    $variables['user_picture'] = file_url_transform_relative($style->buildUrl($user->user_picture->entity->getFileUri()));
  }
  else {
    $variables['user_picture'] = $variables['default_user_picture'];
  }

  $path = \Drupal::service('path.current')->getPath();
  $path_args = explode('/', ltrim($path, '/'));

  // Remove/move regions.
  $alias = ltrim(\Drupal::service('path_alias.manager')
    ->getAliasByPath($path), '/');
  $node = \Drupal::routeMatch()->getParameter('node');

  if ($node instanceof NodeInterface) {
    $date = $node->getCreatedTime();
    $variables['page']['node_date'] = \Drupal::service('date.formatter')
    // Where $format is a PHP date string like 'M Y'.
      ->format($date, 'custom', 'M d, Y');
    $node_comments = FALSE;

    switch ($node->getType()) {
      case 'certificate':
        unset($variables['page']['sidebar_right']);
        unset($variables['page']['breadcrumbs_replacer']['porto_sub_breadcrumbs']);
        break;

      case 'company':
        $breadcrumbs = $variables['page']['breadcrumbs_replacer']['porto_sub_breadcrumbs'];
        unset($variables['page']['breadcrumbs_replacer']['porto_sub_breadcrumbs']);
        array_unshift($variables['page']['content'], $breadcrumbs);
        break;

      case 'forum':
      case 'wso_alpha':
        $comment_count = $node->get('comment_forum')->comment_count;
        $node_comments = TRUE;
        break;

      case 'event':
        $comment_count = $node->get('comment_node_event')->comment_count;
        $node_comments = TRUE;
        break;

      case 'landing_page':
        $variables['show_page_header'] = $node->get('field_hide_header')->value ? FALSE : TRUE;
        break;
    }

    // Add pager at page top.
    if ($node_comments && node_is_page($node)) {
      if ($comment_count > 0) {
        $variables['pager'] = [
          '#type' => 'pager',
          '#quantity' => 200,
        ];
      }
    }
  }

  // Todo: refactor with routes.
  switch ($alias) {
    case 'wall-street-oasis-editorial-board':
      unset($variables['page']['breadcrumbs_replacer']['porto_sub_breadcrumbs']);
      break;

    case 'about-wallstreetoasiscom':
      unset($variables['page']['sidebar_right']);
      unset($variables['page']['breadcrumbs_replacer']['porto_sub_breadcrumbs']);
      break;
  }

  if (\Drupal::routeMatch()->getRouteName() == 'view.my_template_library.uploaded_templates') {
    unset($variables['page']['sidebar_right']);
  }

  if (\Drupal::routeMatch()->getRouteName() == 'view.quiz_result.quiz_result') {
    unset($variables['page']['sidebar_right']);
  }

  if (\Drupal::routeMatch()->getRouteName() == 'view.lms_dashboard.general') {
    unset($variables['page']['sidebar_right']);
  }

  if (\Drupal::routeMatch()->getRouteName() == 'view.lms_dashboard.per_user_course') {
    unset($variables['page']['sidebar_right']);
  }

  if (\Drupal::routeMatch()->getRouteName() == 'wso_lms_managers.wso_lms_managers_invitation_form') {
    unset($variables['page']['sidebar_right']);
  }

  if (!empty($page['sidebar_right']) && !empty($page['sidebar_left'])) {
    $variables['outer_container_class'] = "col-lg-10";
  }
  elseif (!empty($page['sidebar_right']) || !empty($page['sidebar_left'])) {
    $variables['outer_container_class'] = "col-lg-17";
  }
  else {
    if (($path_args[0] == 'forums' || $path_args[0] == 'salary') && !empty($path_args[1]) && empty($page['sidebar_right'])) {
      $variables['outer_container_class'] = "col-lg-offset-1 col-lg-9";
      if (!empty($variables['node'])) {
        if (strpos($path_args[1], 'wso-hall-of-fame') === 0 && !empty($variables['node']->sticky)) {
          $variables['outer_container_class'] = "col-lg-24";
        }
      }
    }
    else {
      $variables['outer_container_class'] = "col-lg-24";
    }
  }

  $variables['left_sidebar_class'] = 'col-lg-7 d-none d-lg-block';
  $variables['right_sidebar_class'] = 'col-lg-7 d-none d-lg-block';
  $variables['title_tag'] = 'h1';

  // Check if current page is the user profile page.
  if ($path_args[0] == 'user' && is_numeric(($path_args[1]))) {
    if ($variables['uid'] == $path_args[1]) {
      $vars = porto_sub_check_user_profile($variables, $path_args[1], TRUE);
    }
    else {
      $vars = porto_sub_check_user_profile($variables, $path_args[1], FALSE);
    }
  }

  if (isset($vars)) {
    foreach ($vars as $key => $value) {
      $variables[$key] = $value;
    }
  }

  // Header search form.
  $cid = 'search_header_form';
  $cache_data = \Drupal::cache()->get($cid);

  if ($cache_data) {
    $cache_data = $cache_data->data;
    $markup = $cache_data['markup'];
    $attached = $cache_data['attached'];
  }
  else {
    $block_manager = \Drupal::service('plugin.manager.block');
    $plugin_block = $block_manager->createInstance('views_exposed_filter_block:search-site');
    $array = $plugin_block->build();
    $array['#search_header_form'] = TRUE;
    // Make sure cached form is not prefilled.
    // @todo: find a better solution for this form. Add it directly to html?
    $array["search_api_fulltext"]["#value"] = '';
    $attached_form = !empty($array['#attached']) ? $array['#attached'] : [];
    $attached_autocomplete = $array["search_api_fulltext"]["#attached"];
    $attached = array_merge_recursive($attached_form, $attached_autocomplete);
    $markup = \Drupal::service('renderer')->render($array);
    \Drupal::cache()->set($cid, [
      'markup' => $markup,
      'attached' => $attached,
    ]);
  }

  $variables['#attached'] = array_merge_recursive($variables['#attached'], $attached);
  $variables['header_search_form'] = [
    '#markup' => $markup,
  ];

  $variables['monkey_header'] = [
    'placeholder' => [
      '#lazy_builder' => [
        'second_step_registration.lazy_builder:monkeyHeader',
        [],
      ],
      '#create_placeholder' => TRUE,
    ],
  ];
}

/**
 *
 */
function porto_sub_check_user_profile($variables, $uid_from_path, $user_page_owner) {
  $account_uid = $variables['uid'];
  $vars = [];
  /** @var \Drupal\user\UserInterface $account */
  $account = User::load($account_uid);

  $vars['account_uid'] = $account->get('uid')->value;
  $vars['account_name'] = $account->get('name')->value;

  if (!$account->user_picture->isEmpty()) {
    $style = ImageStyle::load('user_picture_small');
    $vars['account_picture'] = file_url_transform_relative($style->buildUrl($account->user_picture->entity->getFileUri()));
  }
  else {
    $vars['account_picture'] = $variables['default_user_picture'];
  }

  $vars['rank'] = custom_rank($account_uid, FALSE);
  $vars['wso_credits'] = number_format(_userpoints_get_current_points($account_uid, 'wso_credits'), 0, '.', ',');
  $vars['banana_points'] = number_format(_userpoints_get_current_points($account_uid, $category = NULL), 0, '.', ',');

  $wso_karma = wso_karma_get_user_karma($vars['account_uid']);
  $vars['number_of_sb'] = number_format((float) $wso_karma[0], 0, '.', ',');
  $vars['number_of_ms'] = number_format((float) $wso_karma[1], 0, '.', ',');

  // Account title.
  $vars['title'] = porto_sub_get_account_title($account);

  // Account industry.
  $vars['industry'] = porto_sub_get_account_industry($account);

  // Account roles & cert user.
  $account_roles = $account->getRoles();

  // Different varibles for monkey head dropdown & user page
  // F.e. current user id = 5, cuurent url = /user/1.
  if ($user_page_owner) {
    $vars['page_account_name'] = $vars['account_name'];
    $vars['page_account_picture'] = $vars['account_picture'];
    $vars['page_user_rank'] = $vars['rank'];
    $vars['page_user_wso_credits'] = $vars['wso_credits'];
    $vars['page_user_banana_points'] = $vars['banana_points'];
    $vars['page_user_number_of_sb'] = $vars['number_of_sb'];
    $vars['page_user_number_of_ms'] = $vars['number_of_ms'];
  }
  else {
    $page_account = User::load($uid_from_path);

    // Wrong user id from url.
    if (!$page_account) {
      return;
    }

    $vars['page_account_name'] = $page_account->get('name')->value;

    if (!$page_account->user_picture->isEmpty()) {
      $style = ImageStyle::load('user_picture_small');
      $vars['page_account_picture'] = file_url_transform_relative($style->buildUrl($page_account->user_picture->entity->getFileUri()));
    }
    else {
      $vars['page_account_picture'] = $variables['default_user_picture'];
    }

    $vars['page_user_rank'] = custom_rank($uid_from_path, FALSE);
    $vars['page_user_wso_credits'] = number_format(_userpoints_get_current_points($uid_from_path, 'wso_credits'), 0, '.', ',');
    $vars['page_user_banana_points'] = number_format(_userpoints_get_current_points($uid_from_path, $category = NULL), 0, '.', ',');
    $page_user_wso_karma = wso_karma_get_user_karma($uid_from_path);
    $vars['page_user_number_of_sb'] = number_format($page_user_wso_karma[0], 0, '.', ',');
    $vars['page_user_number_of_ms'] = number_format($page_user_wso_karma[1], 0, '.', ',');
    $vars['title'] = porto_sub_get_account_title($page_account);
    $vars['industry'] = porto_sub_get_account_industry($page_account);
    $account_roles = $page_account->getRoles();
  }
  /** @var \Drupal\company_database\CompanyDbHelper $helper */
  $helper = \Drupal::service('company_database.helper');
  $is_admin = $helper->userHasRole(['administrator', 'superadmin']);

  // Macabacus code block.
  $block_manager = \Drupal::service('plugin.manager.block');
  $macabacus_block = $block_manager->createInstance('wso_macabacus_code_block', []);
  /** @var \Drupal\wso_macabacus\MacabacusService $macabacus_service */
  $macabacus_service = \Drupal::service('wso_macabacus.default');
  if (($is_admin && $macabacus_service->getCode($uid_from_path, NULL))  || ($user_page_owner && $macabacus_service->getCode($uid_from_path))) {
    $vars['macabacus'] = $macabacus_block->build();
  }


  $vars['show_roles_links'] = $is_admin || $user_page_owner;
  $vars['show_products_by_user_order_items'] = $is_admin || $user_page_owner;

  // Cert user.
  if (in_array('certified_user', $account_roles)) {
    $vars['certified_user_status'] = TRUE;
  }
  else {
    $vars['certified_user_status'] = FALSE;
  }

  $roles_links = '<ul class="user__block__items">';

  foreach ($account_roles as $i => $account_role) {
    $expire_date = \Drupal::service('role_expire.api')
      ->getUserRoleExpiryTime($uid_from_path, $account_role);

    switch ($account_role) {
      case 'authenticated':
        $account_roles[$i] = 'Official Monkey';
        break;

      case 'administrator':
        $account_roles[$i] = 'Administrator';

        $flag_types = [
          'bad_formatting',
          'low_quality',
          'low_quality_comment',
          'needs_review',
          'pm_keyword',
          'rude_or_abusive',
          'rude_or_abusive_comment',
          'abuse_comment',
          'abuse_node',
          'abuse_privatemsg',
          'wrong_place',
        ];

        $flaggings = \Drupal::entityTypeManager()
          ->getStorage('flagging')
          ->getQuery()
          ->condition('flag_id', $flag_types, 'IN')
          ->sort('created', 'DESC')
          ->count()
          ->execute();

        $vars['flagged_content_count'] = $flaggings;
        break;

      case 'recruiter':
        $account_roles[$i] = 'Applicant DB Subscription';
        break;

      case 'certified_user':
        $account_roles[$i] = 'Certified User';
        break;

      case 'superadmin':
        break;

      case 'job_applicant':
        $account_roles[$i] = 'Job Board Susbcription';
        break;

      case 'video_subscriber':
        $account_roles[$i] = 'Video Library Subscription';
        break;

      case 'database_contributor':
        $account_roles[$i] = 'WSO Premium';
        break;

      case 'blogger':
        $account_roles[$i] = 'Official Blogger';
        break;

      case 'superdb':
        $account_roles[$i] = 'Annual Comp Report Access';
        break;

      default:
        /** @var \Drupal\user\RoleInterface $role */
        $role = Role::load($account_role);
        $account_roles[$i] = $role->label();
        break;
    }

    $roles_links = $roles_links . '<li class="user__block__item"><span class="role-item">' . $account_roles[$i] . '</span>';

    if ($vars['show_roles_links'] && $expire_date != "") {
      // We need d7 role id as it was used in the cancel subscription flag.
      $d7_role_id = WsoProductBoughtSubscriptionHandler::getRoleById($account_role, TRUE);
      $cancel_link = '';

      if (wso_store_get_subscriptions($uid_from_path, $d7_role_id)) {
        $options = [
          'attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal',
          ],
        ];
        $url = Url::fromRoute('wso_store.cancel_subscription_form', ['account' => $uid_from_path, 'rid' => $d7_role_id], $options);
        $cancel_link = ' ' . Link::fromTextAndUrl(t('(cancel)'), $url)->toString();
      }

      $roles_links = $roles_links . '<span class="expires"><em>Expires on ' . date("m/d/Y", $expire_date) . $cancel_link . '</em></span>';
    }
    else {
      $vars['expire_roles'][$i] = "";
    }

    $roles_links = $roles_links . '</li>';
  }
  $roles_links = $roles_links . '</ul>';
  $vars['roles_links'] = Markup::create($roles_links);
  $vars['account_roles'] = $account_roles;

  if ($user_page_owner) {
    $vars['follow_link'] = '';
    $vars['start_discussion'] = Markup::create('<a href="/node/add/forum" class="btn btn-success btn-xs" rel="nofollow">' . t('Start Discussion') . '</a>');
  }
  else {
    $vars['follow_link'] = '';

    if (\Drupal::currentUser()->isAuthenticated()) {
      $flag_entity = \Drupal::service('flag')->getFlagById(FOLLOW_USER_FLAG_ID);
      if ($flag_entity) {
        $vars['follow_link'] = \Drupal::service('flag.link_builder')
          ->build('user', $uid_from_path, FOLLOW_USER_FLAG_ID);
      }
    }

    // @FIXME.
    $vars['start_discussion'] = '';
  }

  $button_options = [
    'attributes' => [
      'class' => [
        'btn',
        'btn-success',
        'btn-block',
        'margin-bottom-20',
      ],
    ],
    'query' => [
      'uid' => $uid_from_path,
    ],
  ];

  if ($user_page_owner || count(array_intersect([
    'administrator',
    'SuperAdmin',
  ], \Drupal::currentUser()->getRoles())) > 0) {
    $link = Link::fromTextAndUrl(t("Edit Profile"), Url::fromUserInput('/user/' . $uid_from_path . '/edit')
      ->setOptions($button_options));
    $vars['edit_profile_button'] = $link;
    $vars['show_user_menu'] = TRUE;
  }

  if (!$user_page_owner
    && \Drupal::currentUser()->hasPermission('use private messaging system')
  ) {
    $vars['send_message_button'] = Link::createFromRoute(
      t('Send Private Message'),
      'private_message.private_message_create',
      [],
      $button_options
    );
  }

  if ($user_page_owner) {
    $vars['manage_notifications'] = Link::createFromRoute(
      t('Notifications & Subscriptions'),
      'wso_rules_userpoints_pm.notifications_controller_settings',
      ['user' => $uid_from_path],
      [
        'attributes' => [
          'class' => [
            'btn',
            'btn-secondary',
            'margin-bottom-20',
          ],
        ],
      ]
    );
  }

  return $vars;
}

/**
 *
 */
function porto_sub_forum_options_dropdown_block(&$variables) {
  $variables['forum_options_dropdown_block'] = 'forum_options_dropdown test';
}

/**
 *
 */
function porto_sub_get_account_title($account) {
  $title = 'None Selected';

  if ($account->hasField('field_confirm_title') && !$account->get('field_confirm_title')
    ->isEmpty()) {
    $title = $account->get('field_confirm_title')->getString();
  }

  return Markup::create('<h5>Title</h5> ' . $title);
}

/**
 *
 */
function porto_sub_get_account_industry($account) {
  $industry = 'None Selected';

  if ($account->hasField('field_current_industry') && !$account->get('field_current_industry')
    ->isEmpty()) {
    $industry = $account->get('field_current_industry')->getString();
  }

  return Markup::create('<h5>Industry</h5> ' . $industry);
}
