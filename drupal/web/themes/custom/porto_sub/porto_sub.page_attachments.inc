<?php

/**
 * @file
 */

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/*
 * Implements hook_library_info_build().
 */
function porto_sub_library_info_build() {
  $mask = ['component.css', 'component.js'];
  $directory = 'themes/custom/porto_sub/components';
  $mask = array_map('preg_quote', $mask);
  $mask = implode('|', $mask);
  $file_scan = \Drupal::service('file_system')->scanDirectory($directory, "/{$mask}$/");
  $libraries = [];

  /*
  Example with JS, CSS and MediaQuery:
  $libraries['footer.component1'] = [
    'version' => '1.x',
    'css' => [
      'component' => [
        '/themes/custom/porto_sub/components/03-organisms/footer/footer.component.css' => [],
        '/themes/custom/porto_sub/components/03-organisms/footer/footer.mediaquery.min-width-1200.component.css' => [
          'media' => '(min-width: 1200px)'
        ],
      ],
    ],
    'js' => [
      '/themes/custom/porto_sub/components/03-organisms/footer/footer.component.js' => [],
    ],
    'dependencies' => [
      'core/jquery',
    ],
  ];
  */

  foreach ($file_scan as $file) {
    $parts = explode('.', $file->filename);
    $extension = end($parts);

    switch ($extension) {
      case 'css':
        $libraries[$file->name][$extension]['component']['/' . $file->uri] = [];
        break;
      case 'js':
        $libraries[$file->name][$extension] = [
          '/' . $file->uri => [],
        ];
        break;
    }
  }

  return $libraries;
}

/**
 * Implements hook_attachments_alter()
 */
function porto_sub_page_attachments_alter(array &$attachments) {
  foreach ($attachments['#attached']['html_head'] as $key => $attachment) {
    if ($attachment[1] == 'system_meta_generator') {
      //Remove <meta name="Generator" content="Drupal 8 (https://www.drupal.org)">
      unset($attachments['#attached']['html_head'][$key]);
    }
  }

  /* Attach CSS */
  $main_css = FALSE;
  $path = \Drupal::service('path.current')->getPath();
  $alias = ltrim(\Drupal::service('path_alias.manager')->getAliasByPath($path), '/');
  $route = \Drupal::routeMatch()->getRouteName();

  $path_args = explode('/', $alias);
  $node = \Drupal::routeMatch()->getParameter('node');
  $u = '';

  if (\Drupal::currentUser()->isAuthenticated()) {
    $u = '_user';
    $attachments['#attached']['library'][] = 'porto_sub/logged_in_user';
  } else {
    $attachments['#attached']['library'][] = 'porto_sub/instant.page';
  }

  // If we are on the node revision page.
  if (is_numeric($node) && $route == 'entity.node.revision') {
    $node = Node::load($node);
  }

  // Todo: refactor with routes.
  if ($node instanceof NodeInterface) {
    switch ($node->getType()) {
      case 'forum':
      case 'event':
        $main_css = 'porto_sub/forum_post' . $u;

        if ($route == 'entity.node.edit_form') {
          $attachments['#attached']['library'][] = 'porto_sub/forum_post_user_styles';
        }

        if (!\Drupal::currentUser()->isAuthenticated()) {
          unset($attachments['#attached']['library']['calendar/calendar.theme']);
          unset($attachments['#attached']['library']['poll/drupal.poll-links']);
          unset($attachments['#attached']['library']['quiz/styles']);
        }
        break;

      case 'wso_alpha':
        $main_css = 'porto_sub/wso_alpha_post';
        break;

      case 'job':
        $main_css = 'porto_sub/job';
        break;

      case 'poll':
        $main_css = 'porto_sub/poll' . $u;
        break;

      case 'page':
        if ($node->get('comment_node_page')->comment_count > 0) {
          $main_css = 'porto_sub/page_with_comments';
        } else {
          $main_css = TRUE; // Ready at Storybook
        }

        if ($node->hasField('field_paragraphs') && !$node->get('field_paragraphs')->isEmpty()) {
          $main_css = 'porto_sub/page_with_paragraphs';
        }

        if ($path_args[0] == 'wso-alpha') {
          $main_css = 'porto_sub/wso_alpha_post';
        }
        if ($path_args[0] == 'courses') {
          $main_css = 'porto_sub/courses';
        }

        break;

      case 'editorial_team_member':
        $main_css = 'porto_sub/page_with_paragraphs';
        break;

      case 'category_page':
        $main_css = 'porto_sub/page_category';
        break;

      case 'mentor_profile':
        $main_css = 'porto_sub/mentor_profile';
        break;

      case 'landing_page':
        //Talent oasis page styles have many common styles. Todo: split, refactor them into common & diff
        if (!in_array($node->nid, ['402166', '402167', '402168', '402169'])) {
          $main_css = 'porto_sub/old_landing';
        }

        if ($node->hasField('field_wso_paragraphs') && !$node->get('field_wso_paragraphs')->isEmpty()) {
          $main_css = TRUE; // Ready at Storybook
        }
        break;

      case 'company':
      case 'company_interview':
      case 'company_review':
      case 'company_compensation':
        $main_css = 'porto_sub/company' . $u;
        break;

      case 'media':
        $main_css = 'porto_sub/media';
        break;

      case 'lesson':
      case 'restricted_lesson':
        $main_css = 'porto_sub/lesson';
        break;

      case 'course':
        $main_css = 'porto_sub/course';
        break;

      case 'certificate':
        $main_css = 'porto_sub/course_certificate';
        break;

      case 'student_group':
        $main_css = 'porto_sub/student_dashboard';
        break;

      case 'faq':
        $main_css = 'porto_sub/faq_node';
        break;

      case 'premium_video':
        $main_css = 'porto_sub/premium_video';
        break;

      case 'template':
        $main_css = 'porto_sub/template';
        break;
    }
  }

  // Todo: refactor with routes.
  switch ($alias) {
    case (preg_match('/\bcomment\b\/\breply\b\/\w+\/\w+/i', $alias) ? TRUE : FALSE): // urls like /comment/reply/*/*
    case ('moderate/nodes'):
    case ('moderate/comments'):
      $main_css = 'porto_sub/forum_post_user';
      $attachments['#attached']['library'][] = 'porto_sub/forum_post_user_styles';
      break;

    case (preg_match('/\bcomment\b\/\w+\/\bedit\b/i', $alias) ? TRUE : FALSE): // urls like /comment/*/edit
      $main_css = 'porto_sub/edit_comment';
      $page_css_attached = true;
      break;

    // Urls like /forum/trading
    case (preg_match('/\bforum\b\/\w+/i', $alias) ? TRUE : FALSE):
      // Urls like /forum/trading/hot or /forum/trading/top.
    case (preg_match('/\bforum\b\/\w+\/\b(hot|top)\b/i', $alias) ? TRUE : FALSE):
      // @todo - we are changing the urls structure, now forum topics has the
      // same url structure as container pages.
      if ($main_css !== 'porto_sub/forum_post' . $u) {
        $main_css = 'porto_sub/forum_container';
      }
      break;

    case 'courses':
    case 'courses/my':
      $main_css = 'porto_sub/courses';
      break;

    case 'company':
    case 'company/review':
    case 'company/interview':
    case 'company/salary':
      $main_css = 'porto_sub/company_database';
      break;

    case 'events':
      $main_css = 'porto_sub/events';
      $attachments['#attached']['library'][] = 'porto_sub/select2.min';
      break;

    case 'the-talent-oasis':
      $main_css = 'porto_sub/talent_oasis';
      break;

    case 'the-talent-oasis/candidates':
      $main_css = 'porto_sub/talent_oasis_candidates';
      break;

    case 'the-talent-oasis/employers':
      $main_css = 'porto_sub/talent_oasis_employers';
      break;

    case 'the-talent-oasis/faqs':
      $main_css = 'porto_sub/talent_oasis_faqs';
      break;

    case 'forums':
    case 'forum':
      $main_css = 'porto_sub/forums_list';
      break;

    case 'wall-street-videos':
      $main_css = 'porto_sub/wso_video';
      break;
  }

  // Todo: refactor with routes.
  switch ($path_args[0]) {
    case 'mentors':
      $main_css = 'porto_sub/mentor_profile';
      break;

    case 'highest-ranked-comments':
    case 'tracker':
      $main_css = 'porto_sub/forum_container';
      break;

    case 'node':
      if (!empty($path_args[2]) && ($path_args[2] == 'interview' || $path_args[2] == 'salary' || $path_args[2] == 'review')) {
        $main_css = 'porto_sub/company' . $u;
      }
      break;

    case 'user':
      $main_css = 'porto_sub/user' . $u;

      if (!empty($path_args[2]) && $path_args[2] == 'edit') {
        $attachments['#attached']['library'][] = 'porto_sub/user_edit';
      }
      break;

    case 'frequently-asked-questions':
      $main_css = 'porto_sub/faq';
      break;

    case 'search':
    case 'tag':
    case 'taxonomy':
      $main_css = 'porto_sub/search';
      $attachments['#attached']['library'][] = 'porto_sub/select2.min';
      break;

    case 'private_messages':
    case 'private_message':
    case 'messages':
      $main_css = 'porto_sub/messages';
      break;
  }

  switch ($route) {
    case 'private_message.private_message_create':
      $main_css = 'porto_sub/messages';
      break;

    case 'wso_store.buy_product_form':
    case 'wso_braintree.buy_product_form_dicounted':
      $main_css = 'porto_sub/buy_product';
      break;

    case 'view.wso_alpha.research':
    case 'wso_alpha.main_page':
      $main_css = 'porto_sub/wso_alpha';
      break;

    case 'wso_karma.custom_karma_rank_report_do_ratio_page':
    case 'wso_karma.custom_karma_rank_report_do_counters_page':
    case 'wso_karma.custom_karma_rank_report_do_top_page':
      $main_css = 'porto_sub/userpoints';
      break;

    case 'company_db_reports.ib.default':
    case 'company_db_reports.ib.compensation':
    case 'company_db_reports.ib.reviews':
    case 'company_db_reports.ib.universities':
    case 'company_db_reports.ib.diversity':
    case 'company_db_reports.ib.aggregate':
    case 'company_db_reports.ib.csv':
    case 'company_db_reports.cc.default':
    case 'company_db_reports.cc.compensation':
    case 'company_db_reports.cc.reviews':
    case 'company_db_reports.cc.universities':
    case 'company_db_reports.cc.diversity':
    case 'company_db_reports.cc.aggregate':
    case 'company_db_reports.cc.csv':
    case 'company_db_reports.hf.default':
    case 'company_db_reports.hf.compensation':
    case 'company_db_reports.hf.reviews':
    case 'company_db_reports.hf.universities':
    case 'company_db_reports.hf.diversity':
    case 'company_db_reports.hf.aggregate':
    case 'company_db_reports.hf.csv':
    case 'company_db_reports.pe.default':
    case 'company_db_reports.pe.compensation':
    case 'company_db_reports.pe.reviews':
    case 'company_db_reports.pe.universities':
    case 'company_db_reports.pe.diversity':
    case 'company_db_reports.pe.aggregate':
    case 'company_db_reports.pe.csv':
      $main_css = 'porto_sub/industry_report';
      break;

    case 'company_db_reports':
      $main_css = 'porto_sub/wso_industry_reports';
      break;

    case (preg_match('/view.wso_media.page_\w/i', $route) ? TRUE : FALSE):
      $main_css = 'porto_sub/wso_media';
      break;

    case 'view.mentor_profile.mentors_page':
      $main_css = 'porto_sub/mentors';
      $attachments['#attached']['library'][] = 'porto_sub/mark_js';
      break;
  }

  // Special case for frontpage.
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $main_css = TRUE; // Ready at Storybook
  }

  if ($main_css !== FALSE) {
    if (strlen($main_css) > 10) {
      $attachments['#attached']['library'][] = $main_css;
    }
  } else {
    $attachments['#attached']['library'][] = 'porto_sub/page';
  }

  if (in_array('administrator', \Drupal::currentUser()->getRoles())) {
    $attachments['#attached']['library'][] = 'porto_sub/admin_user';
  }

  if (Drupal::currentUser()->hasPermission('administer flagged content table')) {
    $attachments['#attached']['library'][] = 'porto_sub/admin_user';
  }

  if (Drupal::currentUser()->hasPermission('Resource: Edit any content')) {
    $attachments['#attached']['library'][] = 'porto_sub/admin_anchors';
  }
}

/**
 * Implements hook_library_info_alter().
 */
function porto_sub_library_info_alter(&$libraries, $extension)
{
  unset($libraries['cv.jquery.ckeditor']);
}

/**
 * Implements hook_preprocess_views_view().
 */
function porto_sub_preprocess_views_view(&$variables)
{
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];

  if ($view->id() == 'template_library' || $view->id() == 'my_template_library') {
    $variables['#attached']['library'][] = 'porto_sub/template';
  }
}
