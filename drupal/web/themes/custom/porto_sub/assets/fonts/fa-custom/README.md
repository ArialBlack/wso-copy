# How to install new custom icon/icons into custom font?

Path to custom font: `sites/all/themes/Porto/vendor/fa-custom`
Service: `https://fontello.com`

### **Workflow:**
-------------

1. Go to the custom font directory ()
3. Go to `fontello.com` and drag the SVG (fa-custom.svg) into a 'Custom Icons' field.
2. You can find the fa-custom.svg file by path: ```web/sites/all/themes/Porto/vendor/fa-custom/font/fa-custom.svg```
4. Find and mark the new icon and mark all custom (own) icons.
5. Set the font-name as `fa-custom` and click on a button 'Download webfont'.
   See the screenshot (https://trello-attachments.s3.amazonaws.com/546ba8dff94340fcac33de05/5cecf96c1020a15deca33f39/36721de71244669eeaa3289e5e3f79dd/screen-fontello.png)
6. Unarchive and open the downloaded file/dir 'fontello-hash' (E.g. `fontello-ff077d39`)

7. Open the file `fa-custom-codes.css` and copy the content.
8. Open the file 'fa-custom.css' ```web/sites/all/themes/Porto/vendor/fa-custom/css/fa-custom.css```
   and scroll down to codes (E.g. `.fa-ok { content: '\code'; }`).
9. Replace the existed codes with codes from step **7**
10. Check the git history before you push the updates, because of you can remove needed styles accidentally.

11. Go back into downloaded font directory (E.g. `fontello-ff077d39/font`)
    You can see 5 files here (*.svg, *.woff etc.)
12. Copy the files and replace with existed files here `web/sites/all/themes/Porto/vendor/fa-custom/font`
13. Into the file `fa-custom.css` at the 3rd, 4th lines increase the version of fonts 
    (E.g. 'Increase the GET param (version) ?v10' `src: url('../font/fa-custom.woff2?v=10') format('woff2')`)
14. Into the file `html.tpl.php` at the 49th line increase the version of custom font 
    (E.g. 'Increase the GET param (version) ?v10' `<link rel="preload" as="font" href="/sites/all/themes/Porto/vendor/fa-custom/font/fa-custom.woff2?v=10"`)
    Path to `html.tpl.php` is `sites/all/themes/Porto_sub/templates/html.tpl.php`
    
*Created by [Oleg Kruchay](https://olegkruchay.com).* 2019