/* Dev Plugins */
const gulp = require('gulp');
const gulpif = require('gulp-if');
const clean = require('gulp-clean');
//const shell = require('gulp-shell');
const sass = require('gulp-sass')(require('sass'));
const postCSS = require('gulp-postcss');
//const autoprefixer = require('autoprefixer');
const sassGlob = require('gulp-sass-glob');
//const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
//const babel = require('gulp-babel');
//const merge = require('merge-stream');
//const buffer = require('vinyl-buffer');
const spritesmith = require('gulp.spritesmith');
const svgSprite = require('gulp-svg-sprite');
const newer = require('gulp-newer');
//const eslint = require('gulp-eslint');
const cleanCSS = require('gulp-clean-css');
const gcmq = require('gulp-group-css-media-queries');
//const penthouse = require("penthouse");
//const fs = require('fs');
//const urlList = require('./criticalcss-pagelist.json');
//const urlListOp = require('./criticalcss-op.json');
const mediaQueriesSplitter = require('gulp-media-queries-splitter');
const rename = require('gulp-rename');
const path = require('path');
const splitMediaQueries = require('gulp-split-media-queries');
const cached = require('gulp-cached');
const dependents = require('gulp-dependents');
const paths = require('./paths.json');
const debug = require('gulp-debug');

/* Gulp Tasks */
const tasks = {
  buildPNGSprites: (done) => {
    let currentDate = new Date().getTime();

    let spriteData =
      gulp.src('./assets/sprites/src/png/*.*')
        .pipe(spritesmith({
          retinaSrcFilter: './assets/sprites/src/png/*@2x.png',
          imgName: 'sprite.png',
          retinaImgName: 'sprite@2x.png',
          cssName: '_sprite-png.scss',
          algorithm: 'binary-tree',
          imgPath: `/themes/custom/porto_sub/assets/sprites/build/sprite.png?v=4`,
          retinaImgPath: `./assets/sprites/src/sprite@2x.png?v=4`,
          padding: 2
        }));

    spriteData.img.pipe(gulp.dest('./assets/sprites/build/'));
    spriteData.css.pipe(gulp.dest('./components/common-styles/helpers/'));

    done();
  },
  buildSVGSprites: () => (
    gulp
      .src(paths.sprites.svg.src)
      .pipe(newer(paths.sprites.svg.dest[0]))
      .pipe(svgSprite({
        mode: {
          symbol: true,
        },
      }))
      .pipe(gulp.dest(paths.sprites.svg.dest))
  ),
  copyFiles: (params) => {
    if (!params.src || !params.dest) return false;

    return (
      gulp
        .src(params.src)
        .pipe(gulpif(params.newer, newer(params.dest[0])))
        .pipe(gulp.dest(params.dest))
    );
  },
  clean: src => (
    gulp
      .src(src)
      .pipe(clean({ read: false }))
  ),
};

gulp.task('watch', function () {
  gulp.watch(['./assets/app/css/styles.scss', './components/**/*.scss'],gulp.series(['appStyles', 'styles']))
});

gulp.task('styles', function(cb){
  return gulp.src(['./components/**/*.component.scss'], { since: gulp.lastRun('styles') })
    .pipe(dependents())
    .pipe(debug({title: 'processed:'}))
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./components'))
    .on('end', cb)
});

gulp.task('buildstyles', function(){
  return gulp.src(['./components/**/*.component.scss'])
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(gcmq())
    .pipe(cleanCSS({
      level: {
        1: {
          cleanupCharsets: true, // controls `@charset` moving to the front of a stylesheet; defaults to `true`
          normalizeUrls: true, // controls URL normalization; defaults to `true`
          optimizeBackground: true, // controls `background` property optimizations; defaults to `true`
          optimizeBorderRadius: true, // controls `border-radius` property optimizations; defaults to `true`
          optimizeFilter: true, // controls `filter` property optimizations; defaults to `true`
          optimizeFont: false, // controls `font` property optimizations; defaults to `true`
          optimizeFontWeight: false, // controls `font-weight` property optimizations; defaults to `true`
          optimizeOutline: true, // controls `outline` property optimizations; defaults to `true`
          removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
          removeNegativePaddings: true, // controls removing negative paddings; defaults to `true`
          removeQuotes: true, // controls removing quotes when unnecessary; defaults to `true`
          removeWhitespace: false, // controls removing unused whitespace; defaults to `true`
          replaceMultipleZeros: true, // contols removing redundant zeros; defaults to `true`
          replaceTimeUnits: true, // controls replacing time units with shorter values; defaults to `true`
          replaceZeroUnits: true, // controls replacing zero values with units; defaults to `true`
          roundingPrecision: false, // rounds pixel values to `N` decimal places; `false` disables rounding; defaults to `false`
          selectorsSortingMethod: 'standard', // denotes selector sorting method; can be `'natural'` or `'standard'`, `'none'`, or false (the last two since 4.1.0); defaults to `'standard'`
          specialComments: 'all', // denotes a number of /*! ... */ comments preserved; defaults to `all`
          tidyAtRules: false, // controls at-rules (e.g. `@charset`, `@import`) optimizing; defaults to `true`
          tidyBlockScopes: false, // controls block scopes (e.g. `@media`) optimizing; defaults to `true`
          tidySelectors: false, // controls selectors optimizing; defaults to `true`,
          transform: function () {
          } // defines a callback for fine-grained property optimization; defaults to no-op
        },
        2: {
          mergeAdjacentRules: true, // controls adjacent rules merging; defaults to true
          mergeIntoShorthands: true, // controls merging properties into shorthands; defaults to true
          mergeMedia: true, // controls `@media` merging; defaults to true
          mergeNonAdjacentRules: true, // controls non-adjacent rule merging; defaults to true
          mergeSemantically: false, // controls semantic merging; defaults to false
          overrideProperties: true, // controls property overriding based on understandability; defaults to true
          removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
          reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
          removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
          removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
          removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
          removeUnusedAtRules: false, // controls unused at rule removing; defaults to false (available since 4.1.0)
          restructureRules: false, // controls rule restructuring; defaults to false
          skipProperties: [] // controls which properties won't be optimized, defaults to `[]` which means all will be optimized (since 4.1.0)
        }
      },
      format: 'beautify'
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./components'))
});

gulp.task('appStyles', function(){
  return gulp.src(['./assets/app/css/styles.scss'])
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(gcmq())
    .pipe(cleanCSS({
      level: {
        1: {
          cleanupCharsets: true, // controls `@charset` moving to the front of a stylesheet; defaults to `true`
          normalizeUrls: true, // controls URL normalization; defaults to `true`
          optimizeBackground: true, // controls `background` property optimizations; defaults to `true`
          optimizeBorderRadius: true, // controls `border-radius` property optimizations; defaults to `true`
          optimizeFilter: true, // controls `filter` property optimizations; defaults to `true`
          optimizeFont: false, // controls `font` property optimizations; defaults to `true`
          optimizeFontWeight: false, // controls `font-weight` property optimizations; defaults to `true`
          optimizeOutline: true, // controls `outline` property optimizations; defaults to `true`
          removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
          removeNegativePaddings: true, // controls removing negative paddings; defaults to `true`
          removeQuotes: true, // controls removing quotes when unnecessary; defaults to `true`
          removeWhitespace: false, // controls removing unused whitespace; defaults to `true`
          replaceMultipleZeros: true, // contols removing redundant zeros; defaults to `true`
          replaceTimeUnits: true, // controls replacing time units with shorter values; defaults to `true`
          replaceZeroUnits: true, // controls replacing zero values with units; defaults to `true`
          roundingPrecision: false, // rounds pixel values to `N` decimal places; `false` disables rounding; defaults to `false`
          selectorsSortingMethod: 'standard', // denotes selector sorting method; can be `'natural'` or `'standard'`, `'none'`, or false (the last two since 4.1.0); defaults to `'standard'`
          specialComments: 'all', // denotes a number of /*! ... */ comments preserved; defaults to `all`
          tidyAtRules: false, // controls at-rules (e.g. `@charset`, `@import`) optimizing; defaults to `true`
          tidyBlockScopes: false, // controls block scopes (e.g. `@media`) optimizing; defaults to `true`
          tidySelectors: false, // controls selectors optimizing; defaults to `true`,
          transform: function () {
          } // defines a callback for fine-grained property optimization; defaults to no-op
        },
        2: {
          mergeAdjacentRules: true, // controls adjacent rules merging; defaults to true
          mergeIntoShorthands: true, // controls merging properties into shorthands; defaults to true
          mergeMedia: true, // controls `@media` merging; defaults to true
          mergeNonAdjacentRules: true, // controls non-adjacent rule merging; defaults to true
          mergeSemantically: false, // controls semantic merging; defaults to false
          overrideProperties: true, // controls property overriding based on understandability; defaults to true
          removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
          reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
          removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
          removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
          removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
          removeUnusedAtRules: false, // controls unused at rule removing; defaults to false (available since 4.1.0)
          restructureRules: false, // controls rule restructuring; defaults to false
          skipProperties: [] // controls which properties won't be optimized, defaults to `[]` which means all will be optimized (since 4.1.0)
        }
      },
      format: 'beautify'
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./assets/app/css/'))
});

gulp.task('bootstrapStyles', function(){
  return gulp.src(['./components/common-styles/bootstrap/bootstrap.component.scss'])
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./components/common-styles/bootstrap/'))
});

const buildAssetsSprites = gulp.series(
  gulp.parallel(
    tasks.buildPNGSprites,
    tasks.buildSVGSprites,
  ),
  tasks.copyFiles.bind(null, {
    src: paths.storybookPNGSprites.src,
    dest: paths.storybookPNGSprites.dest,
    newer: true,
  }),
);

gulp.task('buildSprites', gulp.series(
  buildAssetsSprites,
));

gulp.task('splitCSS', function (done) {
  gulp.src('./components/05-pages/forum-post/forum-post.mobile.component.css')
    .pipe(splitMediaQueries({
      breakpoint: 768,
    }))
    .pipe(gulp.dest('./components/05-pages/forum-post/split'));

  gulp.src('./components/05-pages/forum-post/forum-post.mobile.user.component.css')
    .pipe(splitMediaQueries({
      breakpoint: 768,
    }))
    .pipe(gulp.dest('./components/05-pages/forum-post/split'));

  gulp.src('./components/05-pages/forum-post/forum-post.component.css')
    .pipe(
      postCSS([
        require('postcss-remove-media-query-ranges')({
          min: 768,
          removeMax: true
        })
      ])
    )
    .pipe(gulp.dest('./components/05-pages/forum-post/split'));

  gulp.src('./components/05-pages/forum-post/forum-post.user.component.css')
    .pipe(
      postCSS([
        require('postcss-remove-media-query-ranges')({
          min: 768,
          removeMax: true
        })
      ])
    )
    .pipe(gulp.dest('./components/05-pages/forum-post/split'));

  done();
});

gulp.task('splitBootstrap', function (done) {
  gulp.src('./components/common-styles/bootstrap/bootstrap.component.css')
    .pipe(splitMediaQueries({
      breakpoint: 768,
    }))
    .pipe(gulp.dest('./components/common-styles/bootstrap/split'));

  done();
});

gulp.task('minimizeCSSOp', function (done) {
  let params = {
    src: [
      './components/05-pages/forum-post/*.css',
    ],
    dest: './components/05-pages/forum-post/',
  };
  gulp
    .src(params.src)
    .pipe(cleanCSS())
    .pipe(gulp.dest(params.dest));

  let paramsSplit = {
    src: [
      './components/05-pages/forum-post/split/*.css',
    ],
    dest: './components/05-pages/forum-post/split/',
  };
  gulp
    .src(paramsSplit.src)
    .pipe(cleanCSS())
    .pipe(gulp.dest(paramsSplit.dest));

  done();
});
