//import { addDecorator } from '@storybook/react';
//import { useEffect } from '@storybook/client-api';
import Twig from 'twig';
import { setupTwig } from './setupTwig';

// Theming
import freeflowTheme from './wsoTheme';

// GLOBAL CSS
//import '../components/style.css';

// If in a Drupal project, it's recommended to import a symlinked version of drupal.js.
import './_drupal.js';

const customViewports = {
  mobile: {
    name: 'Mobile',
    styles: {
      width: '375px',
      height: '667px',
    },
  },
  smallTablet: {
    name: 'Small Tablet',
    styles: {
      width: '768px',
      height: '1024px',
    },
  },
  largeTablet: {
    name: 'Large Tablet',
    styles: {
      width: '1024px',
      height: '1366px',
    },
  },
};

export const parameters = {
  options: {
    theme: freeflowTheme,
    storySort: {
      method: 'alphabetical',
      order: ['Base', 'Atoms', 'Molecules', 'Organisms', 'Templates', 'Pages', 'Drupal']
    }
  },
  viewport: { viewports: customViewports },
};

setupTwig(Twig);
