const { namespaces } = require('./setupTwig');

module.exports = async ({ config }) => {
  // Twig
  config.module.rules.push({
    test: /\.twig$/,
    use: [
      {
        loader: 'twig-loader',
        options: {
          twigOptions: {
            namespaces,
          },
        },
      },
    ],
  });

  // SVG
  config.module.rules.push({
    test: /icons\/.*\.svg$/, // your icons directory
    loader: 'svg-sprite-loader',
    options: {
      extract: false,
      spriteFilename: '../dist/icons.svg',
    },
  });

  // YAML
  config.module.rules.push({
    test: /\.ya?ml$/,
    loader: 'js-yaml-loader',
  });

  return config;
};
