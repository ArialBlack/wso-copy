const path = require('path');

module.exports = {
  "core": {
    "builder": {
      "name": "@storybook/builder-webpack5",
      "options": {
        "fsCache": true,
        "lazyCompilation": true,
      },
    },
  },
  "stories": [
    "../components/**/*.stories.mdx",
    "../components/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-a11y",
    "@storybook/addon-controls",
    "@storybook/addon-postcss"
  ],
  "staticDirs": [
    "../assets"
  ],
  webpackFinal: async (config) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      '@': path.resolve(__dirname, '../components'),
    };
    return config;
  },
}
