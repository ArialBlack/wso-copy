import wysiwyg from './wysiwyg/wysiwyg.twig';
import './wysiwyg/wysiwyg.component.css';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Text' };

export const WYSIWYG = () => (
  wysiwyg()
);
