import colors from './colors.twig';
import './colors.component.css';

/**
 * Storybook Definition.
 */
export default { title: 'Base/Colors' };

export const Colors = () => (
  colors()
);
