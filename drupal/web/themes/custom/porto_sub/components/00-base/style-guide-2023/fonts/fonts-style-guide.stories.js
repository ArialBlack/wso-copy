import fonts from './fonts-style-guide.twig';
import './fonts.component.css';

/**
 * Storybook Definition.
 */
export default { title: 'Base/Style-Guide-2023/Fonts' };

export const fonts_preview = () => (
  fonts()
);
