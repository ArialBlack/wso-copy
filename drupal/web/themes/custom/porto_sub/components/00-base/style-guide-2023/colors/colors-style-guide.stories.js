import colors from './colors-style-guide.twig';
import './colors.component.css';

/**
 * Storybook Definition.
 */
export default { title: 'Base/Style-Guide-2023/Colors' };

export const colors_preview = () => (
  colors({
    ...args
  })
);

const args = {
  colorMonkeyBlue: '#0a4059',
  colorOasisBlue: '#189eda',
  colorLinkBlue100: '#087dfd',
  colorLinkBlueDark: '#0C6AD0',
  colorLinkBlueDarker: '#085ebd',
  colorLinkBlue50: 'rgba(8, 125, 253, 0.5)',
  colorLinkBlue15: 'rgba(8, 125, 253, 0.15)',
  colorLinkBlue10: 'rgba(8, 125, 253, 0.1)',
  colorMonkeyGrey: '#9db2bc',
  colorLightOasis: '#b7e1f3',
  colorBlushGrey: '#f5f7f8',
  colorGreen: '#08c42a',
  colorYellow: '#f5b226',
  colorRed: '#f04944',
  colorDark100: '#2F2F2F',
  colorDark70: 'rgba(47, 47, 47, 0.7)',
  colorDark50: 'rgba(47, 47, 47, 0.5)',
  colorDark30: 'rgba(47, 47, 47, 0.3)',
  colorDark10: 'rgba(47, 47, 47, 0.1)',
  colorWhite100: '#FFF',
  colorWhite70: 'rgba(255, 255, 255, 0.7)',
  colorWhite50: 'rgba(255, 255, 255, 0.5)',
  colorWhite30: 'rgba(255, 255, 255, 0.3)',
  colorWhite10: 'rgba(255, 255, 255, 0.1)',
}
