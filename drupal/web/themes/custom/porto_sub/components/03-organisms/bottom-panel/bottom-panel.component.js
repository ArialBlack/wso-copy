(function ($) {
  Drupal.behaviors.wsoBottomPanel = {
    attach: function (context, settings) {

      if (!window.matchMedia("(max-width: 992px)").matches) {
        return;
      }

      //Bottom panel
      $('#wso-bottom-panel', context).once('wsoBottomPanel').each(function () {
        var $bottomPanel = $(this),
          isMobile,
          $body = $('body'),
          targetButtons,
          resizeTimer,
          scrollTimer,
          documentHeight,
          scrolledPosition,
          currentUrl,
          isInit = false,

          init = function () {
            documentHeight = $(document).height();
            scrolledPosition = $(document).scrollTop();
            currentUrl = window.location.pathname;

            var $forumButtons = $bottomPanel.find('.forum-button a'),
              len = $forumButtons.length;

            for (var i = 0; i < len; i++) {
              var _this = $forumButtons[i];

              if (_this.getAttribute('href') === currentUrl) {
                $(_this).closest('.forum-button').addClass('active');
              }
            }

            switchPanels();
          },

          checkInit = function () {
            if (!isInit) {
              init();
              isInit = true;
            }
          },

          checkScroll = function () {
            scrolledPosition = $(document).scrollTop();
            switchPanels();
          },

          checkResize = function () {
            documentHeight = $(document).height();
            checkInit();
          },

          switchPanels = function () {
            var triggerScrollPosition = 300;

            if (scrolledPosition >= triggerScrollPosition) {
              // Show bottom panel, hide header
              $body.addClass('show-bottom-panel');
            } else {
              if (!$('body').hasClass('mobile-forum-menu-from-bottom')) {
                // Show header, hide bottom panel
                $body.removeClass('show-bottom-panel');
              }
            }

            //if ($bottomPanel.is(':visible')) $body.css('padding-bottom', $bottomPanel.height());
            //else $body.css('padding-bottom', '');
          };

        //First run, first check
        checkInit();

        $(document).scroll(function () {
          clearTimeout(scrollTimer);
          scrollTimer = setTimeout(function () {
            checkScroll();
          }, 250);
        });

        $(window).on('resize', function () {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function () {
            checkResize();
          }, 250);
        });
      });

      $('#wso-bottom-panel').find('.button-menu-trigger', context).once('wsoHeaderTrigger').click(function (e) {
          e.preventDefault();
          $('body').toggleClass('mobile-forum-menu-from-bottom');
      });
    }
  };
}(jQuery));
