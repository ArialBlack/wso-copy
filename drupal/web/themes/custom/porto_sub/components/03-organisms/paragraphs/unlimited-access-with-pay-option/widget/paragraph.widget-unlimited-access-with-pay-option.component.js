(function ($) {

  /**
   * Additional JS for the unlimited access widget in the hero banner.
   * @type {{attach: Drupal.behaviors.unlimited_access_widget.attach}}
   */
  Drupal.behaviors.unlimited_access_widget = {
    attach: function (context, settings) {
      $('.unlimited-access__content.unlimited-access__content-widget', context).once('unlimited_access_widget').each(function () {
        let $this = $(this);
        $this.find('.details__action').on('click', function (event) {
          event.preventDefault();
          // Change arrow icon.
          $(this).toggleClass('active');
          // Show the options.
          $this.find('.unlimited-access__options').toggleClass('hidden');
        });

        $this.find('.unlimited-access__option_item').on('click', function (event) {
          let $input = $(this).find('input');
          $this.find('.unlimited-access__option_item input:checked').each(function () {
            $(this).removeAttr('checked');
          });

          $input.attr('checked', 'checked');
        });
      });
    }
  };
}(jQuery));
