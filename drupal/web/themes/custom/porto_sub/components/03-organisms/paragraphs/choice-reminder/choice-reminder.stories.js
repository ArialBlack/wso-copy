export default { title: '03-Organisms/Paragraphs/Choice reminder' };

import block from './choice-reminder.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.choice-reminder.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'choice-reminder',
    isPublished: true,
    view_mode: 'default',
    bg_class: 'section--has-bg',
    item_id: '123',
    styles: `style='background-color: #1B1C1E'`,
    wsop_title: 'Test WSO paragraph title',
    wso_paragraphs: `<div class='field field-name-field-wso-paragraphs field--type-entity-reference-revisions field--label-hidden field--items'><div class='field--item'><div id='paragraph-id-8803'></div><div class='entity entity-paragraphs-item paragraphs-item-wso-paragraph-rich-text paragraph paragraph--type--wso-paragraph-rich-text paragraph--view-mode--default'><div class='formatted-text field field-name-field-wsop-rich-text field--type-text-with-summary field--label-hidden field--item'><p><span class='color-bright-blue'>This is your last chance to land those few coveted interview spots.</span></p><p>Less than 3% of applicants ever get accepted into these top finance positions. Will you be one of them... or will you just be another statistic who ends up working in a <span class='keyword_link'><a href='/company/back-office'>back office</a></span> somewhere or selling mortgages at a community bank?</p><p>We'll give&nbsp;<span class='color-bright-blue'>everything you need so you can you land position you want</span>... how to master the lingo, what to emphasize and build a rock-solid application... Our reviewers will guide you step by step through <span class='keyword_link'><a href='/career-services/resume-review-edits'>this critical resume review</a></span> process... and we'll unlock the door to the most exclusive, secretive and lucrative professions in the world.</p></div></div></div></div>`,
    first_title: `<div class='formatted-text field field-name-field-first-title field--type-text-long field--label-hidden field--item'><p class='text-align-center'><span style='font-size:24px;'><span class='color-orange'>Test CTA message</span></span></p></div>`,
    field_cta: `<div class='field field-name-field-cta field--type-link field--label-hidden field--item'><a href='/product/resume-review/buy' data-dialog-type='modal' data-dialog-options='{&quot;width&quot;:480,&quot;classes&quot;:{&quot;ui-dialog&quot;:&quot;wso-braintree-dialog&quot;}}' class='use-ajax braintree-modal' data-once='ajax'>GET STARTED NOW</a></div>`,
    background: 'true',
    bg_image_styles: '',
    bg_cover_styles: '',
    configuration: {
      provider: 'Some module'
    }
  })
);
