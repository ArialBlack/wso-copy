export default { title: '03-Organisms/Paragraphs/Fact section' };

import block from './fact-section.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.fact-section.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'fact-section',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    first_title: `<h2>Less Than 3% Get a Job in These Top Industries...<br>... Only Those with an&nbsp;<u>EDGE</u>&nbsp;Get In.</h2>`,
    wsop_rich_text: `<p>That's right. Out of every 1,000 applicants, less than 30 will get the job. The other&nbsp;<u><strong>97% will wash out</strong></u>&nbsp;and never be heard from again.</p>`,
    bottom_title: `<h3>To Get The Job, You Must Ace Step 1...<br>... Getting Your Resume&nbsp;<u>100% Ready</u>.</h3>`,
    configuration: {
      provider: 'Some module'
    }
  })
);
