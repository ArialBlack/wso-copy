export default { title: '03-Organisms/Paragraphs/sticky-mobile-banner' };

import block from './sticky-mobile-banner.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.sticky-mobile-banner.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'sticky-mobile-banner',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    title: '<div class="field field-name-field-title field--type-string field--label-hidden field--item">The new WSO Course</div>',
    price: '<div class="field field-name-field-price-optional field--type-string field--label-hidden field--item">$297</div>',
    cta: '<div class="field field-name-field-cta field--type-link field--label-hidden field--item"><a href="#">Add to Cart</a></div>',
    configuration: {
      provider: 'Some module'
    }
  })
);
