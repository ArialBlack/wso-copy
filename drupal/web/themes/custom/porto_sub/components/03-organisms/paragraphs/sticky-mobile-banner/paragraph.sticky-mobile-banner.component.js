(function ($) {

  /**
   * The script to make mobile banner sticky if page scrolled.
   * @type {{attach: Drupal.behaviors.sticky_mobile_banner.attach}}
   */
  Drupal.behaviors.sticky_mobile_banner = {
    attach: function (context, settings) {
      $('.paragraphs-item-sticky-mobile-banner', context).once('sticky_mobile_banner').each(function () {
        let $paragraph = $(this);

        $(window).scroll(function() {
          var scroll = $(window).scrollTop();

          if (scroll >= 300 && $paragraph.hasClass('hidden')) {
            var header = $('header'),
                headerHeight = header.outerHeight();

            $paragraph.css('top', headerHeight);
            $paragraph
              .removeClass('hidden')
              .addClass('sticky');
          }
          else if (scroll <= 300 && $paragraph.hasClass('sticky')) {
            $paragraph
              .removeClass('sticky')
              .addClass('hidden');
          }
        });
      });
    }
  };
}(jQuery));
