export default { title: '03-Organisms/Paragraphs/Site-Stats' };

import paragraph from './site-stats.twig';
import "./paragraph.site-stats.component.css";

export const default_paragraph = () => (
  paragraph({
    content: '<div class="formatted-text field field-name-field-stats field--type-text-long field--label-hidden row field--items"">\n' +
      '      <div class="field--item col-lg-6">\n' +
      '        <h3 class="topic-title"><a href="/">936,598+</a></h3>\n' +
      '        <p><a href="/">Happy Members</a></p>\n' +
      '      </div>\n' +
      '      <div class="field--item col-lg-6">\n' +
      '        <h3 class="topic-title">600,000+</h3>\n' +
      '        <p>Forum Discussions</p>\n' +
      '      </div>\n' +
      '      <div class="field--item col-lg-6">\n' +
      '        <h3 class="topic-title">3+ million</h3>\n' +
      '        <p>Comments</p>\n' +
      '      </div>\n' +
      '      <div class="field--item col-lg-6">\n' +
      '        <h3 class="topic-title">10+ million</h3>\n' +
      '        <p>Bananas Earned</p>\n' +
      '      </div>\n' +
      '    </div>',
  })
);
