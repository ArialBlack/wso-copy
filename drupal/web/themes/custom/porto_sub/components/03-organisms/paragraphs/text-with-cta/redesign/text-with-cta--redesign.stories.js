export default { title: '03-Organisms/Paragraphs/Text with CTA (Redesign)' };

import block from './text-with-cta--redesign.twig';
import drupalAttribute from 'drupal-attribute'
import page from '@/05-pages/storybook-page-template/page.twig';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'text-with-cta',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    field_text: `<div class="field-name-field-wsop-rich-text">Rich text for text-with-cta</div>`,
    // field_cta: `<div class="field field-name-field-cta field--type-link field--label-hidden field--item"><a href="/product/excel-modeling-course/buy" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:480,&quot;classes&quot;:{&quot;ui-dialog&quot;:&quot;wso-braintree-dialog&quot;}}" class="use-ajax braintree-modal" data-once="ajax">ENROLL NOW</a></div>`,
    configuration: {
      provider: 'Some module'
    },
  })
);

export const block_view = (args) => page ({ default_block });

block_view.args = {
  default_block,
}
