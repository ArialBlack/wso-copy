export default { title: '03-Organisms/Paragraphs/Short course overview' };

import block from './complex-course-overview.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.complex-course-overview.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'short-course-overview',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    number_of_courses: '6 Courses',
    number_of_lessons: '555 Lessons',
    course_duration: '50+ Hours',
    number_of_exercises: '100+ Exercises',
    number_of_students: '111,222 Students',
    configuration: {
      provider: 'Some module'
    }
  })
);
