export default { title: '03-Organisms/Paragraphs/Wide low jumbotron' };

import block from './wide-low-jumbotron.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.wide-low-jumbotron.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: "",
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'wide-low-jumbotron',
    isPublished: true,
    view_mode: 'default',
    default_image: false,
    paragraph_image: 'paragraph_image',
    paragraph_title: 'content.field_paragraph_title',
    paragraph_text: 'content.field_paragraph_text',
    search_enabled: 'search_enabled',
    show_breadcrumbs: false,
    show_mentor: true,
    mentor_profile_head_content: 'mentor_profile_head_content',
    configuration: {
      provider: "Some module"
    }
  })
);
