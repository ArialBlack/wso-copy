export default { title: '03-Organisms/Paragraphs/WSO paragraph title' };

import block from './wsop-title.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.wsop-title.component.css';



export const default_block = () => (
  block({
    drupal_theme_path: "",
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'wso-paragraph-title',
    isPublished: true,
    view_mode: 'default',
    item_id: "123",
    wsop_title: "WSO paragraph title",
    configuration: {
      provider: "Some module"
    }
  })
);
