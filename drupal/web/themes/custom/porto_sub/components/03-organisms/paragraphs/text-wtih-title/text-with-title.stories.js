export default { title: '03-Organisms/Paragraphs/Text with title' };

import block from './text-with-title.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.text-and-title.component.css';
import page from '@/05-pages/storybook-page-template/page.twig';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'text-with-title',
    paragraph_type: `callout`,
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    paragraph_theme_color: '',
    paragraph_light_theme_color: '',
    paragraph_bg_theme_color: '',
    title: `<h3 class="field-name-field-wsop-title">Test title</h3>`,
    rich_text: `<div class="field-name-field-wsop-rich-text">Test text</div>`,
    configuration: {
      provider: 'Some module'
    }
  })
);

export const block_view = (args) => page ({ default_block });

block_view.args = {
  default_block,
}
