(function ($) {

  /**
   * The script to open/close course details.
   * @type {{attach: Drupal.behaviors.course_with_details.attach}}
   */
  Drupal.behaviors.course_with_details = {
    attach: function (context, settings) {
      $('.course-with-details', context).once('course_with_details').each(function () {
        let $paragraph = $(this);

        // Open/close course details.
        $paragraph.find('.course-with-details__button a').click(function (event) {
          event.preventDefault();
          $paragraph.toggleClass('course-with-details--open');

          $paragraph
            .find('.course-with-details__button a')
            .toggleClass('active');

          $paragraph
            .find('.course-with-details__hidden-section')
            .toggleClass('hidden');
        });
      });
    }
  };
}(jQuery));
