export default { title: '03-Organisms/Paragraphs/Course introduction' };

import block from './course-introduction.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.course-introduction.component.css';
import {default_block as wsopTitle } from "../wso-paragraph-title/wsop-title.stories";
import {default_block as courseOverview } from "../course-overview/short/short-course-overview.stories";
import {default_block as cta } from "../text-with-cta/text-with-cta.stories";
import page from '@/05-pages/storybook-page-template/page.twig';
import '@/05-pages/storybook-page-template/page.component.css';

// Test image for stories. Render from staticDirs. Look at .storybook/main.js
const wsopImage = `<img src="/images/logo.jpg" alt="alt-img" />`;

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'course-introduction',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    wsop_title: wsopTitle,
    course_overview: courseOverview,
    wsop_image: wsopImage,
    list_title: 'test modeling course preview',
    paragraph_text: '',
    wso_paragraphs: `<div class='field field-name-field-wso-paragraphs field--type-entity-reference-revisions field--label-hidden field--items'><div class='field--item'><div id='paragraph-id-8803'></div><div class='entity entity-paragraphs-item paragraphs-item-wso-paragraph-rich-text paragraph paragraph--type--wso-paragraph-rich-text paragraph--view-mode--default'><div class='formatted-text field field-name-field-wsop-rich-text field--type-text-with-summary field--label-hidden field--item'><p><span class='color-bright-blue'>This is your last chance to land those few coveted interview spots.</span></p><p>Less than 3% of applicants ever get accepted into these top finance positions. Will you be one of them... or will you just be another statistic who ends up working in a <span class='keyword_link'><a href='/company/back-office'>back office</a></span> somewhere or selling mortgages at a community bank?</p><p>We'll give&nbsp;<span class='color-bright-blue'>everything you need so you can you land position you want</span>... how to master the lingo, what to emphasize and build a rock-solid application... Our reviewers will guide you step by step through <span class='keyword_link'><a href='/career-services/resume-review-edits'>this critical resume review</a></span> process... and we'll unlock the door to the most exclusive, secretive and lucrative professions in the world.</p></div></div></div></div>`,
    first_title: `<div class='formatted-text field field-name-field-first-title field--type-text-long field--label-hidden field--item'><p class='text-align-center'><span style='font-size:24px;'><span class='color-orange'>Test CTA message</span></span></p></div>`,
    wsop_cta: cta,
    configuration: {
      provider: 'Some module'
    }
  })
);

export const block_view = (args) => page ({ default_block });

block_view.args = {
  default_block,
}
