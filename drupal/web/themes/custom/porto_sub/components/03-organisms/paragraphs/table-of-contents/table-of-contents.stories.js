export default { title: '03-Organisms/Paragraphs/Table of contents' };

import block from './table-of-contents.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.toc.component.css';
import {default_block as wsopTitle } from "../wso-paragraph-title/wsop-title.stories";

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'table-of-contents',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    wsop_title: wsopTitle,
    paragraph_text: `<ul><li> <a href=\'#\'>Excel Modeling Course</a></li><li> <a href=\'#\'>Financial Statement Modeling Course</a></li></ul>`,
    configuration: {
      provider: 'Some module'
    }
  })
);

