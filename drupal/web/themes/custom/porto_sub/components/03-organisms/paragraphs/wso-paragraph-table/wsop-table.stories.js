export default { title: '03-Organisms/Paragraphs/WSO paragraph table' };

import block from './wsop-table.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.wsop-table.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'wso-paragraph-table',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    header_type: '1',
    content: `
    <div class='entity entity-paragraphs-item paragraphs-item-wso-paragraph-table paragraph paragraph--type--wso-paragraph-table paragraph--view-mode--default header-type-1'><div class='formatted-text field field-name-field-wsop-table field--type-tablefield field--label-hidden field--item'><div id='tablefield-wrapper-paragraph-3048-field_wsop_table-0' class='tablefield-wrapper'><table id='tablefield-paragraph-3048-field_wsop_table-0' class='tablefield table' data-striping='1'><thead><tr><th class='row_0 col_0'><p>This Online Excel Class Includes:</p></th><th class='row_0 col_1'><p>Value</p></th></tr></thead><tbody><tr class='odd'><td class='row_1 col_0'><p>WSO Excel Modeling Course</p><p>110+ video lessons across 13 Modules taught by a top-bucket bulge bracket investment banker...</p></td><td class='row_1 col_1'><p>$500</p></td></tr><tr class='even'><td class='row_2 col_0'><p>10 Interactive Exercises with Benchmarks</p><p>Gain realistic practice drilling the concepts taught in the course with timed benchmarks so that you can ...</p></td><td class='row_2 col_1'><p>$450</p></td></tr><tr class='odd'><td class='row_3 col_0'><p>24 Months of Unlimited Elite Support from Actual Finance Pros</p><p>Have a technical question? Easily drop a comment into any lesson and get a response from a pro within 48hrs.</p></td><td class='row_3 col_1'><p>$300</p></td></tr><tr class='even'><td class='row_4 col_0'><p>6 Months of FREE Access to Macabacus</p><p>Full access to this Excel plug-in that makes financial modeling even easier, including tools for PowerPoint and Word.</p></td><td class='row_4 col_1'><p>$120</p></td></tr><tr class='odd'><td class='row_5 col_0'><p>TOTAL VALUE</p></td><td class='row_5 col_1'><p>$1,370</p></td></tr></tbody></table></div></div><div class='field field-name-field-wsop-table-header-type field--type-list-integer field--label-hidden field--item'>1</div></div>
    `,
    configuration: {
      provider: 'Some module'
    }
  })
);
