(function ($) {

  /**
   * Additional JS for the scrolling to unlimited access full container.
   * @type {{attach: Drupal.behaviors.scroll_to_unlimited_access.attach}}
   */
  Drupal.behaviors.scroll_to_unlimited_access = {
    attach: function (context, settings) {
      $('.card-with-discount__instead-action', context).once().each(function () {
        $('.link-scroll', context).click(function(e) {
          e.preventDefault();
          var offset = $('.unlimited-access__content-full', context).offset().top;
          $('html, body').animate({
            scrollTop: offset - 200
          }, 500);
        });
      });
    }
  };
}(jQuery));
