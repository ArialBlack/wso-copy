export default { title: '03-Organisms/Paragraphs/Short course overview' };

import block from './short-course-overview.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.short-course-overview.component.css';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'short-course-overview',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    number_of_lessons: '555 Lessons',
    course_duration: '9h 45m',
    number_of_students: '111,222 Students',
    star_rate: 4.9,
    review_star: 4,
    four_half_star: 'tp-stars--4--half',
    number_of_reviews: '999 Reviews',
    configuration: {
      provider: 'Some module'
    }
  })
);
