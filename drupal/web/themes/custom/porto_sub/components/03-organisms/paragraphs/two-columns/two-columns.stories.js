export default { title: '03-Organisms/Paragraphs/Two columns' };

import block from './two-columns.twig';
import drupalAttribute from 'drupal-attribute'
import './paragraph.two-columns.component.css';
import page from '@/05-pages/storybook-page-template/page.twig';

export const default_block = () => (
  block({
    drupal_theme_path: '',
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'two-columns',
    isPublished: true,
    view_mode: 'default',
    item_id: '123',
    grid: '50',
    reverse_order: false,
    first_column_gap_classes: 'first_column_gap_classes',
    second_column_gap_classes: 'second_column_gap_classes',
    paragraph_title: `<div>Paragraph title</div>`,
    wso_paragraphs: `<div>wso_paragraphs</div>`,
    wsop_paragraphs_in_block: `<div>wsop_paragraphs_in_block</div>`,
    configuration: {
      provider: 'Some module'
    },
  })
);

export const block_view = (args) => page ({ default_block });

block_view.args = {
  default_block,
}
