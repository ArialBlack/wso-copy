(function ($) {

  /**
   * The script to stick dynamic header.
   * @type {{attach: Drupal.behaviors.dynamic_header.attach}}
   */
  Drupal.behaviors.dynamic_header = {
    attach: function (context, settings) {
      $('body:not(.page-node-edit) .dynamic-header-block', context).once('dynamic_header').each(function () {
        let $header = $(this);
        let headerTop = $header.offset().top;
        let $widget = $('.hero-banner__floating-widget');
        // Get the full width container and top coordinate of it.
        let $fullWidthContainer = $('.labeled-container-wrapper__size-full_screen', context).first();
        let fullWidthContainerTop = $fullWidthContainer.offset().top - $widget.outerHeight(true) - 200;

        $(document).ready(function () {
          // Show or hide the sticky menu.
          let scrollPos = $(document).scrollTop();
          processHeaderWidgets(scrollPos);
          updateMenuState(scrollPos);
        });

        var timer;
        $(window).resize(function () {
          clearTimeout(timer);
          timer = setTimeout(function () {
            // Show or hide the sticky menu.
            let scrollPos = $(document).scrollTop();
            processHeaderWidgets(scrollPos);
          }, 100);
        });

        $(window).scroll(function(){
          // Show or hide the sticky menu.
          let scrollPos = $(document).scrollTop();
          processHeaderWidgets(scrollPos);
          updateMenuState(scrollPos);
        });

        /**
         * Activate dynamic menu and floating widget.
         *
         * @param scrollPos
         *   The current scroll position.
         */
        function processHeaderWidgets(scrollPos) {
          let headStickyState = $header.hasClass('dynamic-header__sticky');
          let widgetStickyState = $widget.hasClass('hero-banner__floating-widget__sticky');
          let widgetFixedState = $widget.hasClass('hero-banner__floating-widget__fixed');

          // Set the header and widget sticky.
          // In addition, set the widget position.
          if (scrollPos >= headerTop && (!headStickyState || !widgetStickyState) && !widgetFixedState) {
            $header.addClass('dynamic-header__sticky');
            $widget.addClass('hero-banner__floating-widget__sticky');

            // Set the widget position.
            let diff = $('.dialog-off-canvas-main-canvas').offset().top / 1.85;
            let top = headerTop - 68 + diff;

            $widget.css({
              'top': top + 'px',
              'right': $header.offset().left + 'px'
            });
          // Remove the header and widget sticky.
          } else if (scrollPos <= headerTop && headStickyState) {
            $header.removeClass('dynamic-header__sticky');
            $widget.removeClass('hero-banner__floating-widget__sticky');
            $widget.removeClass('hero-banner__floating-widget__fixed');
          }

          // Set the widget fixed if scrolled to the full width container.
          if ($fullWidthContainer.length) {
            if (scrollPos >= fullWidthContainerTop && !widgetFixedState) {
              $widget.removeClass('hero-banner__floating-widget__sticky');
              $widget.addClass('hero-banner__floating-widget__fixed');
              $widget.css('top', fullWidthContainerTop + 'px');
            } else if (scrollPos <= fullWidthContainerTop) {
              $widget.removeClass('hero-banner__floating-widget__fixed');
            }
          }
        }

        /**
         * Update menu items state.
         *
         * @param scrollPos
         *   The current scroll position.
         */
        function updateMenuState(scrollPos) {
          // Activate menu item if user scrolled to section.
          $('.dynamic-header-block__menu li a', $header).each(function () {
            let $item = $(this);
            let href = $item.attr('href');
            let position = href.indexOf('#');
            let $active = $('.dynamic-header-block__menu li a.active', $header);
            if (position === -1) {
              return;
            }

            let section_id = href.substr(position);
            let section = $(section_id);

            if (!section.length) {
              return;
            }

            if (section.offset().top <= scrollPos + 200) {
              if ($item === $active) {
                return;
              }

              $('.dynamic-header-block__menu li a', $header).removeClass('active');
              $item.addClass('active');
            }
          });
        }
      });
    }
  };
}(jQuery));
