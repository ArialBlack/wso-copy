import buttonTwig from './button.twig';
import './button.component.css';

/**
 * Storybook Definition.
 */
export default {
  title: '01-Atoms/Style-Guide-2023/Button',
  argTypes: {
    variation: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'tertiary'],
      },
    },
    size: {
      control: {
        type: 'select',
        options: ['xxlg', 'xlg', 'lg', 'md', 'sm'],
      },
    },
    disabled: {
      control: {
        type: 'select',
        options: ['disabled', 'none']
      },
    },
    icon: {
      control: {
        type: 'select',
        options: ['btn-icon-left', 'btn-icon-right', 'none']
      },
    },
  },
};

const button = ({ variation, size, url, title, disabled, icon }) => (
  buttonTwig({
    url,
    title,
    btnClasses: ['btn--' + variation + '-' + size + ' ' + disabled + ' ' + icon]
  })
);

export const button_preview = button.bind({});

button_preview.args = {
  title: 'Button',
  url: '#',
  variation: 'primary',
  size: 'lg',
  disabled: 'none',
  icon: '',
};
