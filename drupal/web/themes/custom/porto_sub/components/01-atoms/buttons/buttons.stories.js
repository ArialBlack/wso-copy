import buttonsList from "./buttons.twig";
import "./buttons.component.css";

export default {
  title: "01-Atoms/Button"
};

export const ButtonsList = () => (
  buttonsList()
);
