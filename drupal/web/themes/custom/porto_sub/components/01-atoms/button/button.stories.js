import twigButton from "./button.twig";

export default {
  title: "01-Atoms/Button",
  argTypes: {
    type: {
      control: {
        type: "inline-radio",
        options: ["button", "submit"],
      },
    },
    bgColor: {
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Red: 'red'
        }
      },

    }
  },
};

const Template = ({ text, type, bgColor }) =>
  twigButton({
    text,
    type,
    bgColor,
  });

export const DefaultButton = Template.bind({});

DefaultButton.args = {
  text: "Button",
  type: "button",
  bgColor: "black"
};
