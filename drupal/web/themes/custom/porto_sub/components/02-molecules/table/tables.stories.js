import tablesList from "./tables.twig";

export default {
  title: "02-Molecules/Table"
};

export const TablesList = () => (
  tablesList()
);
