(function ($) {

  /**
   * Apply timer for the countdown timer component.
   * @type {{attach: Drupal.behaviors.countdown_timer_base.attach}}
   */
  Drupal.behaviors.countdown_timer_base = {
    attach: function (context, settings) {
      $('.countdown-timer-wrapper', context).once('countdown_timer').each(function () {
        let $timer = $(this);
        let days, hours, minutes, seconds;

        // Set the date we're counting down to
        let resetDate = new Date();
        resetDate.setDate(resetDate.getUTCDate() + (13 - resetDate.getUTCDay()) % 7);
        resetDate.setUTCHours(7, 59, 0, 0);

        let $counter = $('.countdown-timer__counter[data-fromat="days"]');
        if ($counter.length) {
          // Get today's date and time
          let now = new Date().getTime();
          // Find the distance between now and the reset date
          let distance = resetDate - now;

          // Time calculations for days, hours, minutes, and seconds
          days = Math.floor(distance / (1000 * 60 * 60 * 24));
          hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

          // Process the other timer format.
          let value = days > 0 ?
            Drupal.formatPlural(days, "1 day", "@days days", {"@days": days}) :
            Drupal.formatPlural(hours, "1 hour", "@hours hours", {"@hours": hours});

          $counter
            .find(".counter__days-hours")
            .html(value);
        }

        // Update the countdown every 1 second
        let x = setInterval(function() {
          // Get today's date and time
          let now = new Date().getTime();

          // Find the distance between now and the reset date
          let distance = resetDate - now;

          // Time calculations for days, hours, minutes, and seconds
          days = Math.floor(distance / (1000 * 60 * 60 * 24));
          hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // Reset to zero if lets are negative
          if(days < 0){
            days = 0;
          }
          if(hours < 0){
            hours = 0;
          }
          if(minutes < 0){
            minutes = 0;
          }
          if(seconds < 0){
            seconds = 0;
          }

          // Display the result in the element with id="timer"
          $timer
            .find(".countdown-timer__counter .counter__days")
            .html(days + "d");
          $timer
            .find(".countdown-timer__counter .counter__hours")
            .html(hours + "h");
          $timer
            .find(".countdown-timer__counter .counter__minutes")
            .html(minutes + "m");
          $timer
            .find(".countdown-timer__counter .counter__seconds")
            .html(seconds + "s");

          // If the countdown is finished, reset the timer
          if (distance < 0) {
            clearInterval(x);
          }
        }, 1000);
      });
    }
  };
}(jQuery));
