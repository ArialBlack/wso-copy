(function ($) {

  /**
   * Apply timer for the countdown timer component.
   * @type {{attach: Drupal.behaviors.countdown_timer_text.attach}}
   */
  Drupal.behaviors.countdown_timer_text = {
    attach: function (context, settings) {
      $('.countdown-timer', context).once('countdown_timer_text').each(function () {
        let $timer = $(this);

        // Set the date we're counting down to
        let nextFriday = new Date();
        nextFriday.setDate(nextFriday.getUTCDate() + (12 - nextFriday.getUTCDay()) % 7);

        // Set the date we're counting down to.
        let $subtitle = $timer.find('.countdown-timer__subtitle');

        if ($subtitle.length) {
          // Display the next Friday's date in the element with id="next-friday"
          let monthNames = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
          ];

          let month = monthNames[nextFriday.getMonth()];
          let date = nextFriday.getDate();
          let year = nextFriday.getFullYear();

          $subtitle.html("and <u>ends on Friday, " + month + " " + date + ", " + year + "</u>");
        }
      });
    }
  };
}(jQuery));
