(function ($) {

  /**
   * The script to open/close course details.
   * @type {{attach: Drupal.behaviors.course_with_details.attach}}
   */
  Drupal.behaviors.trustpilot_slider = {
    attach: function (context, settings) {
      $('.view-trustpilot-reviews.view-display-id-product_reviews_carousel .view-content', context).once('trustpilot_slider').each(function () {
        $(this).slick({
          dots: true,
          arrows: false,
          autoplay: true,
          centerMode: true,
          pauseOnDotsHover: true,
          slidesToShow: 7,
          slidesToScroll: 1,
          autoplaySpeed: 4000,
          responsive: [
            {
              breakpoint: 2560,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                dots: false,
              }
            }
          ]
        });
      });
    }
  };
}(jQuery));
