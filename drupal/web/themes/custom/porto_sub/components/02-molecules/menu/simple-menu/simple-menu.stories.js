
export default { title: '02-Molecules/Navigation/Menu/Simple Menu' };

import menu from './simple-menu.twig';
import drupalAttribute from 'drupal-attribute'
import './menu.simple-menu.component.css';

export const default_menu = () => (
  menu ({
    drupal_theme_path: "",
    attributes: new drupalAttribute(),
    title_attributes: new drupalAttribute(),
    bundle: 'simple-menu',
    isPublished: "true",
    menu_theme_color: "",
    menu_light_theme_color: "",
    menu_bg_theme_color: "",
    content: "<div>Test</div>",
    configuration: {
      provider: "module"
    }
  })
);