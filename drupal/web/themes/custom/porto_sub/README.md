#HOW TO: Storybook

1. Run <code>'composer install'</code> from the Drupal folder to install drupal/components module.
2. Run <code>'npm i'</code> from the current folder.
3. Take a look at 'package.json' file. There is a list of commands.
4. At the first run - exec <code>'npm run build'</code> to build sprites, style library, styles, and split CSS for the forum nodes.
5. To start development with 'watch' and 'storybook' - run <code>'npm run dev'</code> command.
6. Once you are done with fixing styles - run <code>'npm run build'</code> and commit+push your changes.

##Develop during the watch process
After running <code>'npm run dev'</code> gulp compiles all styles:
1. 'watch' task will start
2. then after the first change at any file, gulp will process all files.
3. After that, after a change at any next file, gulp will process only the changed file, thanks to gulp
v.4 feature: <code>{since: gulp.lastRun('styles')}</code>
and <code>.pipe(dependents())</code>

Since we do not process all files in 'styles' function, only *.component.scss files,
changing some styles in the included file, like 'styles.scss' for the 'footer.component.scss' would not
trigger compile process. So in order to see changes, we need to 'refresh' the parent file
'footer.component.scss'. You can just remove/add a new line. Gulp will compile only this file in the result.
https://i.imgur.com/eSl9t3a.png
It's a temporary solution. We need to find a way how to improve it.

##Tips:
####CSS weight
base key assigns a weight of CSS_BASE = -200
layout key assigns a weight of CSS_LAYOUT = -100
component key assigns a weight of CSS_COMPONENT = 0
state key assigns a weight of CSS_STATE = 100
theme key assigns a weight of CSS_THEME = 200
