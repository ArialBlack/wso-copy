<?php

namespace Drupal\hello_bar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Renders due date form.
   */
  public function helloBar($id) {
    $block = \Drupal\block_content\Entity\BlockContent::load($id);
    $block_render = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view($block);

    if (!$block_render['#block_content']->get('field_button_icon')->isEmpty()) {
      $field_button_icon = $block_render['#block_content']->get('field_button_icon')->getValue();
      $field_button_icon = $field_button_icon[0]['value'];
      $field_button_icon = Markup::create('<i class="icon ' . $field_button_icon . '"></i>');
    }

    if (!$block_render['#block_content']->get('field_background_color')->isEmpty()) {
      $field_background_color = $block_render['#block_content']->get('field_background_color')->getValue();
      $field_background_color = $field_background_color[0]['color'];
      $field_background_color = Markup::create('style="background-color:' . $field_background_color . ';"');
    }

    if (!$block_render['#block_content']->get('field_text_color')->isEmpty()) {
      $field_text_color = $block_render['#block_content']->get('field_text_color')->getValue();
      $field_text_color = $field_text_color[0]['color'];
      $field_text_color = Markup::create('style="color:' . $field_text_color . ';"');
    }

    if (!$block_render['#block_content']->get('field_button_background')->isEmpty() || !$block_render['#block_content']->get('field_button_color')->isEmpty()) {
      $button_styles = 'style="';

      if (!$block_render['#block_content']->get('field_button_background')->isEmpty()) {
        $field_button_background = $block_render['#block_content']->get('field_button_background')->getValue();
        $field_button_background = $field_button_background[0]['color'];
        $button_styles = $button_styles . 'background-color:' . $field_button_background . ';';
      }

      if (!$block_render['#block_content']->get('field_button_color')->isEmpty()) {
        $field_button_color = $block_render['#block_content']->get('field_button_color')->getValue();
        $field_button_color = $field_button_color[0]['color'];
        $button_styles = $button_styles . 'color:' . $field_button_color . ';';
      }

      $button_styles = $button_styles . '"';
      $button_styles = Markup::create($button_styles);
    }

    $bottom_link = FALSE;
    $bottom_link_title = '';
    if (!$block_render['#block_content']->get('field_bottom_link')->isEmpty()) {
      $link = $block_render['#block_content']->get('field_bottom_link')->getValue();
      $bottom_link = Url::fromUri($link[0]['uri'])->toString();
      $bottom_link_title = $link[0]["title"];
    }

    $block_render_array = [
      '#theme' => 'block__hello_bar',
      '#id' => $id,
      '#style_background_color' => $field_background_color,
      '#style_text_color' => $field_text_color,
      '#body' => Markup::create($block_render['#block_content']->get('body')->value),
      '#bottom_link' => $bottom_link,
      '#bottom_link_title' => $bottom_link_title,
      '#button_styles' => $button_styles,
      '#button_icon' => $field_button_icon,
      '#attached' => [
        'css' => [
          drupal_get_path('theme', 'porto_sub') . '/components/03-organisms/block/hello-bar/hello-bar.component.css'
        ],
      ],
    ];

    return $block_render_array;
  }

}
