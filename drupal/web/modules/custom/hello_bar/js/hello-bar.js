(function ($) {
  var $helloBar = $('.hello-bar'),
    $buttonClose = $helloBar.find('.btn-close'),
    htmlId = $helloBar.attr('id');

  if ($helloBar.length === 1) {
    if (localStorage.getItem('helloBarCookie-' + htmlId) == null) {
      $helloBar.addClass('show animated bounceInUp');
    }
    else {
      var cookieDate = Date.parse(localStorage.getItem('helloBarCookie-' + htmlId)),
        currentDate = new Date();

      cookieDate = new Date(cookieDate);

      if (currentDate > cookieDate) {
        $helloBar.addClass('show animated bounceInUp');
        localStorage.removeItem('helloBarCookie-' + htmlId);
      }
    }

    $buttonClose.on('click', function() {
      var expiryDate = new Date();
      expiryDate.setMonth(expiryDate.getMonth() + 1);
      localStorage.setItem('helloBarCookie-' + htmlId, expiryDate);
    });
  }
})(jQuery);
