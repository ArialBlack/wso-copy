(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.wsoMentorsSearchSelects = {
    attach: function (context, settings) {
      $(document).once('wsoMentorsSearchAjaxViews').ajaxSuccess(function (event, data) {
        if (drupalSettings && drupalSettings.views && drupalSettings.views.ajaxViews) {

          var ajaxViews = drupalSettings.views.ajaxViews;

          var markText = function() {
            //Text search
            var $originalInput = $('.form-item-combine input'),
              inputText = $originalInput.val();

            if (inputText.length > 0) {
              //Mark text

              var $items = $('.views-row');
              $items.mark(inputText, {
                'ignorePunctuation': [
                  ',',
                  ';',
                  '.'
                ],
                'className': 'highlighter',
                'exclude': [
                  'noscript',
                  '.btn'
                ],

                //Update search results counter
                'done': function (count) {
                  var itemsFound = 0,
                    mentorsFound = 0;

                  //We have results
                  if (count > 0) {

                    $items.each(function (index) {
                      var $item = $(this);

                      //Open all items with matched results
                      if ($item.find('.highlighter').length > 0) {
                        $item.find('.panel-collapse').show();
                      }
                    });
                  } else {
                    //Have no results
                    $items.unmark();
                  }
                }
              });
            }
          }

          Object.keys(ajaxViews || {}).forEach(function (i) {
            if (ajaxViews[i]['view_name'] == 'mentor_profile' &&  ajaxViews[i]['view_display_id'] == 'mentors_page') {

              setTimeout(function() {
                markText();
              }, 1000);
            }
          });
        }
      });
    }
  };

  Drupal.behaviors.wsoMentorsSearch = {
    attach: function (context, settings) {
      $('.mentors-sort', context).once('wsoMentorsActiveSearch').each(function (e) {
        //Set active sort
        var $sort = $(this).find('a'),
          currentSort = $('select[name="sort_by"]').val(),
          currentOrder = $('select[name="sort_order"]').val();

        $sort.each(function() {
          var $this = $(this);

          if ($this.data('sort') === currentSort && $this.data('order') == currentOrder) {
            $this.addClass('active-sort');
          }
          else {
            $this.removeClass('active-sort');
          }
        });
      });

      $('.mentors-sort a', context).once('wsoMentorsSearch').click(function (e) {
        e.preventDefault();

        var $sort = $(this),
          sortOption = $sort.data('sort'),
          sortOrder = $sort.data('order'),
          $formSubmit = $('#views-exposed-form-mentor-profile-mentors-page').find('.form-actions input[value="Apply"]');

        if (!$sort.hasClass('active-sort')) {
          $('select[name="sort_by"]').val(sortOption).trigger('change');
          $('select[name="sort_order"]').val(sortOrder).trigger('change');
          $formSubmit.click();
        }
      });

      //Set Past Industries === Current Industry
      $('select[name="field_current_industry_value[]"]', context).once('wsoMentorsSearchIndustry').each(function () {
        var $select = $(this),
          $formSubmit = $('#views-exposed-form-mentor-profile-mentors-page').find('.form-actions input[value="Apply"]');

        $select.on('select2:select', function (e) {
          e.preventDefault();
          var currentIndustry = $(this).val();
          $('select[name="field_past_industries_value[]"]').val(currentIndustry).trigger("change");
          $(this).select2('close');
          $formSubmit.click();
        });

        $select.on('select2:unselecting', function() {
          $(this).data('unselecting', true);
        }).on('select2:opening', function(e) {
          if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
            var currentIndustry = $(this).val();
            $('select[name="field_past_industries_value[]"]').val(currentIndustry).trigger("change");
            $formSubmit.click();
          }
        });

        $select.on('select2:clear', function (e) {
          $(this).select2('close');
        });
      });

      //Set Previous Cities === Current City
      $('select[name="field_current_city_value[]"]', context).once('wsoMentorsSearchCity').each(function () {
        var $select = $(this),
          $formSubmit = $('#views-exposed-form-mentor-profile-mentors-page').find('.form-actions input[value="Apply"]');

        $select.on('select2:select', function (e) {
          e.preventDefault();
          var currentIndustry = $(this).val();
          $('select[name="field_previous_cities_value[]"]').val(currentIndustry).trigger("change");
          $(this).select2('close');
          $formSubmit.click();
        });

        $select.on('select2:unselecting', function() {
          $(this).data('unselecting', true);
        }).on('select2:opening', function(e) {
          if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
            var currentIndustry = $(this).val();
            $('select[name="field_previous_cities_value[]"]').val(currentIndustry).trigger("change");
            $formSubmit.click();
          }
        });

        $select.on('select2:clear', function (e) {
          $(this).select2('close');
        });
      });

      //Search mentors grade
      $('select[name="field_date_graduation_undergrad_value[]"]', context).once('wsoMentorsSearchCity').each(function () {
        var $select = $(this),
          $formSubmit = $('#views-exposed-form-mentor-profile-mentors-page').find('.form-actions input[value="Apply"]'),

          formatState = function (state) {
            if (!state.id) {
              return state.text;
            }

            if (state.text === 'Senior Mentors') {
              var $state = $('<span class="option-value">Senior Mentors</span><span class="option-description">(Over 5+ years experience)</span>');
            } else if (state.text === 'Mentors') {
              var $state = $('<span class="option-value">Mentors</span><span class="option-description">(Under 5 years experience)</span>');
            } else {
              var $state = $('<span class="option-value">' + state.text + '</span>');
            }

            return $state;
          };

        $select.select2({
          theme: 'bootstrap4 select2-single',
          placeholder :'Select..',
          templateResult: formatState
        });

        $select.on('change', function () {
          $formSubmit.click();
        });

        $select.on('select2:unselecting', function() {
          $(this).data('unselecting', true);
        }).on('select2:opening', function(e) {
          if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
            var currentIndustry = $(this).val();
            $('select[name="field_date_graduation_undergrad_value[]"]').val(currentIndustry).trigger("change");
            $formSubmit.click();
          }
        });

        $select.on('select2:clear', function (e) {
          $(this).select2('close');
          $formSubmit.click();
        });
      });

      //Search after text input
      $('input[name="combine"]', context).once('wsoMentorsSearchCity').donetyping(function () {
        var $formSubmit = $('#views-exposed-form-mentor-profile-mentors-page').find('.form-actions input[value="Apply"]');
        $formSubmit.click();
      });
    }
  };

  $.fn.extend({
    donetyping: function (callback, timeout) {
      timeout = timeout || 1500; // 1.5 second default timeout
      var timeoutReference,
        doneTyping = function (el) {
          if (!timeoutReference) return;
          timeoutReference = null;
          callback.call(el);
        };
      return this.each(function (i, el) {
        var $el = $(el);

        $el.is(':input') && $el.on('input keyup keypress paste', function (e) {
          if (e.type == 'keyup' && e.keyCode != 8) return;

          if (timeoutReference) clearTimeout(timeoutReference);
          timeoutReference = setTimeout(function () {
            doneTyping(el);
          }, timeout);
        }).on('blur', function () {
          doneTyping(el);
        });
      });
    }
  });

  /*
  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top,
      elementBottom = elementTop + $(this).outerHeight(),
      viewportTop = $(window).scrollTop(),
      viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
  */
}(jQuery, Drupal, drupalSettings));
