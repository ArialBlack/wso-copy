<?php

namespace Drupal\help_cards\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "help_cards_lazy_block",
 *   admin_label = @Translation("Help_cards Lazy block"),
 * )
 */
class LazyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'placeholder' => [
        '#cache' => [
          'contexts' => ['user'],
        ],
        '#lazy_builder' => [
          'help_cards.help_cards_lazy_builder:renderCardsList',
          [],
        ],
      ],
    ];
  }

}
