<?php

namespace Drupal\help_cards;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Lazy builder.
 */
class HelpCardsLazyBuilder implements TrustedCallbackInterface {

  /**
   * Renderer for help_cards_node_list theme hook.
   */
  public function renderCardsList() {
    $build = [
      '#theme' => 'help_cards_cards_list',
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'renderNodeList',
    ];
  }

}
