(function ($) {
  Drupal.behaviors.helpCards = {
    attach: function (context, settings) {
      $('.field-name-field-paragraphs', context).once('helpCardsBlock').each(function () {
        var $block = $(this),
          $container = $('#help-cards'),
          $buttonClose = $block.find('.close'),
          scrollTimer,
          listId = $block.closest('article').attr('data-history-node-id'),
          showBlock = false;

        if (localStorage.getItem('helpCardsCookie-' + listId) == null) {
          showBlock = true;
        }
        else {
          var cookieDate = Date.parse(localStorage.getItem('helpCardsCookie-' + listId)),
            currentDate = new Date();

          cookieDate = new Date(cookieDate);

          if (currentDate > cookieDate) {
            showBlock = true;
            localStorage.removeItem('helpCardsCookie-' + listId);
          }
        }

        if (showBlock) {
          $(document).scroll(function () {
            clearTimeout(scrollTimer);

            scrollTimer = setTimeout(function () {
              if ($(window).scrollTop() > $(document).height() / 2) {
                $container.show().addClass('show');
              }
            }, 250);
          });

          $buttonClose.on('click', function() {
            $block.hide();
            var expiryDate = new Date();
            expiryDate.setMonth(expiryDate.getMonth() + 1);
            localStorage.setItem('helpCardsCookie-' + listId, expiryDate);
          })
        }
      });
    }
  };
})(jQuery);
