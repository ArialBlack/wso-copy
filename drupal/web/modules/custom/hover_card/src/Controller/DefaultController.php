<?php

namespace Drupal\hover_card\Controller;

use Drupal\Component\Render\HtmlEscapedText;
use Drupal\Core\Render\Markup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default controller for the hover_card module.
 */
class DefaultController extends ControllerBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a DefaultController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hoverCard(UserInterface $user = NULL) {
    $name = $user->getAccountName();
    $uid = $user->id();
    $banana_points = _userpoints_get_current_points($uid);
    $wso_credits = _userpoints_get_current_points($uid, 'wso_credits');
    $wso_karma = wso_karma_get_user_karma($uid);
    $title = 'None Selected';
    $industry = 'None Selected';

    if ($user->hasField('field_confirm_title') && !$user->get('field_confirm_title')->isEmpty()) {
      $title = $user->get('field_confirm_title')->getString();
    }

    if ($user->hasField('field_current_industry') && !$user->get('field_current_industry')->isEmpty()) {
      $industry = $user->get('field_current_industry')->getString();
    }

    if ($user->hasRole('certified_user')) {
      $certified_user = TRUE;
    }
    else {
      $certified_user = FALSE;
    }

    $user_data = [
      'name' => new HtmlEscapedText($name),
      'title' => Markup::create($title),
      'industry' => Markup::create($industry),
      'certified_user' => $certified_user,
      'wso_credits' => new HtmlEscapedText(number_format($wso_credits, 0, '.', ',')),
      'monkey_shit' => new HtmlEscapedText(number_format($wso_karma[1], 0, '.', ',')),
      'silver_bananas' => new HtmlEscapedText(number_format($wso_karma[0], 0, '.', ',')),
      'violations' => flagged_content_get_user_violations($uid),
      // That permission have administrators, moderators and content reviwers, so using it.
      'is_admin' => \Drupal::currentUser()->hasPermission('administer comments'),
      'banana_points' => new HtmlEscapedText(number_format($banana_points)),
    ];

    $hover_card_template_build = [
      '#theme' => 'hover_card_template',
      '#details' => $user_data,
      '#cache' => [
        'contexts' => ['user.permissions'],
      ],
    ];

    $hover_card_template = $this->renderer->render($hover_card_template_build);
    $response = new Response();
    $response->setContent($hover_card_template);
    return $response;
  }

}
