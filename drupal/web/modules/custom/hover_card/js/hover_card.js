(function ($, Drupal) {
  Drupal.behaviors.hover_card = {
    attach: function (context, settings) {

      $('#content a.username, #content a.mentions', context).once('hoverCard').each(function () {
        var $this = $(this),
          $hover_details = $('.hover-details'),
          base_path = settings.path.baseUrl,
          hoverTimer,

          fetchData = function($link) {
            var user_link = $link.attr('href'),
              user_id = user_link.replace(base_path, '/').split('/'),
              lastFetched = $hover_details.attr('last-user-id');

            if (lastFetched && lastFetched == user_id[2]) {
              $link.popover('show');
            } else {
              $.ajax({
                url: base_path + 'hover-card/' + user_id[2],
                beforeSend: function () {
                  $hover_details.empty();
                  $hover_details.prepend('<p style="text-align: center; margin-bottom: 0;"><img class="card-preloader" src="/themes/custom/porto_sub/images/animation/card-preloader.gif"></p>');
                  $link.popover('show');
                },
                success: function (data) {
                  $hover_details.html(data).attr('last-user-id', user_id[2]);
                },
              });
            }
          };

        $this.popover({
          content: $hover_details,
          html: true,
          container: 'body',
          placement : 'auto',
          trigger: 'manual',
          template: '<div class="popover user-profile-ajax" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>'
        });

        $this.hover(
          function() {
            //prevent quick hover in/out triggering
            hoverTimer = setTimeout(function () {
              fetchData($this);
            }, 150);
          }, function() {
            clearTimeout(hoverTimer);
            $this.popover('hide');
          }
        );
      });
    }
  };
})(jQuery, Drupal);
