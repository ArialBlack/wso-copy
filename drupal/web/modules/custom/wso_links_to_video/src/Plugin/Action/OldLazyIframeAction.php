<?php

namespace Drupal\wso_links_to_video\Plugin\Action;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a an Old Lazy Iframe Action action.
 *
 * @Action(
 *   id = "wso_links_to_video_old_lazy_iframe_action",
 *   label = @Translation("Old Lazy Iframe Action"),
 *   type = "node",
 *   category = @Translation("Custom")
 * )
 */
class OldLazyIframeAction extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($node, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $allowed = $account->hasPermission('administer wso');
    return $return_as_object ? AccessResult::allowedIf($allowed) : $allowed;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($node = NULL) {
    if (!$node) {
      return;
    }

    /** @var \Drupal\node\NodeInterface $node */
    if ($node->hasField('body')) {
      $body_vals = $node->get('body')->getValue();

      foreach ($body_vals as $delta => $body) {
        $text = $body['value'];
        $html_dom = Html::load($text);
        $iframes = $html_dom->getElementsByTagName('iframe');

        if (!$iframes->length) {
          return;
        }

        foreach ($iframes as $iframe) {
          $old_src = $iframe->getAttribute('data-echo');

          if (strlen($old_src) > 10) {
            $update = TRUE;
            $iframe->removeAttribute('data-echo');
            $iframe->setAttribute('src', $old_src);
          }
        }

        $body_vals[$delta]['value'] = Html::serialize($html_dom);
        $body_vals[$delta]['format'] = 'plain_html';
      }

      if (!empty($update)) {
        $node->set('body', $body_vals);
        $node->save();
      }
    }
  }

}
