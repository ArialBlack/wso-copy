<?php

namespace Drupal\wso_links_to_video\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Url to Youtube Filter.
 *
 * @Filter(
 * id = "url_to_youtube_filter",
 * title = @Translation("Links to Youtube Filter"),
 * description = @Translation("Converts links to Youtube videos. Place after 'Convert URLs into links' filter"),
 * type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class Url2YoutubeFilter extends FilterBase {

  /**
   * Css classes for the iframe element.
   */
  const IFRAME_CLASSES = [
    'youtube-iframe',
    'embed-responsive-item',
  ];

  /**
   * Css classes for the iframe wrapper.
   */
  const WRAPPER_CLASSES = [
    'embed-responsive',
    'embed-responsive-16by9',
    'mb-3',
  ];

  /**
   * Iframe width.
   */
  const IFRAME_WIDTH = 560;

  /**
   * Iframe height.
   */
  const IFRAME_HEIGHT = 315;

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $html_dom = Html::load($text);

    /** @var \DOMElement $link */
    foreach ($html_dom->getElementsByTagName('a') as $link) {
      $url = $link->getAttribute('href');

      // If Youtube ID is present in URL.
      if ($video_id = $this->searchVideoId($url)) {
        $src = wso_links_to_video_get_yt_embed_string($url, $video_id);
        // Generate iframe with wrapper, replace link with generated elements.
        $iframe = $this->generateIframe($html_dom, $src);
        $wrapper = $this->generateWrapper($html_dom);
        $wrapper->appendChild($iframe);
        $link->parentNode->replaceChild($wrapper, $link);
      }
    }

    $text = Html::serialize($html_dom);
    $result = new FilterProcessResult($text);
    $result->setAttachments([
      'library' => ['wso_links_to_video/youtube_iframes'],
    ]);

    return $result;
  }

  /**
   * Search for the youtube video id in the url.
   *
   * @param $url
   *   URL to parse.
   *
   * @return false|string
   *   Youtube video ID or FALSE.
   */
  public function searchVideoId($url) {
    $matches = wso_links_to_video_find_youtube_links($url);
    return $matches[1][0] ?? FALSE;
  }

  /**
   * Generates iframe element.
   *
   * @param \DOMDocument $html_dom
   *   Loaded HTML.
   * @param string $src
   *   Video src.
   *
   * @return \DOMElement|false
   *   Generated <iframe> element.
   */
  public function generateIframe(\DOMDocument $html_dom, string $src) {
    $iframe = $html_dom->createElement('iframe', '');
    $iframe->setAttribute('width', self::IFRAME_WIDTH);
    $iframe->setAttribute('height', self::IFRAME_HEIGHT);
    $iframe->setAttribute('loading', 'lazy');
    $iframe->setAttribute('src', $src);
    $classes = implode(' ', self::IFRAME_CLASSES);
    $iframe->setAttribute('class', $classes);
    return $iframe;
  }

  /**
   * Generates wrapper div for the iframe.
   *
   * @param \DOMDocument $html_dom
   *   Loaded HTML.
   *
   * @return \DOMElement|false
   *   Generated wrapper <div> element.
   */
  public function generateWrapper(\DOMDocument $html_dom) {
    $wrapper = $html_dom->createElement('div', '');
    $classes = implode(' ', self::WRAPPER_CLASSES);
    $wrapper->setAttribute('class', $classes);
    return $wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Converts links to Youtube videos.');
  }

}
