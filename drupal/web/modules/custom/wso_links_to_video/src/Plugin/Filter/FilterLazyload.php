<?php

namespace Drupal\wso_links_to_video\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to lazy-load inline images.
 *
 * @Filter(
 *   id = "filter_lazyload",
 *   title = @Translation("Native Lazyload inline images"),
 *   description = @Translation("Lazy load inline images via browser's native attribute"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 20
 * )
 */
class FilterLazyload extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $html_dom = Html::load($text);
    $images = $html_dom->getElementsByTagName('img');
    $c = 0;

    foreach ($images as $image) {
      $c++;
      // Do not lazyload first two images inside text.
      // Dirty fix: its complicated to detect image 'above the fold' from backend.
      // https://stackoverflow.com/questions/63962906/largest-contentful-paint-increased-dramatically-when-using-lazyload
      if ($c < 3) {
        continue;
      }

      $image->setAttribute('loading', 'lazy');
    }

    $result->setProcessedText(Html::serialize($html_dom));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('All inline-images will be lazy-loaded.');
  }

}
