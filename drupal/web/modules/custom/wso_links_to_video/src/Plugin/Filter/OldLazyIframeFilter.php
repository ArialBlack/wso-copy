<?php

namespace Drupal\wso_links_to_video\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Youtube iframe to text link Filter.
 *
 * @Filter(
 * id = "old_lazy_iframe_filter",
 * title = @Translation("Old Lazy Iframe Filter"),
 * description = @Translation("Converts old iframe with 'data-echo' to new native lazy iframe"),
 * type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class OldLazyIframeFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $html_dom = Html::load($text);
    $iframes = $html_dom->getElementsByTagName('iframe');

    for ($i = $iframes->length; --$i >= 0; ) {
      $iframe = $iframes->item($i);
      $old_src = $iframe->getAttribute('data-echo');

      if (strlen($old_src) > 10) {
        $iframe->removeAttribute('data-echo');
        $iframe->setAttribute('src', $old_src);
        $iframe->setAttribute('loading', 'lazy');
      }
    }

    $result->setProcessedText(Html::serialize($html_dom));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Converts old iframe with data-echo to native lazy iframe.');
  }
}
