<?php

namespace Drupal\wso_links_to_video\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Youtube iframe to text link Filter.
 *
 * @Filter(
 * id = "iframe_to_text_filter",
 * title = @Translation("Youtube iframe to text link Filter"),
 * description = @Translation("Converts Youtube iframe to text link"),
 * type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class Iframe2TextFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $html_dom = Html::load($text);
    $iframes = $html_dom->getElementsByTagName('iframe');

    for ($i = $iframes->length; --$i >= 0; ) {
      $iframe = $iframes->item($i);
      $src = $iframe->getAttribute('src');
      $yt_urls = wso_links_to_video_find_youtube_links($src);

      if (isset($yt_urls[0][0])) {
        $youtube_paragraph = $html_dom->createElement("p", $yt_urls[0][0]);
        $iframe->parentNode->replaceChild($youtube_paragraph, $iframe);
      }
    }

    $result->setProcessedText(Html::serialize($html_dom));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Converts Youtube iframe to text link.');
  }
}
