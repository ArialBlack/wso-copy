(function ($) {
  Drupal.behaviors.wsoYoutubeIframes = {
    attach: function (context, settings) {

      $('iframe.youtube-iframe', context).each(function () {
        var $this = $(this);
        var dataSrc = $this.attr('data-src');

        if (dataSrc) {
          $this.attr('src', dataSrc);
        }
      });
    }
  };
}(jQuery));
