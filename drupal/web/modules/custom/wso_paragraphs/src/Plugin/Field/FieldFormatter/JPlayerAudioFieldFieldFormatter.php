<?php
/**
 * @file
 * Contains \Drupal\wso_paragraphs\Plugin\Field\FieldFormatter\JPlayerAudioFieldFieldFormatter.
 */

namespace Drupal\wso_paragraphs\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Entity\File;

/** *
 * @FieldFormatter(
 *   id = "jPlayer_audiofield_field_formatter",
 *   label = @Translation("jPlayer Audio Player"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class JPlayerAudioFieldFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $file = File::load($item->target_id);

      $element[$delta] = [
        '#file' => file_create_url($file->getFileUri()),
        '#theme' => 'wso_paragraphs_jplayer_audiofield_formatter',
        '#attached' => [
          'library' => [
            'wso_paragraphs/jplayer'
          ]
        ],
      ];
    }

    return $element;
  }

}
