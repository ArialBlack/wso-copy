<?php

namespace Drupal\wso_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "wso_paragraphs_paragraph_title",
 *   label = @Translation("Paragraph title element"),
 *   description = @Translation("Allows to select HTML wrapper for title."),
 *   weight = 0,
 * )
 */
class ParagraphTitleBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) { }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if ($paragraph->hasField('field_paragraph_title')
      || $paragraph->hasField('field_wsop_title')
      || $paragraph->hasField('field_first_title')) {

      $form['title_element'] = [
        '#type' => 'select',
        '#title' => $this->t('Title element'),
        '#description' => $this->t('Wrapper HTML element'),
        '#options' => $this->getTitleOptions(),
        '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'title_element', $this->getDefaultTag($paragraph)),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $title_element = $paragraph->getBehaviorSetting($this->getPluginId(), 'title_element', $this->getDefaultTag($paragraph));
    $options = $this->getTitleOptions();
    return [$this->t('Title element: @element', ['@element' => $options[$title_element]])];
  }

  /**
   * Return options for heading elements.
   */
  private function getTitleOptions() {
    return [
      'h2' => '<h2>',
      'h3' => '<h3>',
      'h4' => '<h4>',
      'h5' => '<h5>',
      'h6' => '<h6>',
      'div' => '<div>',
    ];
  }

  /*
   * Return default tag per paragraph's type.
   */
  private function getDefaultTag(Paragraph $paragraph) {
    if (!$paragraph->getParagraphType()) {
      return 'div';
    }

    switch ($paragraph->getParagraphType()->id()) {
      case 'list_with_courses':
      case 'grid':
      case 'two_columns':
        return 'h2';
        break;

      case 'list_with_cards':
        return 'h3';
        break;

      case 'text_with_title':
        if ($paragraph->parent_type->value == 'paragraph') {
          $parent_paragraph = Paragraph::load($paragraph->parent_id->value);

          if ($parent_paragraph) {
            $parent_paragraph_type = $parent_paragraph->getParagraphType()->id();

            if ($parent_paragraph_type = 'course_introduction') {
              return 'h3';
            }
          }
        }
        break;

      default:
        return 'div';
    }
  }

}
