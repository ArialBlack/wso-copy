(function ($) {
  Drupal.behaviors.fieldWistia = {
    attach: function (context, settings) {
      $('.wistia_embed', context).once('fieldWistia').each(function () {
        var $this = $(this),
          $wistaContainer = $this.closest('.wistia_responsive_padding');

        window.wistiaInit = function(Wistia) {
          var wistia_id = $this.data('id');

          if (wistia_id) {
            var wistiaEmbed = Wistia.embed(wistia_id);
          }

          if (typeof (wistiaEmbed) !== 'undefined') {
            wistiaEmbed.ready(function () {
              var video_ratio = wistiaEmbed.height() / wistiaEmbed.width();

              if (video_ratio > 0) {
                $wistaContainer.css('padding', video_ratio * 100 + '% 0 0 0');
              }

              setTimeout(function () {
                $wistaContainer.closest('.wistia_responsive_padding_container').prev('.wistia-preloader').remove();
              }, 250);
            });
          }
        };
      });
    }
  };

}(jQuery));
