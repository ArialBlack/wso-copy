(function ($) {
  Drupal.behaviors.paragraphTOC = {
    attach: function (context, settings) {
      $('.paragraph--type--table-of-contents #scroll-spy-nav a', context).once('paragraphTOC').click(function (e) {
        event.preventDefault();

        var toInt = function(number) {
          if (isNaN(number)) {
            number = 0;
          }

          return number;
        };

        var headerHeight = toInt($('#header').height());
        var adminMenHeight = toInt($('#toolbar-bar').height() + $('#toolbar-item-administration-tray').height());

        var anchor = $.attr(this, 'href');
        anchor = anchor.split('#')[1];

        if (anchor) {
          var anchorOffset = toInt($('#' + anchor).offset().top);

          $('html, body').animate({
            scrollTop: anchorOffset - headerHeight - adminMenHeight - 30
          }, 500);
        }

      });
    }
  };
}(jQuery));

