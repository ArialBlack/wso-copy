(function ($) {
  var wso_jplayer_id = 0;

  Drupal.behaviors.wsoJPlayer = {
    attach: function (context, settings) {
      $('.wso-jplayer', context).once('wsoJPlayer').each(function () {
        wso_jplayer_id++;

        var $player = $(this),
          fileName = $player.data('file'),
          ext = fileName.substr(fileName.lastIndexOf('.') + 1),
          m4aFile = '',
          mp3File = '';

        if (ext == 'm4a') {
          m4aFile = fileName;
        }

        if (ext == 'mp3') {
          mp3File = fileName;
        }

        $player.attr('id', 'jquery_jplayer_' + wso_jplayer_id);
        $player.next('.jp-audio').attr('id', 'jp_container_' + wso_jplayer_id);

        $player.jPlayer({
          ready: function (event) {
            $(this).jPlayer("setMedia", {
              title: "audio", //todo: ask for title option
              m4a: m4aFile,
              mp3: mp3File
            });
          },
          supplied: "m4a, mp3",
          wmode: "window",
          useStateClassSkin: true,
          autoBlur: false,
          smoothPlayBar: true,
          keyEnabled: true,
          remainingDuration: true,
          toggleDuration: true,
          globalVolume: true,
          cssSelectorAncestor: "#jp_container_" + wso_jplayer_id,
        });

      });
    }
  };
}(jQuery));
