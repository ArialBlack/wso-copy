/**
 * @file
 * Provides search functionality for jumbotron paragraph.
 */
(function ($, Drupal, drupalSettings) {

  /**
   * Process user redirect to the search page.
   * @type {{attach: Drupal.behaviors.jumbotronSearchRedirect.attach}}
   */
  Drupal.behaviors.jumbotronSearchRedirect = {
    attach: function (context) {

      // Fetch backend settings.
      var type = drupalSettings.field_search_by_type;

      var doSearch = function (searchText) {
        // Fetch text from search widget.

        let host = window.location.origin;
        let url = new URL(host + '/search/site');
        url.searchParams.append('search_api_fulltext', searchText);

        // Check if type exist.
        if (type !== undefined) {
          url.searchParams.append('f[0]', 'type:' + type);
        }

        location.href = url.toString();
      };

      $('input.jumbotron-search-widget', context).keypress(function (event) {
        if (event.keyCode === 13) {
          doSearch($(this).val());
        }
      });

      $('#jumbotron-submit-search', context).click(function (event) {
        doSearch($('input.jumbotron-search-widget').val());
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
