(function ($) {
  Drupal.behaviors.paragraphsJumbotron = {
    attach: function (context, settings) {
      $('.paragraph-type--wide-low-jumbotron', context).once('paragraphsJumbotron').each(function () {
        var $jumbotron = $(this);
        var $scroll2CoursesButton = $jumbotron.find('a[href="#courses"]');
        var $tpReviewsButton = $jumbotron.find('a[href="#reviews"]');
        var $coursesSection = $('.paragraph--type--list-with-courses').eq(0);
        var $tpReviewsSection = $('.view-trustpilot-reviews').eq(0);
        var $tpWidget = $('.view-content-header .tp-stars').clone();

        var toInt = function(number) {
          if (isNaN(number)) {
            number = 0;
          }

          return number;
        };

        var btnAction = function(e, $selector) {
          event.preventDefault();
          var headerHeight = toInt($('#header').height());
          var adminMenHeight = toInt($('#toolbar-bar').height() + $('#toolbar-item-administration-tray').height());
          var anchorOffset = toInt($selector.offset().top);

          $('html, body').animate({
            scrollTop: anchorOffset - headerHeight - adminMenHeight - 30
          }, 500);
        };

        $tpWidget.insertAfter($tpReviewsButton.find('.text').wrap('<div class="tp-widget"></div>'));

        $scroll2CoursesButton.click(function(e) {
          btnAction(e, $coursesSection);
        });

        $tpReviewsButton.click(function(e) {
          btnAction(e, $tpReviewsSection);
        });

      });
    }
  };
}(jQuery));
