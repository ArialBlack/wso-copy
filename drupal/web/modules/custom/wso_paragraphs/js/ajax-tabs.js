(function ($) {
  Drupal.behaviors.fieldPdf = {
    attach: function (context, settings) {
      $('button.ajax-tab', context).each(function () {
        $(this).on('click', function (e) {
          e.preventDefault();
          var url = $(this).data('ajax-url');
          Drupal.ajax({
            url: url,
            wrapper: 'article-wrapper'
          }).execute()
          history.pushState({}, '', url);
        });
      });
    }
  };

}(jQuery, Drupal));
