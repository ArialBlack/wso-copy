(function ($) {
  Drupal.behaviors.fieldPdf = {
    attach: function (context, settings) {
      $('.paragraphs-item-wso-paragraph-pdf iframe', context).once('fieldPdf').each(function () {
        var $this = $(this),
          resizeTimer,
          $lessonHeader = $('#lesson-header'),
          $window = $(window),

          resizePdfContainer = function() {
            var windowHeight = $window.height(),
              lessonHeaderHeight = $lessonHeader.height();

            $this.css('min-height', windowHeight - lessonHeaderHeight - 55 + 'px');
          };

        if ($lessonHeader.length !== 1) {
          return;
        }

        resizePdfContainer();

        $(window).on('resize', function () {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function () {
            resizePdfContainer();
          }, 250);
        });

      });
    }
  };

}(jQuery));
