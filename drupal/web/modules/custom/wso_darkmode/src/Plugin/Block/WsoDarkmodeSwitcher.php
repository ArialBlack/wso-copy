<?php

namespace Drupal\wso_darkmode\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'WsoDarkmodeSwitcher' block.
 *
 * @Block(
 *  id = "wso_darkmode_switcher",
 *  admin_label = @Translation("WsoDarkmode Switcher"),
 * )
 */
class WsoDarkmodeSwitcher extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $module_path = drupal_get_path('module', 'wso_darkmode');
    $build['wso_darkmode_switcher']['#theme'] = 'wso_darkmode';
    $build['wso_darkmode_switcher']['#module_path'] = $module_path;
    $build['wso_darkmode_switcher']['#attached'] = array(
      'library' => array(
        'wso_darkmode/wso_darkmode',
      ),
    );
    return $build;
  }

}
