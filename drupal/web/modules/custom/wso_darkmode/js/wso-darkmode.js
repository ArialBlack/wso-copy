(function ($) {
  var invertColor = function (hex) {
    if (hex.indexOf('#') === 0) {
      hex = hex.slice(1);
    }

    // Сonvert 3-digit hex to 6-digits.
    if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }

    if (hex.length !== 6) {
      throw new Error('Invalid HEX color.');
    }

    // Invert only light color components.
    var r = parseInt(hex.slice(0, 2), 16);
      g = parseInt(hex.slice(2, 4), 16);
      b = parseInt(hex.slice(4, 6), 16);

    if (r > 100 && g > 100 && b > 100) {
      r = (255 - r).toString(16);
      g = (255 - g).toString(16);
      b = (255 - b).toString(16);

      // Pad each with zeros and return.
      return '#' + padZero(r) + padZero(g) + padZero(b);
    }
    else {
      return hex;
    }
  };

  var padZero = function (str, len) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
  };

  var switchBackgroundColors = function ($page) {
    var $sections = $page.find('[data-bg-color]');

    $sections.each(function () {
      var $this = $(this),
        bgColor = $this.data('bg-color'),
        invertedColor = invertColor(bgColor);

      $this.css('background-color', invertedColor).data('bg-color', invertedColor);
    });
  };

  var targetPages = 'body.with-paragraphs, body.path-frontpage';

  Drupal.behaviors.wsoDarkMode = {
    attach: function (context, settings) {
      // Switcher logic.
      $('#wso-darkmode-switcher', context).once('wsoDarkMode').on('click', function (e) {
        e.preventDefault();
        var $html = $('html'),
          $pages = $(targetPages),
          darkModeEnabled = localStorage.getItem('wsoDarkMode');

        if (darkModeEnabled == 1) {
          localStorage.setItem('wsoDarkMode', 0);
          $html.removeClass('darkmode');
          $("iframe.cke_wysiwyg_frame").contents().find("html").removeClass('darkmode');
        }
        else {
          localStorage.setItem('wsoDarkMode', 1);
          $html.addClass('darkmode');
          $("iframe.cke_wysiwyg_frame").contents().find("html").addClass('darkmode');
        }

        switchBackgroundColors($pages);
      });

      // Invert inline colors at some pages.
      $(targetPages, context).once('wsoDarkModeInvert').each(function () {
        if ($('html').hasClass('darkmode')) {
          switchBackgroundColors($(this));
        }
      });
    }
  };
})(jQuery);

