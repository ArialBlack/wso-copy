# Project Description

This is Docker-based installation of the WSO web-site, created during the D8 migration.
It uses the docker4drupal stack: https://github.com/wodby/docker4drupal and Drupal composer template: 
https://github.com/drupal-composer/drupal-project

It can be run using ddev or any other way, but preferable is using d4d setup.
The live site doesn't use docker or any virtualization. 
Our docker setup isn't exactly matching the live server. 
For example live server has installed MySQL 8 on, and we use Mariadb 10 in our docker setup (no docker images with mysql 8 due to oracle licensing I guess). 
We have had an ongoing discussion about that setup for years but our sysadmin is strongly against using docker, 
and it is hard for having all that software inlined on the dev's machines without it.
Trying to use ansible or something like that will just get things more complicated.


## 1. Requirements
* First you need to [install composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).
* Install Docker ([Linux](https://docs.docker.com/engine/installation), [Docker for Mac](https://docs.docker.com/engine/installation/mac) or [Docker for Windows (10+ Pro)](https://docs.docker.com/engine/installation/windows))
* For Linux additionally install [docker compose](https://docs.docker.com/compose/install)

## 2. Project setup
1. Copy the .env_template to .env. Check the .env file. Here you can set your host and project name. Container names will depend on that.
1. Check the PHP_TAG, right now we use 7.4 on the live site, so you should choose the version accordingly, depending on
   the system you use. Notice that MacOS requires to use different image. Some newer images may not work with Mac M1, so
   please choose carefuly.
2. Update traefik configuration. Read [this doc](https://wodby.com/docs/stacks/drupal/local/#running-multiple-projects) 
regarding running multiple projects on one machine. By default docker-compose.yml in this repo has traefik service disabled
and you need to copy the traefik.yml outside the project dir and run it separately with `docker-compose -f traefik.yml up -d`
But you can use the docker-composer.override.yml to run traefik service with other services. See the next item for details.
3. Overriding docker configuration. Project includes docker-compose.overrides.local.yml which you can rename to docker-composer.override.yml
and use for different overrides (like running traefik, chaning ports or adding some specific services or OS-related tweaks). You can also use
`ln -s docker-composer.override.ENV.yml docker-composer.override.yml` where ENV has three options now - `local`, `dev`, `mac`.
The Mac version uses mutagen for performance (https://wodby.com/docs/1.0/stacks/drupal/local/#mutagen - you need to install it first).
Mutagen is strongly recommended to use on the mac machnines. In case you decide to use it you need to run `mutagen-compose` instead of `docker-compose` later in this guide.  
See docker4drupal docs for the details.
4. Copy the /drupal/web/sites/default/settings.dev.php to /drupal/web/sites/default/settings.local.php
5. Create folder named mariadb-init in the project root and place the database dump there. You may need to fix the collation first (MySQL 8 and Mariadb 10 use different collations), on macOS it can done this way: `LC_ALL=C sed -i '' 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g' [dump_name].sql`
6. You can start project with `docker-compose up -d`. Inital load of the detabase dump takes around 3-5 hours. Make sure to run `docker-compose down -v` before re-importing the database (in case it fails for example) . Containers should be stopped only with `docker-compose stop` (not down, or you'll need to re-import the db).
7. After database dump is restored do `docker-compose exec php bash`, you'll need to run a few commands there:
  - `composer install`
  - `drush en -y stage_file_proxy dblog`
  - `drush pm-uninstall monolog`
8. Create the solr core. Run `docker-compose exec solr_wso bash` and run this: `make create core=wso -f /usr/local/bin/actions.mk`
9. Use drush uli inside the php container to log in as admin. I suggest creating a separate account and granting it the admin role instead of using the superadmin account later.

## 3. Running project
1. You can start project with `docker-compose up -d`. Inital load of the detabase dump takes around 3-5 hours. Make sure to run `docker-compose down -v` before re-importing the database (in case it fails for example) . Containers should be stopped only with `docker-compose stop` (not down, or you'll need to re-import the db).
3. Working directory is `./drupal` folder. Drupal is installed into the `./drupal/web`, configuration is exported to `./drupal/conf`
4. We don't commit the vendor directory. You need to run `composer install` to download modules and required libraries. 
You need to do taht from the `./drupal` directory (as all other interaction with composer).
5. For the module installation we use composer too. Example: `composer require drupal/admin_toolbar`. Patches are handled via composer too.
See the  [drupal composer template](https://github.com/drupal-composer/drupal-project) documentation.

## 4. Drupal installation
1. By default site should be available at http://wallstreetoasis.localhost:8000 . You can change the host in .env file. Also you can 
change the port by changing traefik port binding.
2. You always can run drush uli to log in.
3. For running drush commands you need to log in into the php container with `docker-compose exec php bash` or you can 
execute instruction directly by `docker-compose exec php drush {command}`. Also you can add alias for that into your .bashrc or .bashprofile.
Here is an example: `alias ddrush='docker-compose exec php drush'`. Don't forget to run `source ~/.bashrc` to apply changes.
